package GammaXmlMessages;

import java.lang.*;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class HeaderParams extends XmlElementBuilder
{
	public static final String headerElementName = "envelope";
	private static final String uidElementName = "uid";
	private static final String subjectElementName = "subject";
	private static final String whenElementName = "when";
	private static final String sinceEpochElementName = "since_epoch";
	private static final String dateElementName = "date";
	private static final String timeElementName = "time";
	private static final String locationElementName = "location";
	private static final String locLatitudeElemName = "lat";
	private static final String locLongtitudeElemName = "lon";
	
	private final String envelopeSensitivityAttr = "classification";
	private final String envelopePriorityAttr = "priority";
	
	private static final long utcMillisecThreshold = ((long)100) * ((long)1000) * ((long)1000000); // 100 billion  - threshold for distinguishing millisecs and seconds
	
	public String envelopeSensitivity;
	public String envelopePriority;
	
	public EnvelopeUid uid;
	public Date when;
	public String subject;
	
	public Double longt;
	public Double lat;
	
	public EnvelopeUid refUid;
	
	public HeaderParams(EnvelopeUid Uid, Date When, Double Longt, Double Lat, String Sensitivity, String Priority, String Subject)
	{
		uid = Uid;
		when = When;
		longt = Longt;
		lat = Lat;
		
		envelopeSensitivity = Sensitivity;
		envelopePriority = Priority;
		subject = Subject;
		
		// TODO: make uid reference for header
		refUid = null;
	}
	
	public HeaderParams(Element elem) throws Exception
	{
		if(elem.getTagName() != headerElementName)
		{
			throw new Exception();
		}
		
		int uidNum = 0;
		int whenNum = 0;
		int subjectNum = 0;
		int latNum = 0;
		int longNum = 0;
		
		NodeList childs = elem.getChildNodes();
		for(int i = 0; i < childs.getLength(); i++)
		{
			Node currNode = childs.item(i);
			if(currNode.getNodeType() == Node.ELEMENT_NODE)
			{
				Element currElem = (Element) currNode;
				if(currElem.getTagName().equals(uidElementName))
				{
					uidNum++;
					uid = getUidFromElem(currElem);
				}
				
				if(currElem.getTagName().equals(whenElementName))
				{
					whenNum++;
					when = getDateTimeFromElem(currElem);
				}
				if(currElem.getTagName().equals(subjectElementName))
				{
					subjectNum++;
					subject = currElem.getTextContent();
				}
				if(currElem.getTagName().equals(locLatitudeElemName))
				{
					latNum++;
					lat = Double.valueOf(currElem.getTextContent());
				}				
				if(currElem.getTagName().equals(locLongtitudeElemName))
				{
					longNum++;
					longt = Double.valueOf(currElem.getTextContent());
				}
				// TODO: add all the rest
			}
		}
		if((uidNum != 1) || (whenNum != 1) || (subjectNum != 1))
		{
			throw new Exception("Incorrect header structure!");
		}
	}
	
	
	private Date getDateTimeFromElem(Element elem) throws Exception 
	{
		NodeList childNodes = elem.getChildNodes();
		for(int i = 0; i < childNodes.getLength(); i++)
		{
			Node currNode = childNodes.item(i);
			if(currNode.getNodeType() != Node.ELEMENT_NODE)
			{
				continue;
			}
			Element firstElem = (Element) currNode;
			if(firstElem.getTagName() == sinceEpochElementName)
			{
				long multiplier = 1;
				long sinceEpoch = Long.parseLong(firstElem.getTextContent());
				if(sinceEpoch < utcMillisecThreshold)
				{
					multiplier = 1000; // this is in seconds - multiply by 1000
				}
				return new Date(sinceEpoch * multiplier); // here an error may occur
			}
			else if(firstElem.getTagName().equals(dateElementName))
			{
				return getDateTimeFromComplexElem(elem);
			}
			else
			{
				throw new Exception("Date in envelope is not well-formed!");
			}
		}
		throw new Exception("Date in envelope is not well-formed!");
	}

	private Date getDateTimeFromComplexElem(Element elem) 
	{
		// TODO: finish that stuff!!
		int dateYearNum = 0;
		return null;
	}

	private EnvelopeUid getUidFromElem(Element currElem) throws Exception 
	{		
		return new EnvelopeUid(currElem);
	}
	
	public Element serialise(Document envelope)
	{
		Element result = null;
		try
		{
			Element header = envelope.createElement(headerElementName);
			if(envelopeSensitivity != null)
			{
				Attr sensitivity = envelope.createAttribute(envelopeSensitivityAttr);
				sensitivity.setValue(envelopeSensitivity);
				header.setAttributeNode(sensitivity);
			}
			
			if(envelopePriority != null)
			{
				Attr priority = envelope.createAttribute(envelopePriorityAttr);
				priority.setValue(envelopePriority);
				header.setAttributeNode(priority);
			}
			
			Element uidElem = envelope.createElement(uidElementName);
			
			Element uidElemSystemId = envelope.createElement(EnvelopeUid.uidSystemIDElemName);
			uidElemSystemId.appendChild(envelope.createTextNode(uid.GetSystemId()));
			
			Element uidElemSequenceNumber = envelope.createElement(EnvelopeUid.uidSequenceNumberName);
			uidElemSequenceNumber.appendChild(envelope.createTextNode(uid.GetSequenceNumber()));
			
			uidElem.appendChild(uidElemSystemId);
			uidElem.appendChild(uidElemSequenceNumber);
			
			header.appendChild(uidElem);
			
			
			if(when != null)
			{
				Long sinceEpoch = when.getTime(); // we assign only since_epoch format.
				String sinceEpochStr = sinceEpoch.toString();
				Element whenElem = envelope.createElement(whenElementName);
				Element sinceEpochElem = envelope.createElement(sinceEpochElementName);
				sinceEpochElem.appendChild(envelope.createTextNode(sinceEpochStr));
				whenElem.appendChild(sinceEpochElem);
				header.appendChild(whenElem);
			}
			
			if(subject != null)
			{
				Element subj = envelope.createElement(subjectElementName);
				subj.appendChild(envelope.createTextNode(subject));
				header.appendChild(subj);
			}
			
			if( (longt != null) && (lat != null) )
			{
				String longStr = longt.toString();
				String latStr = lat.toString();
				
				Element locatElem = envelope.createElement(locationElementName);
				
				Element longLocElem = envelope.createElement(locLongtitudeElemName);
				longLocElem.appendChild(envelope.createTextNode(longStr));
				
				Element latLocElem = envelope.createElement(locLatitudeElemName);
				latLocElem.appendChild(envelope.createTextNode(latStr));
				
				locatElem.appendChild(latLocElem);
				locatElem.appendChild(longLocElem);
				
				header.appendChild(locatElem);
			}
			
			// TODO: add uid reference processing (header)
			
			result = header;
		}
		catch (Exception e)
		{
			
		}
		return result;
	}
	
}
