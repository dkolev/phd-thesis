function [ AdjMatr, StartID, StopID, ExpandedAssetID, ExpandedEnterID, Correspondance ] = ...
    CreateExpandedGraph4VC( ThreatGraph, AttackTypeNum, AssetID, EnterID )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

%% Split vertices for cutting in the initial graph
n = size(ThreatGraph, 1);

V2C = setdiff(1:n, [AssetID, EnterID]);

tmp = repmat(V2C, n, 1);

Correspondance = cell(length(V2C), 2);
%preparing for vertex cut
NewEntryIndeces = sum(tmp < repmat((1:n)', 1, length(V2C)), 2) + (1:n)';
NewExitIndeces = NewEntryIndeces;
NewExitIndeces(V2C) = NewExitIndeces(V2C) + 1;

ThrGr = zeros(n + length(V2C));

ThrGr(NewEntryIndeces, NewExitIndeces) = ThreatGraph;

for i=1:length(V2C)
    ThrGr(NewExitIndeces(V2C(i)), NewEntryIndeces(V2C(i))) = 1;
    Correspondance{i, 1} = V2C(i);
end

ThreatGraph = ThrGr;
AssetID = NewEntryIndeces(AssetID)';
EnterID = NewEntryIndeces(EnterID)';

n = size(ThreatGraph, 1);

%% Create block matrix (expanded graph)
%maping to the extanding graph
Blocks = cell(AttackTypeNum, 1);
for i=1:AttackTypeNum
    Blocks{i} = ThreatGraph;
end

ToAdd = 0:n:( (n-1)*AttackTypeNum );

tmp = blkdiag(Blocks{:}); % create block-diagonal matrix (layer-based graph)

%prepare for vertex-cut
N = size(tmp, 1);
%% Add Start node
N = size(tmp, 1);
StartID = N+1;

tmp = [tmp; zeros(1, N)];
tmp = [tmp, zeros(N+1, 1)];

EnterID_n = repmat(EnterID, AttackTypeNum, 1) + repmat(ToAdd', 1, length(EnterID)); 
EnterID_n = EnterID_n';
EnterID_nn = EnterID_n(:);

tmp(EnterID_nn, end) = 1;

%% Add Stop node
N = size(tmp, 1);
StopID = N+1;

tmp = [tmp; zeros(1, N)];
tmp = [tmp, zeros(N+1, 1)];

AssetID_n = repmat(AssetID, AttackTypeNum, 1) + repmat(ToAdd', 1, length(AssetID)); 
AssetID_n = AssetID_n';
AssetID_nn = AssetID_n(:);

tmp(end, AssetID_nn) = 1;

AdjMatr = tmp;
ExpandedAssetID = AssetID_n;
ExpandedEnterID = EnterID_n;

%% Create Correspondance between initial nodes and edges in exp. graph.

V2C_ent = NewEntryIndeces(V2C);
V2C_ext = NewExitIndeces(V2C);

V2C_ent = repmat(V2C_ent, 1, AttackTypeNum) + repmat(ToAdd, length(V2C), 1);
V2C_ext = repmat(V2C_ext, 1, AttackTypeNum) + repmat(ToAdd, length(V2C), 1);

for i=1:length(V2C)
    Correspondance{i, 2} = [V2C_ent(i, :)',V2C_ext(i, :)'];
end

end

