function [  ] = InitSecurityModel( InternalGraph, InternWeights, ImpactCalcFun, ThreatGraph, AssetID, EnterID,...
    ChckPntControlMat, ThreatControlMat, CostFun)
%Initialize the security model structures.

AttackTypesNum = size(ThreatControlMat, 1);
ImpactPerAsset = ImpactCalcFun(InternalGraph, InternWeights, AttackTypesNum); %matrix asset x attackTypeNum x ImpactTypes, filled with impacts
CritNum = size(ImpactPerAsset, 3);
AssetNum = length(AssetID);
%ImpactTypeNum = size(ImpactPerAsset, 3);
AttackTypesNum = size(ThreatControlMat, 1);

tmp = sum(ThreatGraph(AssetID, :), 1);
AssetAccessID = find(tmp > 0);

% Corresp: ChckPointNum x 2. Corres{i, 1} - ith initial check point ID.
% Corresp{i, 2} - matrix of corresponding Check-edges in the Exp Graph.
% Corresp{i, 2}(j, :) - edge, that corresponds to Corres{i, 1} node, attack
% type j.
% ExpandedAssetID : AssetNum x AttackNum. The same for EnterID
%
[ExpandedGraphStructure, StartID, StopID, ExpandedAssetID, ExpandedEnterID, Corresp] =...
    CreateExpandedGraph4VC(ThreatGraph, AttackTypesNum, AssetID, EnterID);

CheckPointsNum = size(Corresp, 1);
N = size(ExpandedGraphStructure, 1);

CostTh = 2;
ImpactUpperBound = repmat(inf, CritNum-1, 1);


[ Criteria, D, p, y, z, fval OptimalAttacks, IsFeasible ] = MaximizeCriteria(CostTh , ImpactPerAsset,...
    ExpandedGraphStructure, Corresp, ExpandedAssetID, StartID, StopID, ...
    ThreatControlMat, ChckPntControlMat, ImpactUpperBound, CostFun);

if(~IsFeasible)
   disp('Insufficient funds for given protection level');
   return; 
end

% ATMSecurityGame = struct('IntGraph', InternalGraph, 'InternWeights', InternWeights,...
%     'ImpactPerAsset', ImpactPerAsset, 'ThreatGraph', ThreatGraph, 'AssetID', AssetID,...
%     'AssetAccessID',  AssetAccessID, 'ExpandedGraphStructure', ExpandedGraphStructure, 'StartID', StartID,...
%     'StopID', StopID, 'ExpandedAssetID', ExpandedAssetID, 'ExpandedEnterID', ExpandedEnterID);

%% Full coverage test
% [ Aineq, bineq, Aeq, beq, f ] = PrepareLProblem(...
%     ExpandedGraphStructure, StartID, StopID,...
%     Corresp, ThreatControlMat, ChckPntControlMat, CostFun);
% 
% [ D, p, y, fval ] = SolveCutLP( f, Aineq, bineq, Aeq, beq,...
%     CheckPointsNum, AttackTypesNum, N);


GraphStructure = biograph(ThreatGraph');
h = view(GraphStructure);
set(h.Nodes(EnterID), 'Shape', 'trapezium')
for i=1:length(EnterID)
    set(h.Nodes(EnterID(i)), 'ID', ['Entrance ', num2str(i)]);
end
set(h.Nodes(AssetID), 'Shape', 'ellipse');
for i=1:length(AssetID)
    set(h.Nodes(AssetID(i)), 'ID', ['SA ', num2str(i)]);
end
clc;

st = sprintf('Cost for a given protection security system: %f \n', fval);
disp(st);
Ids = find(y==1);
for i = 1:length(Ids)
    controlNumb = ceil(Ids(i) / CheckPointsNum);
    chPointNumb = Ids(i) - (controlNumb - 1)*CheckPointsNum;
    st = sprintf('Control type %d should be placed at node %d \n', controlNumb, Corresp{chPointNumb, 1});
    disp(st);
end

AssetProtected = reshape(p(ExpandedAssetID), AssetNum, AttackTypesNum);
[r, c] = find(AssetProtected == 0);
for i=1:length(r)
    st = sprintf('Attack type %d to asset %d is tolerated. \n', c(i), r(i));
    disp(st);
end
if(isempty(r))
    disp('No attacks tolerated, full protection.');
end 

disp('For given constraints optimal (for criteria 1) security placement guarantees:');

for i=1:length(Criteria)
    st = sprintf('Objective function %d not higher than %f \n', i, Criteria(i));
    disp(st);
end

disp(ImpactPerAsset(:,:,1));

%CalculateImpact(ExpandedGraphStructure, ImpactPerAsset(:,:,1), D, Corresp, ExpandedAssetID, StartID, StopID);


end

