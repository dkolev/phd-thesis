package GammaXmlMessages;

import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Node;

import java.io.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class IdsXmlDataFile extends GammaXmlDataFile
{
	private static final String headerElementName = "messages";
	
	private List<IdsMessage> _messages;
	
	
	
	private Document doc;
	
	public IdsXmlDataFile(List<IdsMessage> messages) throws ParserConfigurationException
	{
		_messages = messages;
		
	}
	
	public IdsXmlDataFile(Document doc) throws Exception
	{
		Element messages = doc.getDocumentElement();
		_messages = new ArrayList<IdsMessage>();
		if(!messages.getTagName().equals(headerElementName))
		{
			throw new Exception("Documet format error!");
		}
		NodeList messageList = messages.getChildNodes();
		for(int i = 0; i < messageList.getLength(); i++)
		{
			Node currentNode = messageList.item(i);
			// create messages from nodes
			if(currentNode.getNodeType() == Node.ELEMENT_NODE)
			{
				Element currMsg = (Element) currentNode;
				IdsMessage msg = new IdsMessage(currMsg);
				_messages.add(msg);
			}
		}
	}
	
	public String getFormedMessage()
	{
		String result = null;
		try
		{
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder;
			docBuilder = docFactory.newDocumentBuilder();
		
			doc = docBuilder.newDocument();	
			
			Element root = doc.createElement(headerElementName);
			for(int i = 0; i < _messages.size(); i++)
			{
				Element currMsg = _messages.get(i).serialiseMessage(doc);
				if(currMsg != null)
				{
					root.appendChild(currMsg);
				}
			}
			doc.appendChild(root);
			result = getStringFromDocument(doc);
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return result;
	}
	
	
	public String getStringFromDocument(Document doc)
	{
	    try
	    {
	       DOMSource domSource = new DOMSource(doc);
	       StringWriter writer = new StringWriter();
	       StreamResult result = new StreamResult(writer);
	       TransformerFactory tf = TransformerFactory.newInstance();
	       Transformer transformer = tf.newTransformer();
	       transformer.transform(domSource, result);
	       return writer.toString();
	    }
	    catch(TransformerException ex)
	    {
	       ex.printStackTrace();
	       return null;
	    }
	}
	
	public int getMessageNum()
	{
		return _messages.size();
	}
	
	
	@Override
	public String getMessageContent(int idx)
	{
		String result = null;
		if(idx >= 0 && idx < _messages.size())
		{
			result = _messages.get(idx).getContent();
		}
		return result;
	}
	
	@Override
	public String getMessageContentType(int idx)
	{
		String result = null;
		if(idx >= 0 && idx < _messages.size())
		{
			result = _messages.get(idx).getContentType();
		}
		return result;
	}	
	
	public String getMessageSystemId(int idx)
	{
		String result = null;
		if(idx >= 0 && idx < _messages.size())
		{
			result = _messages.get(idx).getuisSystem();
		}
		return result;
	}
	
	public double getCoordinatesLongtitude(int idx)
	{
		double result = 0;
		if(idx >= 0 && idx < _messages.size())
		{
			Double res = _messages.get(idx)
					.getCoordinatesLongtitude();
			if(res != null)
			{
				result = res;
			}
		}
		return result;
	}
	
	public double getCoordinatesLatitude(int idx)
	{
		double result = 0;
		if(idx >= 0 && idx < _messages.size())
		{
			Double res = _messages.get(idx)
					.getCoordinatesLatitude();
			if(res != null)
			{
				result = res;
			}
		}
		return result;
	}

}
