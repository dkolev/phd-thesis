# -*- coding: utf-8 -*-
"""
Created on Sun Jun  9 14:24:47 2019

@author: admin
"""

import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt


class NeuralNetEventDetector:
    
    
    model = None
    x_in = None
    loss = None
    net = None
    optimizer = None
    sess = None
    per_vec_loss = None
    
    num_iters = None
    
    loss_smooth = 0.001
    batch_size = 3
    
    
    def __init__(self, num_iters):
        self.num_iters = num_iters
        
    def select_batch(self, X, batch_size):
        idx = np.random.randint(0, X.shape[0], batch_size)
        return X[idx, :]
        
        
    def fit(self, data, Y):
        X = data[Y == 0, :]
        self.create_model(X.shape[1])
        self.sess = tf.Session()
        curr_loss = 0
        self.sess.run(tf.global_variables_initializer())
        for i in range(self.num_iters):
            xb = self.select_batch(X, self.batch_size)
            [opt, loss] = self.sess.run([self.optimizer, self.loss], feed_dict={self.x_in : xb})
            alpha = 1.0 / np.float(i + 1)
            if(alpha < self.loss_smooth):
                alpha = self.loss_smooth
            curr_loss = (1 - alpha) * curr_loss + alpha * loss
            if i % 50 == 0:
                print(curr_loss)
        
        dists = self.distance(X)
        idx = np.argsort(dists)
        sd = dists[idx]
        Ys = Y[idx]
        Yidx = Ys > 0
        Yidx = Yidx.astype('float32')
        tn = np.cumsum(1 - Yidx) / np.sum(1 - Yidx)
        tp = (np.sum(Yidx) - np.cumsum(Yidx))/ np.sum(Yidx)
        plt.plot(tn, tp)
        difftn = tn[1:] - tn[:-1]
        auc = np.sum(difftn * tp[1:])
        threshold = np.min(sd[tn > 0.95])
        Yvals = np.unique(Y)
        Yvals = Yvals[Yvals != 0]
        table = {}
        recall = np.sum((Ys == 0) * (sd < threshold)) / np.sum((Ys == 0))
        tpv = np.sum((Ys > 0) * (sd >= threshold)) / np.sum((Ys > 0))
        precision = np.sum((Ys == 0) * (sd < threshold)) / np.sum(sd < threshold)
        table[0] = [auc, recall, tpv, precision]
        for y in Yvals:
            tpvy = np.sum((Ys == y) * (sd >= threshold)) / np.sum(Ys == y)
            Ysy = Yidx[(Ys == y) + (Ys == 0)]
            tny = np.cumsum(1 - Ysy) / np.sum(1 - Ysy)
            tpy = (np.sum(Ysy) - np.cumsum(Ysy))/ np.sum(Ysy) 
            difftn = tny[1:] - tny[:-1]
            aucy = np.sum(difftn * tpy[1:])
            table[y] = [tpvy, aucy]
        return table
         
    def distance(self, X):
        result = self.sess.run(self.per_vec_loss, feed_dict={self.x_in : X})
        #for i in range(X.shape[0]):
        #    result[i] = self.sess.run(self.loss, feed_dict={self.x_in : np.reshape(X[i, :], (1, X.shape[1]))})
        return result
        
        
    def create_model(self, n_inp):
        self.x_in = tf.placeholder("float", shape=[None, n_inp])
        model = [
            tf.layers.Dense(n_inp / 2, activation=tf.nn.relu),
            tf.layers.Dense(n_inp / 4, activation=tf.nn.tanh),
            tf.layers.Dense(n_inp / 2, activation=tf.nn.relu),
            tf.layers.Dense(n_inp)]
        
        self.net = self.x_in
        for layer in model:
            self.net = layer(self.net)
        self.loss = tf.reduce_sum(tf.abs(self.net - self.x_in))
        self.per_vec_loss = tf.reduce_sum(tf.abs(self.net - self.x_in), axis=1)
        adam_opt = tf.train.AdamOptimizer()
        self.optimizer = adam_opt.minimize(self.loss)
        return model