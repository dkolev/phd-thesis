function [ h, context ] = DrawAttackerNode( h, context )
% a path in initial nodes is required 
node = context.node;
oldNode = context.oldNode;

% reset old nodes
if(~isempty(oldNode))
    set(h.Nodes(oldNode), 'Shape', context.oldShape);
    set(h.Nodes(oldNode), 'Color', context.oldColor);
    set(h.Nodes(oldNode), 'LineWidth', context.oldWidth);
end

context.oldShape = get(h.Nodes(node), 'Shape');
context.oldColor = get(h.Nodes(node), 'Color');
context.oldWidth = get(h.Nodes(node), 'LineWidth');
context.oldNode = node;

set(h.Nodes(node), 'Shape', 'ellipse');
set(h.Nodes(node), 'Color', [1, 0, 1]);
set(h.Nodes(node), 'LineWidth', 2);


end

