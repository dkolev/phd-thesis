package GammaXmlMessages;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public abstract class IdsAlarmContent extends XmlElementBuilder
{
	protected String contentType;
	
	public abstract Element serialiseContent(Document doc);
	
	
	public static IdsAlarmContent getAlarmContent(Element elem)
	{
		IdsAlarmContent content = null;
		try
		{
			content = new DlrIdsAlarmContent(elem);
		}
		catch(Exception e)
		{
			
		}
		if(content == null)
		{
			content = new StringIdsAlarmContent(elem);
		}
		return content;
	}
	
	public abstract String what();
	
	public String getType()
	{
		return contentType;
	}
}
