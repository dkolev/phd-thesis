function [ Capacity, Latency, DataErrorRate ] = FormCapacityLatencyVectors( SupportingAssetStructure )
%UNTITLED7 Summary of this function goes here
%   Detailed explanation goes here
Capacity = zeros(size(SupportingAssetStructure, 1));
Latency = zeros(size(SupportingAssetStructure,1), 1);
DetaErrorRate = zeros(size(SupportingAssetStructure,1), 1);
for i =1:size(SupportingAssetStructure, 1)
    Capacity(:,i) = SupportingAssetStructure{i, 2};
    Latency(i) = SupportingAssetStructure{i, 1};
    DataErrorRate(i) = SupportingAssetStructure{i, 4};
end

end

