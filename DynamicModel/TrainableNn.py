# -*- coding: utf-8 -*-
"""
Created on Wed Jun 12 18:15:24 2019

@author: admin
"""
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
from LAMBOptimiser import LAMBOptimizer, create_optimizer

class ClassificationVariationalAutoEncoder:
    
    encoder_log_var = None
    encoder_mean = None
    
    input_encoder = None
    input_decoder = None
    decoder = None
    decoder_loss = None
    
    prob = None
    logprob = None
    optimizer = None
    
    hidden_dim = None
    visible_dim = None
    session = None
    
    def select_batch(self, X, labels, batch_size):
        idx = np.random.randint(0, X.shape[0], batch_size)
        return X[idx, :], labels[idx, :]
    
    def __init__(self, hidden_dim, input_dim, class_num):
        self.hidden_dim = hidden_dim
        self.visible_dim = input_dim
        self.class_num = class_num
        self.loss_smooth = 0.001
        self.create_encoder_decoder()
    
    
    def sample_size(self, z_mean, z_log_var, nsamples):
        epsilon = np.random.multivariate_normal(z_mean, np.diag(np.exp(0.5 * z_log_var)), nsamples)
    
    def sampling(self, z_mean, z_log_var, n_samples):
        epsilon = tf.random.normal([tf.shape(z_mean)[0], n_samples, self.hidden_dim])
        z_mean_r = tf.tile(tf.expand_dims(z_mean, 1), [1, n_samples, 1])
        z_log_var_r = tf.tile(tf.expand_dims(z_log_var, 1), [1, n_samples, 1])
        return z_mean_r + tf.exp(0.5 * z_log_var_r) * epsilon
    
    def create_encoder_decoder(self):
        self.input_encoder = tf.placeholder(tf.float32, [None, self.visible_dim])
        self.input_labels = tf.placeholder(tf.float32, [None, self.class_num])
        net = tf.layers.Dense(self.visible_dim / 2 + self.hidden_dim, activation=tf.nn.selu)(self.input_encoder)
        net = tf.layers.Dense(self.visible_dim / 4 + self.hidden_dim * 3 / 4, activation=tf.nn.selu)(net)
        net1 = tf.layers.Dense(self.hidden_dim)(net)
        net2 = tf.layers.Dense(self.hidden_dim)(net)
        self.encoder_log_var = net1
        self.encoder_mean = net2

        mc_samples = 5
        sample = tf.reduce_mean(self.sampling(self.encoder_mean, self.encoder_log_var, mc_samples), axis=1)
        sample_eval = tf.reduce_mean(self.sampling(self.encoder_mean, self.encoder_log_var, 1), axis=1)
        
        decoder_layers = [
                tf.layers.Dense(self.visible_dim / 4 + self.hidden_dim, activation=tf.nn.selu),
                tf.layers.Dense(self.visible_dim / 2 + self.hidden_dim, activation=tf.nn.selu),
                tf.layers.Dense(self.visible_dim)
                ]
        
        decoder_lable_layers = [
                tf.layers.Dense(self.class_num * 0.25 + self.hidden_dim * 0.75, activation=tf.nn.selu),
                tf.layers.Dense(self.class_num * 0.75 + self.hidden_dim * 0.25, activation=tf.nn.selu),
                tf.layers.Dense(self.class_num)
                ]
        
        net = sample
        for layer in decoder_layers:
            net = layer(net)
        self.decoder = net
        
        net = sample
        for layer in decoder_lable_layers:
            net = layer(net)
        self.cross_entropy = tf.reduce_sum(tf.nn.softmax_cross_entropy_with_logits(labels=self.input_labels, logits=net))
        self.prob_label = tf.nn.softmax(net)
        
        self.classification_error = tf.reduce_mean(tf.cast(tf.equal(tf.argmax(self.prob_label, axis=1),
                                                                    tf.argmax(self.input_labels, axis=1)), tf.float32))
        self.true_positive = tf.reduce_sum(tf.cast(tf.greater(tf.reduce_sum(self.prob_label[:, 1:]), 0.5), tf.float32) *
            tf.cast(tf.greater(tf.argmax(self.input_labels, axis=1), 0), tf.float32)) / tf.reduce_sum(tf.cast(tf.greater(tf.argmax(self.input_labels, axis=1), 0), tf.float32))
        
        self.false_positive = tf.reduce_sum(tf.cast(tf.greater(tf.reduce_sum(self.prob_label[:, 1:]), 0.5), tf.float32) *
            tf.cast(tf.equal(tf.argmax(self.input_labels, axis=1), 0), tf.float32)) / tf.reduce_sum(tf.cast(tf.equal(tf.argmax(self.input_labels, axis=1), 0), tf.float32))
        
        
        net = sample_eval
        for layer in decoder_layers:
            net = layer(net)
        self.prob = tf.reduce_mean(
                tf.exp(-0.5 * tf.reduce_sum(tf.square(net - self.input_encoder), axis=1)))
        
        self.logprob = -0.5 * tf.reduce_sum(tf.square(net - self.input_encoder), axis=1)
        
        self.decoder_loss = 0.5 * tf.reduce_sum(tf.square(self.decoder  - self.input_encoder))  
        lk_loss =  tf.reduce_sum( 1 + self.encoder_log_var - tf.square(self.encoder_mean) 
            - tf.exp(self.encoder_log_var) ) 
        self.decoder_loss = self.decoder_loss - 0.5 * lk_loss
        #optimizer = tf.train.AdamOptimizer()
        #grads = optimizer.compute_gradients(self.decoder_loss)
        #capped_gvs = []
        #for grad, var in grads:
        #    if grad is not None:
        #        capped_gvs.append((tf.clip_by_value(grad, -1., 1.), var))
        #    else:
        #        capped_gvs.append((grad, var))
        #self.optimizer = optimizer.apply_gradients(grads)
        self.final_loss = self.decoder_loss + self.cross_entropy
        self.optimizer = create_optimizer(self.final_loss, 0.001, 500000, False)
        
    def no_att_prob(self, data):
        [res] = self.session.run([self.prob_label], feed_dict={self.input_encoder : data})
        distances = res[:, 0]
        return distances
            
    
    def fit(self, X, YY, Xt, YYt):
        Xp = X
        Y = np.zeros((X.shape[0], self.class_num), np.float32)
        Yt = np.zeros((Xt.shape[0], self.class_num), np.float32)
        Y[np.arange(Y.shape[0]), YY.astype(np.int32)] = 1
        Yt[np.arange(Yt.shape[0]), YYt.astype(np.int32)] = 1
        self.session = tf.Session()
        self.session.run(tf.global_variables_initializer())
        curr_loss = 0
        tp_a = 0
        fp_a = 0
        for i in range(500000):
            b, bl = self.select_batch(Xp, Y, 1000)
            bt, btl = self.select_batch(Xt, Yt, 500)
            [loss, class_quality, pl] = self.session.run([self.decoder_loss, self.classification_error, self.prob_label], feed_dict={self.input_encoder : bt, 
                self.input_labels: btl})
            if not np.isfinite(loss) or loss > 1e15:
                continue
            [opt]  = self.session.run([self.optimizer],
                feed_dict={self.input_encoder : b, self.input_labels: bl})
            tp = np.sum((np.sum(pl[:, 1:], axis=1) > 0.5) * (np.argmax(btl, axis=1) > 0)) / np.sum(np.argmax(btl, axis=1) > 0)
            fp = np.sum((pl[:, 0] <= 0.5) * (np.argmax(btl, axis=1) == 0)) / np.sum(np.argmax(btl, axis=1) == 0)
            
            alpha = 1.0 / np.float(i + 1)
            if(alpha < self.loss_smooth):
                alpha = self.loss_smooth
            curr_loss = (1 - alpha) * curr_loss + alpha * class_quality
            tp_a = (1 - alpha) * tp_a + alpha * tp
            fp_a = (1 - alpha) * fp_a + alpha * fp
            if i % 50 == 0:
                print([curr_loss, tp_a, fp_a])
 
        probs = self.no_att_prob(Xt)
        dists = -np.log(probs + 0.00000001)
        idx = np.argsort(dists)
        sd = dists[idx]
        Ys = YYt[idx]
        Yidx = Ys > 0
        Yidx = Yidx.astype('float32')
        tn = np.cumsum(1 - Yidx) / np.sum(1 - Yidx)
        tp = (np.sum(Yidx) - np.cumsum(Yidx))/ np.sum(Yidx)
        plt.plot(tn, tp)
        difftn = tn[1:] - tn[:-1]
        auc = np.sum(difftn * tp[1:])
        threshold = np.min(sd[tn > 0.97])
        Yvals = np.unique(YYt)
        Yvals = Yvals[Yvals != 0]
        table = {}
        recall = np.sum((Ys == 0) * (sd < threshold)) / np.sum((Ys == 0))
        tpv = np.sum((Ys > 0) * (sd >= threshold)) / np.sum((Ys > 0))
        precision = np.sum((Ys == 0) * (sd < threshold)) / np.sum(sd < threshold)
        table[0] = [auc, recall, tpv, precision]
        for y in Yvals:
            tpvy = np.sum((Ys == y) * (sd >= threshold)) / np.sum(Ys == y)
            Ysy = Yidx[(Ys == y) + (Ys == 0)]
            tny = np.cumsum(1 - Ysy) / np.sum(1 - Ysy)
            tpy = (np.sum(Ysy) - np.cumsum(Ysy))/ np.sum(Ysy) 
            difftn = tny[1:] - tny[:-1]
            aucy = np.sum(difftn * tpy[1:])
            table[y] = [tpvy, aucy]
        return table
            
            
        