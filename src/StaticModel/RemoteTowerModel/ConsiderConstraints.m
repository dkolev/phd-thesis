function [Aeq_new, beq_new, ProtectionRequired] = ConsiderConstraints(ImpactMatrix, ImpactUpperBound, Aeq, beq, ...
    Ddim, pdim, xDim, zDim, nuDim, Kdim, yDim)

CritNum = length(ImpactUpperBound);
tmp = reshape(ImpactUpperBound, [1,1, CritNum]);
tmp = repmat(tmp, size(ImpactMatrix, 1), size(ImpactMatrix, 2));

ProtectionRequired = sum(ImpactMatrix >= tmp, 3);
ProtectionRequired = ProtectionRequired > 0;

IDs = ProtectionRequired(:);
if(~isempty(IDs)) %label should be about nu, not p!
    
    NewRows = eye(nuDim);
    NewRows = NewRows(logical(IDs), :);
    NewLimNum = size(NewRows, 1);

    Aeq_new = [zeros(NewLimNum, Ddim+pdim+xDim+zDim), NewRows, zeros(NewLimNum, Kdim+yDim)];
    beq_new = zeros(NewLimNum, 1); %because it is nu!!!!

    Aeq_new = [Aeq; Aeq_new];
    beq_new = [beq; beq_new];
else
    Aeq_new = Aeq;
    beq_new = beq;
end


end