function [ OptimalImpact, OptimalAttack ] = CalculateImpact( ExpandedGraphStructure, Impact, D, K, Corresp, ExtendedAssetID, StartID, StopID )
%UNTITLED10 Summary of this function goes here
%   Detailed explanation goes here
CuttedGraph = GetExpGraphWithControls(ExpandedGraphStructure, D, Corresp, K, ExtendedAssetID);

Weights = zeros(size(CuttedGraph, 1));

Weights(StopID,  ExtendedAssetID(:)) = 1./Impact(:);

tmp = CuttedGraph';
Weights = Weights';

Weights = Weights(logical( tmp(:) ));
[dist, OptimalAttack] = graphshortestpath(sparse(CuttedGraph'), StartID, StopID, 'Weights', Weights);

OptimalImpact = 1/dist;


end

