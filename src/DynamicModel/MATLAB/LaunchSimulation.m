function [ p, betta, mu ] = LaunchSimulation(paths, types, pathID, toWrite)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
% Convention - detector is placed on the second (!) node of the expanded
% graph

load('detectionData.mat');
airportImage = imread('templateAirport.png');
if(toWrite)
    vw = VideoWriter('AEPM_video3.avi');
    open(vw);
end
RegNodesNum = size(ThreatGraph, 1);
ExpandNodesNum = size(ExpandedGraphStructure, 1);

[~, initPath] = ConvertExpandPathList( paths, Corresp,ExpandedAssetID, ExpandedEnterID,...
AssetID', EnterID', StartID, StopID, ExpandNodesNum );

% Setup detectors
[EventDetectorsFP, EventDetectorsTP, ExpEventDetectorsFP, ExpEventDetectorsTP, linkedNodes] = ...
    SetupDetectorsAirportModel(Corresp);

%SetupSecuritySkills; 

% Draw graph
[handle, GraphStructure] = ShowGraph( ThreatGraph, EnterID, CyberNodes, AssetID, [] );
context.oldNode = [];

[ ~, airportImageContext ] = InitAirportDrawing( airportImage );

[SecurityControlSkills, SecurityControlsExpanded] = ...
    SetupSecuritySkills(Corresp, RegNodesNum, ExpandNodesNum, AttackTypesNum);

%setup predictor parameters
betta = 1;
nu_s = 1;
mu = 0;
p = ones(size(paths, 1), 1) / size(paths, 1); % no prior knowledge of attack

iterNum = 0;
simulateAttack = pathID > 0;
currSA = [];
if(simulateAttack)
    currPath = paths(pathID, :);
    currPathInit = initPath(pathID, :);

    currType = types(pathID);

    cutInitPath = currPathInit(currPathInit > 0);
    handle = DrawPath(handle, cutInitPath);

    pathLenght = find(currPath == 0, 1, 'first');
    iterNum = pathLenght;
else
   
    iterNum = 5000;
    
end

for i = 1:iterNum
    
    redPath = zeros(0, size(paths, 2));
    yellowPath = zeros(0, size(paths, 2));
    detectedNodes = [];
    if(simulateAttack)
    
        currNode = currPath(i);
        currInitNode = currPathInit(i);


        IDX = find(ExpEventDetectorsTP(:, 1) == currNode);
        for j = 1:length(IDX)
            tpRate = ExpEventDetectorsTP(IDX(j), 2);
            ifDetect = tpRate > rand(1);
            if(ifDetect)
                node = ExpEventDetectorsTP(IDX(j), 1);
                [r, ~] = find(linkedNodes == node);
                for jj = 1:length(r)
                    detectedNodes = [detectedNodes, linkedNodes(r(jj), linkedNodes(r(jj), :) > 0 )];
                end
            end
        end
    end
    falseDetectionNodesP = find(ExpEventDetectorsFP(:, 2) > rand(size(ExpEventDetectorsFP, 1), 1));
    falseDetectionNodesP = ExpEventDetectorsFP(falseDetectionNodesP, 1);
    falseDetectionNodes = [];
    for j = 1:length(falseDetectionNodesP)
        node = falseDetectionNodesP(j);
        [r, ~] = find(linkedNodes == node);
        for jj = 1:length(r)
            falseDetectionNodes = [falseDetectionNodes, linkedNodes(r(jj), linkedNodes(r(jj), :) > 0 )];
        end
    end
    FalseDetectionNodes = setdiff(falseDetectionNodes, detectedNodes);
    % random permutation
    allDetections = [FalseDetectionNodes, detectedNodes];
    allDetections = allDetections(randperm(length(allDetections)));
    for j = 1:length(allDetections)
        IDX = find(ExpEventDetectorsFP(:, 1) == allDetections(j));
        dFP = ExpEventDetectorsFP(IDX, 2);
        dTP = ExpEventDetectorsTP(IDX, 2);
        for jj = 1:length(IDX)
            [ m_new, betta_new, p_new ] = ...
                updateDistribution( mu, betta, nu_s, p, allDetections(j), dFP(jj), dTP(jj), paths, SecurityControlsExpanded, types );
        
            mu = m_new;
            betta = betta_new;
            p = p_new;
        end
    end
    initDet = [];
    % select 'red' and 'yellow' paths
    % TODO: this approach is idiotic. It should depend on the expected FP rate for path. Change it, bastard!!
    if(simulateAttack)
        idxRed = p == max(p);
        redPath = initPath(idxRed, :);
        redTypes = types(idxRed);
        %disp(['Attack Type Probability: ' num2str(sum(p(types == currType)))]);
        if(currInitNode ~= 0)
           disp(currInitNode);
           %[ context, handle ] = RedrawDetections( handle, context, allDetections, Corresp );
           [ context, handle ] = RedrawGraph( handle, redPath, yellowPath, context, currInitNode, allDetections, Corresp  );
           [ airportImageContext ] = UpdateAirportImage( airportImageContext, currInitNode );
           initDet = GetInitDetectionList( allDetections, Corresp, AttackTypes );
           currSA = GetAttacksSummary(redPath, redTypes, AttackTypes);
        end
        if(toWrite)
            img = GetBiographPicture;
            frame = ProduceFrame(airportImageContext, img, currSA, initDet);
            for j = 1:25
                writeVideo(vw, frame);
            end
        end
        pause(0.1);
    else
       disp(-p(p > 0)' * (log(p(p > 0)))); 
       %pause(0.1);
    end
    %
end
if(toWrite)
    close(vw);
end
end

function [EventDetectorsFP, EventDetectorsTP, ExpEventDetectorsFP, ExpEventDetectorsTP, linkedNodes] = ...
    SetupDetectorsAirportModel(corresp)
%setup detectors and translate them to expanded graph

LogDetectorFP = [0.001, 0, 0, 0.001, 0, 0];
LogDetectorTP = [0.8, 0, 0, 0.6, 0, 0];

CameraDetectorFP = [0.001, 0.001, 0.001, 0, 0.001, 0];
CameraDetectorTP = [0.7, 0.9, 0.8, 0, 0.9, 0];

SignalDetectorFP = [0, 0, 0.003, 0, 0, 0.003];
SignalDetectorTP = [0, 0, 0.9, 0, 0, 0.9];

CameraNodes = [19:31, 36:38];
SignalNodes = [28:31, 36:39, 41:44];
LogNodes = [58, 60:64, 66:69];
EventDetectorsFP = ...
    [CameraNodes', CameraDetectorFP(ones(length(CameraNodes), 1), :);...
    SignalNodes', SignalDetectorFP(ones(length(SignalNodes), 1), :);...
    LogNodes', LogDetectorFP(ones(length(LogNodes), 1), :)];

EventDetectorsTP = ...
    [CameraNodes', CameraDetectorTP(ones(length(CameraNodes), 1), :);...
    SignalNodes', SignalDetectorTP(ones(length(SignalNodes), 1), :);...
    LogNodes', LogDetectorTP(ones(length(LogNodes), 1), :)];

[ExpEventDetectorsFP, ExpEventDetectorsTP, linkedNodes] = ...
    ExpandEventDetetors(EventDetectorsFP, EventDetectorsTP, corresp);

end

function [ExpEventDetectorsFP, ExpEventDetectorsTP, linkedNodes] = ExpandEventDetetors(EventDetectorsFP, EventDetectorsTP, corresp)
ExpEventDetectorsFP = zeros(0, 2);
ExpEventDetectorsTP = zeros(0, 2);
N = size(EventDetectorsFP, 1);
linkedNodes = zeros(N, size(corresp{1, 2}, 1)); %matrix with NumAttackTypes columns
nodeList = cell2mat(corresp(:, 1));
for i = 1:N
   currNode = EventDetectorsFP(i, 1);
   detectableAttacks = find(EventDetectorsTP(i, 2:end) > 0);
   ID = find(nodeList == currNode);
   tmp = corresp{ID, 2}; %matrix of expanded links that correspond to the initial node with the detector
   detectNodes = tmp(detectableAttacks, 2);
   ExpEventDetectorsFP = [ExpEventDetectorsFP; [detectNodes, EventDetectorsFP(i, detectableAttacks + 1)']];
   ExpEventDetectorsTP = [ExpEventDetectorsTP; [detectNodes, EventDetectorsTP(i, detectableAttacks + 1)']];
   linkedNodes(i, detectableAttacks) = detectNodes';
end

end

function [ExpControls] = ExpandSecurityControls(Controls, corresp, ExpandNodesNum)
%Controls - matrix with number of rows equal to InitNodesNum, number of
%columns - AttackTypeNum
ExpControls = zeros(ExpandNodesNum, 1);
IDX = find(sum(Controls, 2) > 0);

correspNodes = cell2mat(corresp(:, 1));

for i = 1:length(IDX)
   currNode = IDX(i);
   correspIDX = find(correspNodes == currNode);
   detectableAttacks = find(Controls(currNode, :) > 0);
   tmp = corresp{correspIDX, 2}; %matrix of expanded links that correspond to the initial node with the detector
   expNodes = tmp(detectableAttacks, 2); %convention - second node is used for control placement
   expSkills = Controls(currNode, detectableAttacks)';
   ExpControls(expNodes) = expSkills;
end

end

function [SecurityControlSkills, SecurityControlsExpanded] = SetupSecuritySkills(Corresp, InitNodesNum, ExpandNodesNum, AttackNums)

SecurityControlSkills = zeros(InitNodesNum, AttackNums);
% setup security
%                         CYB PHY INT CYB_I PHY_I INT_I
SecurityControlSkills(19, :) = [0 5 3 0 1 1];
SecurityControlSkills(20, :) = [0 4 2 0 1 1];
SecurityControlSkills(22, :) = [0 5 4 0 2 2];
SecurityControlSkills(28, :) = [2 6 2 0 4 2];
SecurityControlSkills(30, :) = [2 6 4 0 4 2];
SecurityControlSkills(58, :) = [3 0 0 2 0 0];
SecurityControlSkills(63, :) = [4 0 0 2 0 0];
SecurityControlSkills(61, :) = [6 0 0 4 0 0];
SecurityControlSkills(60, :) = [6 0 0 4 0 0];
SecurityControlSkills(19, :) = [0 5 3 0 1 1];
SecurityControlSkills(57, :) = [3 3 3 1 3 2];
SecurityControlSkills(43, :) = [2 3 3 1 3 2];

SecurityControlsExpanded = ExpandSecurityControls(SecurityControlSkills, Corresp, ExpandNodesNum);

end

function ControlStrengthPerPath = GetControlStrengthPerPath(path, attackTypes, controls)
% builds a matrix which represents for each path for each node of the path
% the skill of the  corresponding control. Attack types are taken into
% account.
ControlStrengthPerPath = zeros(size(path));
controlsAugm = [controls; zeros(1, size(controls, 2))];
attackTypesUnique = unique(attackTypes);
for i = 1:length(attackTypesUnique)
    IDX = (attackTypes == attackTypesUnique(i));
    ControlStrengthPerPath(IDX, :) = controls;
end

end
