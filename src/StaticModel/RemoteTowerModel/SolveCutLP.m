function [ D, p, x, z, nu, K, y fval, IsFeasible ] = SolveCutLP( f, Aineq, bineq, Aeq, beq, Ddim, pdim, xDim, zDim, nuDim, Kdim, yDim)
%UNTITLED11 Summary of this function goes here
%   Detailed explanation goes here
ctype = repmat('B', 1, length(f));
ctype((Ddim + pdim + xDim + 1):(Ddim + pdim + xDim + zDim)) = 'C';
[yy,fval]= cplexmilp(f, Aineq, bineq, Aeq, beq, [], [], [], [], [], ctype); 
IsFeasible = true;
if(~isempty(yy))
    D = yy(1:Ddim); %where the controls should be placed
    p = yy((Ddim+1):(Ddim+pdim)); % node labels
    x = yy((Ddim+pdim+1):(Ddim+pdim+xDim)); %which controls are selected
    z = yy((Ddim+pdim+xDim+1):(Ddim+pdim+xDim+zDim)); %optimization criteria
    nu = yy( (Ddim+pdim+xDim+zDim+1):(Ddim+pdim+xDim+zDim+nuDim)); %slack variables
    K = yy((Ddim+pdim+xDim+zDim+nuDim+1):(Ddim+pdim+xDim+zDim+nuDim+Kdim)); %how procedural controls are impacted
    y = yy((Ddim+pdim+xDim+zDim+nuDim+Kdim+1):(Ddim+pdim+xDim+zDim+nuDim+Kdim+yDim)); %which procedural controls should be placed
else
    D = [];
    p = [];
    x = [];
    z = [];
    nu = [];
    K = [];
    y = [];
    IsFeasible = false;
end

end

