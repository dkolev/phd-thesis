ImpactMatrix = CalculateObjectiveFunctions(RMTsystemGraph,  SupportingAssetStructure, Sources, Control, SourceFrameSize, ControlFrameSize, maxFrameSize,...
SourceDataRate, ControlDataRate, SourceLatLimitations, SourceCapLimitations, ControlLatLimitations, ControlCapLimitations,...
SourceTRLimitations, ControlTRLimitations, SafetyDataAssets, CapacityDataAssets, 3);

ImpactMatrix = [ImpactMatrix, ImpactMatrix];

CyberControls = 1;
GeoControls = 2:5;
CloseToAssetsGeoControls = 5;

CheckPointControlTypes = 5;
ProceduralControlTypes = 1;
AttackTypes = 6;

CreateThreatGraphForRMT();

AssetID = 1:16;
EnterID = 17:19;

CloseToAssetPoints = sum(ThreatGraphRMT(AssetID, :), 1)>0;

ChckPntControlMat = ones(size(ThreatGraphRMT, 1), CheckPointControlTypes);
ChckPntControlMat(~CloseToAssetPoints, CloseToAssetsGeoControls) = 0;
ChckPntControlMat(CyberNodes, GeoControls) = 0;
ChckPntControlMat(GeoNodes, CyberControls) = 0;
ChckPntControlMat([AssetID, EnterID], :) = 0;
ChckPntControlMat([AssetID, EnterID], :) = [];

ThreatControlMat = [1, 0, 0, 0, 1;...
                    0, 1, 1, 1, 1;...
                    0, 1, 1, 0, 1;...
                    0, 0, 0, 0, 0;...
                    0, 0, 1, 0, 1;...
                    0, 0, 1, 0, 1];
OnceControlCost = [150, 0, 35000, 0, 0];
PermanentControlCost = [50, 10000, 10000, 10000, 10000];

ProceduralControl = [zeros(length(AssetID), 3), ones(length(AssetID), 3)];

ProcControlCost = 15000;

PossibleTypesOfAttacksMatrix = [0, 1;
                                1, 0;
                                1, 0;
                                0, 1;
                                1, 0;
                                1, 0];

InitSecurityModel(ImpactMatrix, ThreatGraphRMT, AssetID, EnterID, ChckPntControlMat, ThreatControlMat, OnceControlCost,...
    PermanentControlCost, ProceduralControl, ProcControlCost, GeoNodes, CyberNodes, PossibleTypesOfAttacksMatrix);
