package GammaXmlMessages;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public abstract class MessagePayload extends XmlElementBuilder
{
	public static final String payloadElementName = "payload";
	
	public abstract Element serialisePayload(Document doc);
	
	protected Element setupRootElement(Document doc) throws ParserConfigurationException
	{
		Element root = doc.createElement(payloadElementName);
			
		return root;	
	}
	
	public static MessagePayload createInstanceFromElement(Element elem) throws Exception
	{
		Node subNode = elem.getFirstChild();
		if(subNode.getNodeType() == Node.ELEMENT_NODE)
		{
			int numMatches = 0;
			Element subElement = (Element)subNode;
			String currTag = subElement.getTagName();
			if(currTag.equals(IdsAlarmPayload.alarmElementName))
			{
				numMatches++;
				return new IdsAlarmPayload(subElement);
			}
			
			if(numMatches == 0)
			{
				throw new Exception("Non-implemented payload type!");				
			}
		}
		else
		{
			throw new Exception("Payload not well-formed!");
		}
		return null;
	}
	
}
