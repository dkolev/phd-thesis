function frame = ProduceFrame( airportImageContext, graphImg, SA, detectionList )

geoImg = airportImageContext.CurrImage;
if(size(geoImg, 1) > size(graphImg, 1))
   N = size(graphImg, 2);
   diffM =  size(geoImg, 1) - size(graphImg, 1);
   graphImg = [graphImg; 255 * ones(diffM, N, size(graphImg, 3))];
else
   N = size(geoImg, 2);
   diffM =  - size(geoImg, 1) + size(graphImg, 1);
   geoImg = [geoImg; 255 * ones(diffM, N, size(geoImg, 3))];    
end

frame = [geoImg, graphImg];
textPart = uint8(255 * ones(size(frame, 1), 300, 3));
if(~isempty(SA))
    startPos = [5, 5];
    %textPart = 255 * ones(size(frame, 1), 300, 3);
    for i = 1:size(SA, 1)
        currPos = startPos + [0, (i-1) * 40];
        textPart = insertText(textPart, currPos, ['Supporting Asset ' num2str(SA{i, 1}), ' :'],...
            'FontSize', 14, 'BoxOpacity', 1, 'BoxColor', 'white');
        
        descrPos = currPos + [0, 20];
        currAttacks = SA{i, 2};
        text = [];
        for j = 1:length(currAttacks)
            text = [text, ' ', currAttacks{j}];
        end
        textPart = insertText(textPart, descrPos, text, ...
            'FontSize', 14, 'BoxOpacity', 1, 'BoxColor', 'white');
    end
    
end
frame = [frame, uint8(textPart)];

upperPart = 255 * ones(300, size(frame, 2), size(frame, 3));
currPos = [10, 10];
upperPart = insertText(upperPart, currPos, 'Current Detections :',...
            'FontSize', 14, 'BoxOpacity', 1, 'BoxColor', 'white');
for i = 1:size(detectionList)
    currPos = currPos + [0, 20];
    
    text = ['Node ', num2str(detectionList{i, 1}), ' attack types : '];
    attTypes = detectionList{i, 2};
    for j = 1:length(attTypes)
        text = [text, ' ',  attTypes{i}]; 
    end
    upperPart = insertText(upperPart, currPos, text,...
            'FontSize', 14, 'BoxOpacity', 1, 'BoxColor', 'white');    
end

end