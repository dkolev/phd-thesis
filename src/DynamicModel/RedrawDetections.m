function [ context, handle ] = RedrawDetections( handle, context, allDetections, corresp )
%
oldNodes = zeros(1, length(allDetections));
LineWidth = zeros(1, length(allDetections));
LineColor = zeros(length(allDetections), 3);

for i = 1:length(allDetections)
   
     node = getInitNode(allDetections(i), corresp);
     if(ismember(node, oldNodes))
        continue; 
     end
     if(node == 0)
         continue;
     end
     oldNodes(i) = node;
     LineWidth(i) = get(handle.Nodes(node), 'LineWidth');
     LineColor(i, :) = get(handle.Nodes(node), 'LineColor');
     
     set(handle.Nodes(node), 'LineWidth', 2);
     set(handle.Nodes(node), 'LineColor', [1, 0, 0]);
    
end
LineWidth = LineWidth(oldNodes > 0);
LineColor = LineColor(oldNodes > 0, :);
oldNodes = oldNodes(oldNodes > 0);

context.detections.oldNodes = oldNodes;
context.detections.LineWidth = LineWidth;
context.detections.LineColor = LineColor;

end

function initNode = getInitNode(expNode, corresp)
initNode = 0;
for i = 1:size(corresp, 1)
   
    if(ismember(expNode, corresp{i, 2}(:)))
        initNode = corresp{i, 1};
        break;
    end
    
end

end

