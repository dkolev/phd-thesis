function [ airportImage, airportImageContext ] = InitAirportDrawing( img )
%
%img = imresize(img, 2);
pix2node = SetPixelNodeCorrespondence( );
%circleInserter = vision.ShapeInserter('Shape', 'Circles');
M = size(pix2node, 1);   
airportImage = img;
airportImage = insertShape(airportImage, 'circle', uint32([pix2node(:, 2:3), 5*ones(M, 1)]));
airportImage = insertShape(airportImage, 'circle', uint32([pix2node(:, 2:3), 6*ones(M, 1)]));
airportImageContext.pix2node = pix2node;
airportImageContext.InitImage = img;
airportImageContext.InitImageWithNodes = airportImage;
airportImageContext.WithLinesImage = airportImage;
airportImageContext.CurrImage = airportImage;
airportImageContext.PrevAdversaryPosition = [];



end

