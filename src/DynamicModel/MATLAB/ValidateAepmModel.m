function [ numPredictedScenarios, numPredictedAssets, isCorrect ] = ValidateAepmModel( scenarioNum, alpha )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
load('DetectionData21022016.mat');
ttt = pths;
[~, ~, ~, numPredictedScenarios, numPredictedAssets, isCorrect] = ...
    LaunchSimulation(ttt, types, scenarioNum, false, false, alpha);
numIter = 20;
for i = 2:numIter
[~, ~, ~, numPredictedScenarios1, numPredictedAssets1, isCorrect1] = ...
    LaunchSimulation(ttt, types, scenarioNum, false, false, alpha);    

numPredictedScenarios = numPredictedScenarios1 + numPredictedScenarios;
numPredictedAssets = numPredictedAssets1 + numPredictedAssets;
isCorrect = isCorrect1 + isCorrect;

end

isCorrect = isCorrect / numIter;
numPredictedScenarios = numPredictedScenarios / numIter;
numPredictedAssets = numPredictedAssets / numIter;

end

