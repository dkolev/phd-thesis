# -*- coding: utf-8 -*-
"""
Created on Wed Jun 12 18:15:24 2019

@author: admin
"""
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
from LAMBOptimiser import LAMBOptimizer, create_optimizer

class VariationalAutoEncoder:
    
    encoder_log_var = None
    encoder_mean = None
    
    input_encoder = None
    input_decoder = None
    decoder = None
    decoder_loss = None
    
    prob = None
    logprob = None
    optimizer = None
    
    hidden_dim = None
    visible_dim = None
    session = None
    
    def select_batch(self, X, batch_size):
        idx = np.random.randint(0, X.shape[0], batch_size)
        return X[idx, :]
    
    def __init__(self, hidden_dim, input_dim):
        self.hidden_dim = hidden_dim
        self.visible_dim = input_dim
        self.loss_smooth = 0.001
        self.create_encoder_decoder()
    
    
    def sample_size(self, z_mean, z_log_var, nsamples):
        epsilon = np.random.multivariate_normal(z_mean, np.diag(np.exp(0.5 * z_log_var)), nsamples)
    
    def sampling(self, z_mean, z_log_var, n_samples):
        epsilon = tf.random.normal([tf.shape(z_mean)[0], n_samples, self.hidden_dim])
        z_mean_r = tf.tile(tf.expand_dims(z_mean, 1), [1, n_samples, 1])
        z_log_var_r = tf.tile(tf.expand_dims(z_log_var, 1), [1, n_samples, 1])
        return z_mean_r + tf.exp(0.5 * z_log_var_r) * epsilon
    
    def create_encoder_decoder(self):
        self.input_encoder = tf.placeholder(tf.float32, [None, self.visible_dim])
        self.input_decoder = tf.placeholder(tf.float32, [None, self.hidden_dim])
        net = tf.layers.Dense(self.visible_dim / 2 + self.hidden_dim, activation=tf.nn.selu)(self.input_encoder)
        net = tf.layers.Dense(self.visible_dim / 4 + self.hidden_dim * 3 / 4, activation=tf.nn.selu)(net)
        net1 = tf.layers.Dense(self.hidden_dim)(net)
        net2 = tf.layers.Dense(self.hidden_dim)(net)
        self.encoder_log_var = net1
        self.encoder_mean = net2

        mc_samples = 5
        sample = tf.reduce_mean(self.sampling(self.encoder_mean, self.encoder_log_var, mc_samples), axis=1)
        sample_eval = tf.reduce_mean(self.sampling(self.encoder_mean, self.encoder_log_var, 1), axis=1)
        
        decoder_layers = [
                tf.layers.Dense(self.visible_dim / 4 + self.hidden_dim, activation=tf.nn.selu),
                tf.layers.Dense(self.visible_dim / 2 + self.hidden_dim, activation=tf.nn.selu),
                tf.layers.Dense(self.visible_dim)
                ]
        net = sample
        for layer in decoder_layers:
            net = layer(net)
        self.decoder = net
        
        net = sample_eval
        for layer in decoder_layers:
            net = layer(net)
        self.prob = tf.reduce_mean(
                tf.exp(-0.5 * tf.reduce_sum(tf.square(net - self.input_encoder), axis=1)))
        
        self.logprob = -0.5 * tf.reduce_sum(tf.square(net - self.input_encoder), axis=1)
        
        self.decoder_loss = 0.5 * tf.reduce_sum(tf.square(self.decoder  - self.input_encoder))  
        lk_loss =  tf.reduce_sum( 1 + self.encoder_log_var - tf.square(self.encoder_mean) 
            - tf.exp(self.encoder_log_var) ) 
        self.decoder_loss = self.decoder_loss - 0.5 * lk_loss
        optimizer = tf.train.AdamOptimizer()
        grads = optimizer.compute_gradients(self.decoder_loss)
        capped_gvs = []
        #for grad, var in grads:
        #    if grad is not None:
        #        capped_gvs.append((tf.clip_by_value(grad, -1., 1.), var))
        #    else:
        #        capped_gvs.append((grad, var))
        #self.optimizer = optimizer.apply_gradients(grads)
        self.optimizer = create_optimizer(self.decoder_loss, 0.001, 500000, False)
        
    def distance(self, data):
        distances = np.zeros((data.shape[0],))
        for i in range(data.shape[0]):
            sample = np.reshape(data[i, :], (1, self.visible_dim)) 
            res = self.session.run([self.logprob], feed_dict={self.input_encoder : sample})
            distances[i] = res[0]
        return -distances
            
    
    def fit(self, X, Y):
        Xp = X[Y == 0, :]
        self.session = tf.Session()
        self.session.run(tf.global_variables_initializer())
        curr_loss = 0
        for i in range(200000):
            b = self.select_batch(Xp, 1000)
            [loss] = self.session.run([self.decoder_loss], feed_dict={self.input_encoder : b})
            if not np.isfinite(loss) or loss > 1e15:
                continue
            [opt]  = self.session.run([self.optimizer], feed_dict={self.input_encoder : b})
            alpha = 1.0 / np.float(i + 1)
            if(alpha < self.loss_smooth):
                alpha = self.loss_smooth
            curr_loss = (1 - alpha) * curr_loss + alpha * loss
            if i % 50 == 0:
                print(curr_loss)
 
        dists = self.distance(X)
        idx = np.argsort(dists)
        sd = dists[idx]
        Ys = Y[idx]
        Yidx = Ys > 0
        Yidx = Yidx.astype('float32')
        tn = np.cumsum(1 - Yidx) / np.sum(1 - Yidx)
        tp = (np.sum(Yidx) - np.cumsum(Yidx))/ np.sum(Yidx)
        plt.plot(tn, tp)
        difftn = tn[1:] - tn[:-1]
        auc = np.sum(difftn * tp[1:])
        threshold = np.min(sd[tn > 0.95])
        Yvals = np.unique(Y)
        Yvals = Yvals[Yvals != 0]
        table = {}
        recall = np.sum((Ys == 0) * (sd < threshold)) / np.sum((Ys == 0))
        tpv = np.sum((Ys > 0) * (sd >= threshold)) / np.sum((Ys > 0))
        precision = np.sum((Ys == 0) * (sd < threshold)) / np.sum(sd < threshold)
        table[0] = [auc, recall, tpv, precision]
        for y in Yvals:
            tpvy = np.sum((Ys == y) * (sd >= threshold)) / np.sum(Ys == y)
            Ysy = Yidx[(Ys == y) + (Ys == 0)]
            tny = np.cumsum(1 - Ysy) / np.sum(1 - Ysy)
            tpy = (np.sum(Ysy) - np.cumsum(Ysy))/ np.sum(Ysy) 
            difftn = tny[1:] - tny[:-1]
            aucy = np.sum(difftn * tpy[1:])
            table[y] = [tpvy, aucy]
        return table
            
            
        