\input{report_header}

\section{Research Context and Goal}
\indent Historically, critical infrastructures relied on bespoke software, and because they were loosely integrated, this limited the potential impact of knock-on effects between different systems. Those days are gone. Now, most industries make extensive use of commercial ``off-the-shelf" systems, which are not specifically intended for security-related applications. These systems include Linux, Voice over Internet Protocol (VoIP), and augmentation systems for GPS, the strengths and weaknesses of which are known by a growing number of attackers. At the same time, a number of programmes are increasing interdependencies both within and between infrastructures.\\ 
Recent large-scale programmes in air traffic management (ATM), including the Single European Sky ATM Research (SESAR) programme in Europe, along with the Next Generation Air Transportation System (NextGen) in the United States, illustrate the abovementioned points. In both cases, these programmes are readying novel operational concepts and technological enablers \cite{citeulike:5484296,6980330,6218393}. These concepts rely on large-scale data transfers across national boundaries and between a host of integrated systems. Regulatory organisations are gaining an awareness of potential threats and vulnerabilities, as reflected in the attempts of the Federal Aviation Administration and the European Aviation Safety Agency to establish ATM security as an integral part of the system engineering process. A brief description of these broad subjects is given in this report, as they were mostly discussed during the first viva in relation to the static model.\\
\indent This document addresses the challenge of supporting security risk assessment (SRA), as well as the selection of control and event detectors, in large-scale engineering programmes. Current practices and standards for security risk management involve the identification of security risks and the implementation of associated controls at a system- or component-level. Risk assessment is typically performed by experts, and it relies on the combined use of qualitative and quantitative methods. However, the higher levels of interconnectivity across infrastructure components require the analysis of threat propagation within and across the associated supply chains. Various security risk management methodologies exist, but few are specifically tailored to the design and development process.\\
\indent The goal of this research is to develop a mathematically-founded method to support SRA in the general case, which involves incorporating the main abstractions defined by the standards. In particular, the approach evaluates the perceived optimality of the security controls. It also assists in the development of ``what-if" simulations in support of risk mitigation decision-making processes. The latter allow for the identification of appropriate controls (or sensors) and their placement within the (sub-)system context. Additionally, guided by the same methodology, a generic approach for situation awareness is developed in this research.
\section{Research Contribution}
\indent In the scope of this research, two main contributions are offered. The first is a generic framework for the SRA domain, which is linked to the existing SRA framework. The framework strictly translates the main entities used within the Security Risk Assessment Methodology (SecRAM) into mathematical formalisms, and provides a basis for building a graph-based model. The construction methodology for the graph-based model is presented. The process of defining and selecting security controls is represented as a multi-objective zero-sum security game, which is converted to a mixed-integer linear programming problem. In this research, the security game is referred to as a static model, and it was a subject of analysis during the previous viva. 
\indent Over the framework designed in the first part of the document, a method for situation awareness analysis is presented, which may be regarded as a logical continuation of the static model. This deals with the installation tasks, whereas in the second part, online data stream processing generic models are proposed and derived from standard Bayesian frameworks. Bayesian frameworks are selected as they provide a set of standard methodologies for decision-making and state estimation under uncertainty and noisy data. These models are the main focus of the report, and they are described in due course.

\section{Static Model}
\indent This research proposes the use of graph-based formalisation for generic threat analysis, considering a high-level system view, as is done in SRA standards. An attack scenario is represented as a directed path linking the intended target (or ``supporting asset" (SA), following SecRAM definitions) with the exploited vulnerability. Therefore, threat paths are the joint set of all adversary actions considered in a given scenario. An adversary’s behaviour is modelled as a selection process in an optimal game strategy, employing game theory formalism.\\
\indent It is useful to conceptualise the proposed modelling scheme as an extension of the domain used in network security games (NSG) \cite{Bell:2008:10.1098/rsta.2008.0019}. The approach described in this document defines a regular way to extend the classical NSG to a multi-objective game with multiple types of attack against multiple targets, demonstrating an alignment with basic SRA procedures.\\  
\indent The SecRAM methodology defines four main steps, which are modelled in the next subsections. These steps are the identification of SA and system function (or ``primary asset" (PA), as defined by SecRAM), the definition of threat scenarios, risk evaluation, and the selection of security controls.\\
\indent For systematic reasons, and to achieve closer alignment with existing SRA procedures, the proposed approach is separated into the following two sub-models:
\begin{itemize}
	\item Asset Model: This aims to represent the structure of the system that is to be protected, its main assets, and its functions.
	\item Threat Model: This is related to the definition of the main threats and the sequence of actions that an adversary undertakes. 
\end{itemize}
\indent Each model is based on graphs. To avoid duplication of the content reported on in the author’s prior research, only the threat model is described. A description of this model is also relevant given that it is used in the description of the dynamic model.

\subsection{Threat Model}
\indent The threat scenario definition phase aims to determine the system’s main vulnerabilities, and to identify a constructive approach for their exploitation. The term ``threat scenario" defines a chain or series of events that is initiated by a threat and that may lead to an unwanted incident. These chains of events are formalised in the proposed methodology as a directed graph.\\
\indent To give a short description of the approach, an adversary’s strategy involves selecting a pair of SAs and a specific type of attack to use against the target. Following the NSG model, the adversary selects a path in the graph $G=(N,E)$ from an ``entry node" to the target. This graph encodes the main steps that the adversary must carry out in order to perform an attack against the selected target(s) within the system. For instance, this may include compromising physical security by opening doors or gaining unauthorised access to a computer. A subset of the nodes of $G$ is mapped to the set of SAs, thus defining the set of target nodes. The resulting graph is denoted the threat path graph (TPG).\\ 

\section{Dynamic Model} \label{general}
\indent A brief description of the static model was offered in the previous section. The aim of the static model is to perform the optimal assignment of security controls within the system. However, the static model neglects to consider ongoing development within the system and the timeline, which may be useful for continuous monitoring and situation awareness.\\
\indent As the first step, it is necessary to provide a basis for this section’s discussion by defining the term ``situation awareness". As defined in \cite{Gilson_95_situation_awareness}, the concept of situation awareness, as initially discussed during the First World War by Oswald Boelke, refers to the act of ``gaining an awareness of the enemy before the enemy [gains] a similar awareness, and [devising] methods for accomplishing this." After some time, the definition of situation awareness was expanded at the system level. \cite{Woods:1988} pointed out that, to maintain an adequate awareness of system status, tracking the development of events as they gradually unfold is necessary. In the years since then, situation awareness has been defined differently in the literature, but almost all researchers define an agent environment, and refer to a perception of the goals and current status of the agents in time and space. The dynamic model introduced in the next paragraph addresses the problem of improving situation awareness within a system.\\
\indent The dynamic model has the same grounding as the static model (i.e., the TPG). However, the main focus of the dynamic model is not threat prevention; rather, it is the detection of an ongoing attack, the estimation of the current ``status" of the adversary, and the prediction of the set of possible targets. Therefore, the asset model part is not a significant element of the model’s definition because the impact estimation is not a priority. However, the threat model is altered slightly to fit into the ``event detection" domain. The system under protection is assumed to generate security-relevant information through a set of detectors. This information may be diverse, and in terms of its quality, it may also be heterogenous. Following the definition of situation awareness given above, this research’s operational definition of the main aim of the dynamic model is that it monitors relevant information and, in turn, determines whether the system is under attack. If an attack is identified, then the dynamic model identifies the assets which are the adversary’s potential targets.
\indent Implementation of the dynamic model does not require any change in the approach used for TPG development. Therefore, it may be used in combination (or consequently) in order to accomplish the output of the static model. Additionally, as demonstrated in due course, the dynamic model’s structure can be used to design an optimal placement of \textit{event detectors} within the graph.\\
\indent In what remains of this report, the following are covered:
\begin{itemize}
	\item The main additional entities and notations are introduced, and the questions stated above (i.e., those relating to the question of whether the system is under attack, and – if so – what the adversary’s targets may be) are formalised into specific probability estimation problems.
	\item The dynamic model design and derivation is given for the estimation of the ``state" of the adversary. 
	\item The Bayesian filter-based model for ``under-attack" probability detection is derived, accompanied by theoretical analysis.
	\item Validation of results is provided.
\end{itemize}


\section{Threat Model Modification and Notations}
\indent To use the proposed TPG for the purposes of attack detection and situation awareness, this section introduces several abstract entities.\\
\indent Consider a TPG, as defined in the previous section: $G = (N, A)$, with a set of nodes $N, \space |N| = n$, and a set of arcs $A$. The adjacency matrix of $G$ is denoted as $\mathfrak{A}$. $E \subset N$ is the set of entry points, and $T \subset N$ is the set of targets. In addition, $t$ is used to denote time, and discrete time is considered for modeling purposes. \\
\indent As the main aim of the model is attack detection, it is necessary to introduce the set of problem-relevant parameters which are to be estimated by the model. The adversary’s state space is given by $S = N \cup \{ \emptyset \}$, where the empty set means that the attack has not started. Additionally, the adversary is characterised by his ``skill": $l$. Here, the term ``skill" denotes (a set of) any indirectly observed real-valued variables, which may be used as a ranking criteria of the adversary’s possibilities (the higher the skill, the greater the availability of resources and skills). The definition of the adversary's state may be different for different instantiations of the dynamic model, as illustrated in due course.\\
\indent Let us formalise the information flow from the event detectors that may be received. Any data source that produces any information that characterises the system state may be considered an event detector. For instance, consider two types of information: firstly, an alert from an intrusion detection system; and secondly, a ``door opened" signal. Both signals can be used for incident investigation, and they complement each other. It is clear that the detection rate of ``door-opened" signals is significantly higher than, and is not strictly related to, attack detection. However, if an intrusion does not happen without a door being opened, this signal may be useful in assessing false alerts from an intrusion system.\\
\indent Formally speaking, the graph is equipped with a set of event detectors $\mathfrak{D}$. At each moment of time, the defender receives a set of detections $$D_t = (d_1^t, \dots,  d_{K_t}^t)$$ For each $d_i$, an associated vertex (or subset of vertices, given that the approach is easily extendable) exists, denoted as $v(d_i) = n \in N$. Each detection indicates the possible position of the adversary within the graph. Furthermore, each detector $d$ is characterised by a set of parameters:
\begin{itemize}
	\item True positive detection rate $p_{tp}(d)$: Probability of the detector sending information about an ongoing event in case an attack is happening. 
	\item False positive detection rate $p_{fp}(d)$: Probability of the detector sending information about an ongoing event in case no attack is happening.
\end{itemize}
\indent Returning to the ``door-opened" example, the probability of the door being opened when no attack happens is quite high. Assuming that the respective ``door-opened" detector is somewhat precise (which is a realistic assumption, given the simplicity of the event), the final false positive rate of the event is almost equal to the true positive rate. However, a substantial amount of information may be given in case no ``door-opened" event is recorded, but the intrusion system sends alert signals. This increases the likelihood of false positives compared to the case in which a ``door-opened" event is recorded.\\  
\indent Additionally, a set of security controls is given (placed at the edges of the graph), denoted as $\mathfrak{M}$. The adversary can pass the edge if the skill of the actor is higher than the corresponding ``skill threshold" of the deployed security control. The thresholds of the security controls are encoded in the matrix $W \in \mathbb{R}^{n \times n}$.\\

\subsection{Model Functionality}
\indent In this subsection, the main functionality of the model is formalised to reflect the questions posed in Section \ref{general}. The overall dynamic model is split into the solution of two main problems, as listed further.\\
\indent The \textbf{first} problem that the dynamic model solves may be stated as follows: given the TPG model $G$, a set of event detectors $\mathfrak{D}$, and security controls $\mathfrak{M}$, estimate the following probabilities for each moment of time:
\begin{itemize}
	\item Conditional distribution of the adversary’s state (i.e., $p_t(s_t|D_0^t)$). The state of the adversary is described in Section \ref{pbm}, where a derivation of the given probabilities is provided.
	\item Conditional probability of an attack taking place at a given moment. This estimate may depend on the structure of the TPG and the adversary’s state space. Derivation of the respective probabilities is given in Section \ref{bfm}.
\end{itemize}
\indent The \textbf{second} problem solved by the dynamic model is that of optimal event detector placement. This may be considered part of the static model, but the basis used to define the loss function is the Bayesian filter model, which permits an estimate of the ``under-attack" probability. This is detailed in Section \ref{bfm}. 

\section{Path-based Model} \label{pbm}
\indent In this section, a version of the dynamic model is described where the adversary’s state is given as a set of paths in $G$ from any entry point to a SA. Thus, the problem is to estimate a probability distribution over a set of paths and the skill variable: $p(P(s, v, G), l | D_0^t)$. An additional ``empty" path is introduced to define a ``no-attack" state.\\

\subsection{Model Statement}
\indent  Two main variables are introduced in this subsection that characterise the adversary in the current model.
\indent It is assumed that the skill variable, $s \sim \mathcal{N}(s|\mu,\sigma)$, is normally distributed (further derivation given for a scalar, for a vector with independent components the derivation scheme is the same).\\
\indent In addition, $P ~ (p_1,…,p_M)$ denotes the distribution over a set of paths, where $M$ refers to the number of paths.\\
$SC(P)$ is the set of security controls deployed at path $P$. The skill threshold for a security control $c$ is given by $q(c)$. $SC(P, d), d \in N$ stays for a set of security controls at path $P$ up to node $d$.\\
\indent Let us define the detection likelihood function:
\begin{equation}
	p(d_n=1|s,P) = \begin{cases}
		P_{FP}(d_n), & d_n \notin P \\
		P_{TP}(d_n)I(s \geq \maxl{j \in SC(P, d_n)}q(j)) + \\ \quad + P_{FP}(d_n)I(s \leq \maxl{j \in SC(P, d_n)}q(j)), & d_n \in P 
	\end{cases}
\end{equation}
\indent This formula indicates that, given the selected path $P$ and skill $s$, the probability of a detection occurring at a node $d_n \notin P$ is equal to the probability of false positive detection. If the node $d_n \in P$, the probability of detection is equal to the probability of true positive detection \textbf{only} if the adversary’s skill is high enough to reach the $d_n$ threshold. Otherwise, the probability of detection is equal to the false positive detection probability (coded using the indicator functions).\\
\indent Following Bayes’ formula, it is possible to estimate the posterior probability distribution of $s$ and $P$. For $s$:
\begin{equation} \label{simple_dm_ps_no_approx}
	p(s)= \frac{\sum_P p(d_n=1|s,P) \mathcal{N}(s | \mu,\sigma)p(P)}{Z}
\end{equation}
\begin{equation}
	\begin{multlined}
		Z = \int \sum_P p(d_n=1|s,P) \mathcal{N}(s | \mu,\sigma)p(P) ds = \\
		= \sum_{P : d_n \in P} \int p(d_n = 1|s,P)\mathcal{N}(s | \mu,\sigma)p(P)ds + P_{FP}(d_n)p(P:d_n \notin P) = \\
		= \sum_{P : d_n \in P} \big[P_{TP}(d_n)(1-\Phi(-\frac{\mu - q^*(P)}{\sigma}))+ P_{FP}(d_n)\Phi(-\frac{\mu - q^*(P)}{\sigma})\big]p(P)+ \\ + P_{FP}(d_n)P(P:d_n \notin P)
	\end{multlined}
\end{equation}
$$$$
For $P$:
\begin{equation}
	\begin{multlined}
		p(P)=\int p(d_n=1|s,P)\mathcal{N}(s | \mu,\sigma)ds p(P) = \\
		= \begin{cases}   
			P_{TP}(d_n)(1-\Phi(-\frac{\mu - q^*(P)}{\sigma}))+ P_{FP}(d_n)\Phi(-\frac{\mu - q^*(P)}{\sigma})]p(P),d_n \in P \\
			P_{FP}(d_n)p(P),d_n \notin P) 
		\end{cases}
	\end{multlined}
\end{equation}
\indent The formulas above give rise to an obvious problem: in particular, the normal distribution is transformed into a mixture of truncated normal distributions. The number of components in the distribution is proportional to the number of paths passing through a given node. If Bayes’ rule is applied iteratively through time, the number of components will grow exponentially. To avoid this problem, the use of an approximate inference is recommended, which preserves the distribution family. Formally speaking, the mixture of truncated normal distributions is approximated by a normal distribution.   

\subsubsection{Expectation Propagation}
\indent The expectation propagation (EP) algorithm, initially proposed by \cite{Minka:2001:EPA:647235.720257} for a problem of posterior density approximation, is used for probability density approximation. A typical problem is that, in the general case, the exact posterior probability distribution does not preserve the form of the prior (if the distribution of the conditional variable is not conjugate, which it is not in the general case). EP proposes an algorithm for approximate Bayesian inference, which preserves the distribution family and has a close analytical form for the exponential distribution family, independent from the conditional variable distribution family.\\
\indent EP is considered a classical algorithm, and for this reason, only a motivation for its usage and a high-level overview is given here. Initially, EP was proposed for the approximation of the best posterior distribution, which is exactly the problem considered here. The algorithm was successfully applied in the Microsoft TrueSkill algorithm \cite{Herbrich:2006:TBS:2976456.2976528}, where the ``skill" of each player is estimated and tracked over time, based on the outcomes of different games. This approach is similar in various respects to the methodology proposed here, if we assume that the adversary plays a ``game" with each security control.\\
\indent The idea behind EP is a factorised approximation of the distributions, which can be defined by factor graphs. Consider the initial probability distribution $p(z) = \prod_{j}f_j(z)$, where $f_j$ denotes some function, which depends on a subset of variables. It is possible to approximate the overall probability distribution by a product of factors $Q(z) = \prod_{j}\tilde{f_j}(z)$. This is usually achieved by optimising mutual Kullback-Leibler divergence using variational per-component descent:
\begin{equation}
	\widetilde{f_i^{new}}(z) = \argmin{\tilde{f_i}(z)}Q_{KL}(Z^{-1}f_i(z)\prod_{j \neq i}\tilde{f_j}(z) || Q(z))
\end{equation}
Often the following ``projection" notation is used to represent the optimisation result: 
\begin{equation}
	\widetilde{f_i^{new}}(z) = \frac{proj\big[ f_i(z)\prod_{j \neq i}\tilde{f_j}(z) \big]}{\prod_{j \neq i}\tilde{f_j}(z)}
\end{equation}
\indent The iterative optimisation process may be presented as a message passing algorithm in the factor graph, significantly replicating loopy belief propagation \cite{Bishop:2006:PRM:1162264}. 

\subsection{Derivation} 
\indent In this subsection, an EP algorithm is applied to the equations for $p(s)$ and $p(P)$ \eqref{simple_dm_ps_no_approx}. Consider first $p(s)$. Given that the normal distribution belongs to the family of exponential distributions, it is enough to match the expectation and variance parameters. Let us introduce the following auxiliary functions:
\begin{equation}
	M(a,b,\sigma, \mu)= \mu (\Phi(b,\mu, \sigma) - \Phi(a,\mu,\sigma))+\sigma^2 (\mathcal{N}(a | \mu,\sigma)-\mathcal{N}(b | \mu,\sigma))
\end{equation}
\begin{equation}
	\begin{multlined}
		V(a,b,\sigma,\mu) = \\
		= \sigma^2 \frac{(\Phi(b,\mu,\sigma)-\Phi(a,\mu,\sigma))+((a-\mu)\mathcal{N}(a | \mu,\sigma)-(b-\mu)\mathcal{N}(b | \mu,\sigma))}{\Phi(b,\mu,\sigma)-\Phi(a,\mu,\sigma)} - \\
		- \frac{\sigma^2 (\mathcal{N}(a | \mu,\sigma) - \mathcal{N}(b | \mu,\sigma))^2}{\Phi(b,\mu,\sigma)-\Phi(a,\mu,\sigma)}
	\end{multlined}
\end{equation}
\indent In turn, one can show that the resulting expected value and variance for $p(s)$ are:
\begin{equation}
	\begin{multlined}
		\mu = \mathbb{E}s=\big[P_{FP(d_n)}P(t:d_n \notin P)\mu+ \Sigma_(P:d_n \in P)[P_{TP}(d_n)M(q^* (P),\inf,\mu,\sigma) + \\
		+ P_{FP}(d_n)M(-\inf,q^*(P),\mu,\sigma)]p(P)\big] Z^{-1}
	\end{multlined}
\end{equation}
\indent The derived formulas can be applied to the model. Starting from some initial values of $\mu$ and $\sigma$, and based on the observed event detectors, the values for the expected value and variance of $s$ and the probability distribution of $P$ are updated recursively.


\section{Bayesian Filter Model} \label{bfm}
\indent The dynamic model instantiation described in this section deals with a more complex definition of the adversary’s state. It is assumed that the adversary is moving within the graph according to a random Markov walk. At one step, the adversary may move to the neighbouring nodes of $G$. One may assume uniform probability, but it is possible to embed a ``motivation" parameter, which parameterises the transition matrix.\\
\indent The formalised problem is an estimation of $p_t(v_t, l|D_0^t)$, the probability distribution of the adversary’s position and skill. To introduce a probability of an ``ongoing" attack, an additional node is added to $G$, which will be detailed further.\\
\indent A family of models of this kind is referred to as Bayesian filters (BF), which is a baseline approach used in object tracking problems \cite{Bishop:2006:PRM:1162264}. Taking into account the proposed formalisation of the dynamic model, it may be considered a standard discrete-space tracking problem in the graph, hidden Markov model (HMM). The main difference is due to the fact that a skill estimation is undertaken simultaneously with tracking.  

\subsection{Model Derivation}
\indent In this subsection, the case of a single adversary is detailed. The derivation is carried out using the typical scheme for BF-type models: incremental recursive estimation of the probability distributions using Bayes’ formula. To achieve this, a predictive distribution is initially estimated, after which it is corrected using the data observed at the current moment of time.\\
\indent Consider the following equation:
$$p(v_t, v_{t-1}, l, D_t | D_0^{t-1}) = p(D_t | l, v_t) p(v_t | v_{t-1}, l) p(v_{t-1}, l | D_0^{t-1})$$
\indent Let us define the detection likelihood function as follows:
$$
p(D_t | l, v_t) = \big(\prod_{d \in D_t : v(d) \neq v_t }p_{fp}(d)\big)
\times \big(\prod_{d \in D_t : v(d) = v_t }p_{tp}(d)\big)
\times \big(\prod_{d \notin D_t : v(d) = v_t }(1 - p_{tp}(d))\big)$$
Therefore:
\begin{equation}
	p(v_t, l | D_0^t) \propto p(D_t | l, v_t) \sum_{v_{t-1}}p(v_t | v_{t-1}, l) p(v_{t-1}, l | D_0^{t-1})
\end{equation}
\indent Depending on the selected distributions of $l, p(v_t | v_{t-1}, l)$, different approximations can be used. For discrete-valued $l$, a direct derivation may be used.

\subsection{Model Analysis}
\indent In this subsection, theoretical properties of the stated model are analysed. In addition, ``under-attack" probabilities and estimation variances are derived, and a discussion is given of optimal event detector placement problems. In sequence, two main sub-models are considered:
\begin{itemize}
	\item Model with no false positives: For this model, only missed attack minimisation is performed.
	\item Model with false positives: For this model, we will consider an optimal event detector placement for best false alert filtering with constraints on real attack detection.
\end{itemize}

\subsubsection{Theoretical Analysis of Dynamic Model Framework}
\indent Let us consider the simplified model where no false or true positives are present. In addition, in this model, no security controls exist. In such a scenario, we iterate the system state, as presented before.\\
\indent Let $p_t$ be a vector of $p(v_{t} | D_0^{t})$ for each node of the graph. The transition matrix $A(l)$ is defined by the structure of the graph such that each node of the graph defines a state in the transition matrix. For correctness, we define one additional state (or an additional node) $v_{na}$, which corresponds to the condition where the attack has not started. This node is ``connected" to the entry nodes of the graph and has a self-transition probability $a_{na}$, which defines the probability that the attack will not start during the next time step. This node has no incoming links from the rest of the nodes in the graph. Furthermore, we refer to the augmented state vector $p_t \in \mathbb{R}^{n+1}$, where $p_t(v_{na})$, which is the probability of the adversary being at the additionally introduced node (i.e., no attack is happening and no attack has occurred). At that special node, no event detectors can be installed.\\
\indent The formulas given in the previous section can be rewritten, for the simplified case considered here, using matrix notation:
$$p_t \propto \Gamma * A * p_{t-1} $$
\indent Here, matrix $\Gamma$ is a diagonal matrix with components equal to the false negative probabilities of detection for each node in the graph.\\
\indent Such an equation defines a recurrent definition of the state. We are interested in the static distribution, which defines the probability of the adversary remaining undetected until a given node in the graph, in case of an infinite observation period. One may define a ``restricted" area in the graph such that the probability of the adversary reaching that area undetected is minimal. That subset may be related to some critical infrastructure area, where the system is most vulnerable (e.g., a firewall-protected sub-network).\\
\indent Therefore, we are interested in the formulation $\lambda * p = \Gamma * A * p $, which leads to the eigenvector search for the matrix $\Gamma * A$. Here, it is easy to show that the lemma given below holds, as the proof below indicates.\\
\begin{lemma*} \label{thm:eigen_vector_lemma}
	If $p_{na} \neq 0$, then:
	\begin{itemize}
		\item Matrix $\Gamma * A$ has an eigenvalue equal to $a_{na}$.
		\item If $p$ is a stationary distribution and $p(v_{na}) \neq 0$, then $p$ is right-side eigenvector for the eigenvalue $a_{na}$.
	\end{itemize}
\end{lemma*} 
\begin{proof}
	\indent Let us consider the structure of matrices $A$ and $\Gamma$. As stated previously, the additional ``no-attack" node has no incoming links and has a self-transition probability. This means that the last row of matrix $A$ has one non-zero entry at the diagonal, which is equal to $a_{na}$. $\Gamma$ is a diagonal matrix with entries corresponding to $v_{na}$ equal to 1, and so the structure of the last row of $\Gamma * A$ is the same. Therefore, $a_{na}$ is a solution to the characteristic polynomial for matrix $\Gamma * A$, and it is an eigenvalue.\\
	\indent Consider the stationary distribution $p$, if it exists. Following the same idea, the component corresponding to $v_{na}$ of the vector $(\Gamma * A * p)$ is equal to $a_{na} * p_t(v_{na}) = \lambda * p_t(v_{na})$. Therefore, if $p(v_{na}) \neq 0$, then it is correct that $a_{na} = \lambda$. 
\end{proof}

\indent Figure \ref{fig:fig_propagation} outlines the evolution of $p_t(v_{na})$ through time with the corresponding component of $p(v_{na})$ with $p_{na} = 0.9$ for the TPG from the previous sections. It is important to notice that the assumption is made that the graph has no cycles, except the trivial self-connection for node $v_{na}$. However, the stationary distribution may not exist.\\

\begin{figure}[t]
	\includegraphics[width=1.2\textwidth,center]{figures/convergence.png}
	\caption{Propagation vs eigenvector component}
	\label{fig:fig_propagation}
\end{figure}

%TODO: write an annex

\subsubsection{Impact of False Positive Events}
\indent We consider the model where event detectors may produce false positive events and no attack is ongoing. Figure \ref{fig:fig_propagation_fp} provides an example of the behaviour of $p_t(v_{na})$ for the false positive areas. One can see that the false events force $p_t(v_{na})$ to generate sudden drops, which rapidly disappear (i.e., in 2-6 iterations). However, the value $p_t(v_{na})$ represents the probability that the attack never started. Let us assume that the restricted area is defined as a 2-neighbourhood of the SA nodes. If we consider the same values for the restricted area and the SAs, we can state that:
\begin{itemize}
	\item The baseline value for both of the areas is significantly higher than for $v_{na}$. Thus, the probability that the adversary is not at the restricted area is relatively high.
	\item The drops are significantly less wide and the probability of not being attacked is always higher for the restricted area rather than for the full graph. 
\end{itemize}
\indent Let us analyse the proposed model. For a moment of time $t$, the current distribution of the attacker’s appearance $p_t$ is defined by the sequence of matrices $\Gamma_0, \Gamma_1, ..., \Gamma_t$, where each matrix $\Gamma_i$ is diagonal and represents a list of probabilities of true or false positive detections for each node, as derived earlier. Considering the expected value of $p_t$, the following recurrent formula may be derived:
$$\mathbb{E}p_t \propto \sum_{D_t}p(D_t | v_{na})\Gamma(D_t)A\mathbb{E}p_{t-1}$$
where the sum is taken over all possible configurations of values received from the event detections at time $t$. $\Gamma(D_t)$ represents the corresponding detection matrix for false or true positives. Assuming convergence at $t \rightarrow \inf, p_t \rightarrow p$:
$$\mathbb{E}p \propto \sum_{D_t}p(D_t | v_{na})\Gamma(D_t)A\mathbb{E}p$$
Denote $\tilde{L} = \sum_{D_t | v_{na}}p(D_t)\Gamma(D_t)A$. Thus, we need to consider a similar eigenvector search problem for the definition of the expected value $p$ (and the variance of each component).\\
\indent Considering that no event detector can be placed at the artificial node $v_{na}$, it may be proven – as in Lemma \ref{thm:eigen_vector_lemma} – that the stationary distribution corresponds to the eigenvector of $\tilde{L}$ for the value $a_{na}$. This simply follows from the fact that $\sum_{D_t}p(D_t)a_{na} = a_{na}$.\\
\indent It is possible to simplify the computation of $\tilde{L}$, assuming that the detections are independent. This assumption is correct in the case where no attack is happening:
$$\tilde{L} = \sum_{D_t}p(D_t | v_{na})\Gamma(D_t)A = \big(\sum_{D_t}p(D_t | v_{na})\Gamma(D_t)\big)A = \tilde{\Gamma_t}A $$
\indent Consider the diagonal element at component $j$ of the matrix $\Gamma(D_t)$ in case of a single event detector installed at the node $v_j$:
$$ \sum_{D_t}p(D_t | v_{na})\gamma_{jj}(d_t^j) = \sum_{D_t^{\textbackslash j}}p(D_t^{\textbackslash j} | v_{na}) p(D_t^{\textbackslash j} | v_{j})\big[(1-p_{fp}(v_j))(1 - p_{tp}(v_j)) + p_{fp}(v_j)p_{tp}(v_j)\big]$$
\indent Considering that $p(D_t^{\textbackslash j} | v_{j}) = p(D_t^{\textbackslash j} | v_{na})$ and changing the summation order:
$$ \widetilde{\gamma_{jj}}(d_t^j) = \big[(1-p_{fp}(v_j))(1 - p_{tp}(v_j)) + p_{fp}(v_j)p_{tp}(v_j)\big] \sum_{D_t^{\textbackslash j}}p(D_t^{\textbackslash j} | v_{na})^2 $$
\indent It is easy to show that:
$$\sum_{D_t^{\textbackslash j}}p(D_t^{\textbackslash j} | v_{na})^2 = \prod_{v \in D_t^{\textbackslash j}  }\big(p_{fp}(v)^2 + (1 - p_{fp}(v))^2 \big) $$
\indent Thus, the final equation for multiple event detectors at node $v_j$ is as follows:
$$\widetilde{\gamma_{jj}}(d_t^j) = \prod_{v}\big(p_{fp}(v)^2 + (1 - p_{fp}(v))^2 \big) \prod_{v \notin D_t^{\textbackslash j}}\big[(1-p_{fp}(v))(1 - p_{tp}(v)) + p_{fp}(v)p_{tp}(v)\big] $$


\begin{figure}[t]
	\centering
	\includegraphics[width=1.2\textwidth,center]{figures/false_positive_areas.png}
	\caption{Propagation vs eigenvector component with false positives}
	\label{fig:fig_propagation_fp}
\end{figure}

\subsubsection{Optimal Event Detector Placement}
\indent In this section, the optimisation problem for optimal event detector placement with respect to the two main objectives is stated:
\begin{itemize}
	\item Probability of successful undetected attack.
	\item Minimisation of the impact of false positives in terms of probability drops (i.e., variance minimisation problem).
\end{itemize}
\indent In this document, the first problem is stated. The second problem is considered as a potential avenue for further research.\\
\indent Let us consider the problem of minimal non-detection probability. The problem involves the static distribution of the system with no false positive events, and it minimises the probability of a successful attacker’s intrusion into the restricted subset of nodes. Such a subset of nodes within the graph is denoted as $w \in \{0, 1\} ^ {|N| + 1}$.\\
\indent Denote the event detection placement within the graph by $\mathfrak{E}$. Thus, using the results of Lemma \ref{thm:eigen_vector_lemma}, the problem may be formulated in the form of MILP as follows:\\
\begin{equation}
	\begin{cases}
		\minl{\mathfrak{E}} \langle w, p \rangle \\
		\Gamma(\mathfrak{E}) A p = a_{na} p \\
		\langle 1, p \rangle = 1 \\
		p \geq 0 \\
		F(\mathfrak{E}) \leq G
	\end{cases}
\end{equation}
\indent Here, $F(\mathfrak{E}) \leq G$ stays for any arbitrary linear constraints for $\mathfrak{E}$, such as cost or deployment limitations.

\section{Experimental Results}
\indent Model evaluation was performed using the Intrusion Detection Evaluation Dataset 2017 \cite{icissp18_ids2017}. This dataset contains some of the most up-to-date and commonly seen attacks, and so it resembles real-world data (PCAPs). Additionally, the dataset includes the results of the network traffic analysis using CICFlowMeter with data exchange flows formed as typical machine learning feature vectors. Each recording (vector) includes the time stamp, source and destination IPs, source and destination ports, and protocol and attack types (label).\\
\indent For this dataset, the abstract behaviour of 25 users was simulated based on the HTTP, HTTPS, FTP, SSH, and email protocols used for data exchange. The attack labels included Brute Force FTP, Brute Force SSH, DoS, Heartbleed, Web Attack, Infiltration, Botnet, and DDoS.\\
\indent The following experiments were carried out over the selected dataset:
\begin{itemize}
	\item Attack type prediction (classification) based on data received from event detectors. Here, the EP-based model may be considered a classifier ensembling technique over several additional classifiers, which monitor the data flow.
	\item Optimal event detector selection over limited resources. 
	\item ``Under attack" condition detection, based on the estimated variances of the attack flag.
\end{itemize}
\indent In order to perform the experiments listed above, a TPG should be developed and additional event detector types should be specified.

\section{Conclusion}
\indent This report presented the proposed dynamic model, and it derived several possible monitoring problem instantiations and algorithms, grounded in Bayesian inference. The developed models include:
\begin{itemize}
	\item EP-grounded model for predicting an adversary’s actions
	\item ``Under-attack" probability estimation using BF-type model
	\item Optimal event detection assignment problems (one formally stated, the second given in outline)
\end{itemize}
\indent The developed models can be considered a logical continuation of the TPG and the static model, as developed previously. In ongoing and future research, experiments will be undertaken over the selected dataset, and a statement will be given of the optimisation problem, which performs the minimisation of the false detection impacts.

\bibliographystyle{plain}
\bibliography{refs}

\end{document}
