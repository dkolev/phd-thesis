function [ AdjMatr, StartID, StopID, ExpandedAssetID, ExpandedEnterID ] = ...
    CreateExpandedGraph( ThreatGraph, AttackTypeNum, AssetID, EnterID )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

n = size(ThreatGraph, 1);

Blocks = cell(AttackTypeNum, 1);
for i=1:AttackTypeNum
    Blocks{i} = ThreatGraph;
end

ToAdd = 0:n:( (n-1)*AttackTypeNum );

tmp = blkdiag(Blocks{:}); % create block-diagonal matrix (layer-based graph)

%prepare for vertex-cut
N = size(tmp, 1);
% add Start node
N = size(tmp, 1);
StartID = N;

tmp = [tmp; zeros(1, N)];
tmp = [tmp, zeros(N+1, 1)];

EnterID_n = repmat(EnterID, AttackTypeNum, 1) + repmat(ToAdd', 1, length(EnterID)); 
EnterID_n = EnterID_n';
EnterID_n = EnterID_n(:);

tmp(EnterID_n, end) = 1;
% add Stop node
N = size(tmp, 1);
StopID = N;

tmp = [tmp; zeros(1, N)];
tmp = [tmp, zeros(N+1, 1)];

AssetID_n = repmat(AssetID, AttackTypeNum, 1) + repmat(ToAdd', 1, length(AssetID)); 
AssetID_n = AssetID_n';
AssetID_n = AssetID_n(:);

tmp(end, AssetID_n) = 1;

AdjMatr = tmp;
ExpandedAssetID = AssetID_n;
ExpandedEnterID = EnterID_n;


end

