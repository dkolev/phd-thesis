function [ h, GraphStructure ] = ShowGraph( ThreatGraph, EnterID, CyberNodes, AssetID, h )
% Updates the graph appearance
GraphStructure = []
if (isempty(h))
    GraphStructure = biograph(ThreatGraph');
    h = view(GraphStructure);
end
set(h.Nodes(EnterID), 'Shape', 'trapezium')
for i=1:size(ThreatGraph,1)
    set(h.Nodes(i), 'ID', num2str(i));
end
for i=1:length(CyberNodes)
    set(h.Nodes(CyberNodes(i)), 'Color', [0, 0.4, 0.6]);
end
for i=1:length(EnterID)
    set(h.Nodes(EnterID(i)), 'ID', ['Entrance ', num2str(i)]);
end
set(h.Nodes(AssetID), 'Shape', 'ellipse');
for i=1:length(AssetID)
    set(h.Nodes(AssetID(i)), 'ID', ['SA ', num2str(i)]);
end
clc;
    
end

