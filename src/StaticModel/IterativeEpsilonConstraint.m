function [ CurrResults ] = IterativeEpsilonConstraint( CostTh , ImpactMatrix,...
    ExpandedGraphStructure, Corresp, ExtendedAssetID, StartID, StopID, ...
    ThreatControlMat, ChckPntControlMat, ImpactUpperBound, CostFun, lambda )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
[ Criteria, D, p, y, z, fval, OptimalAttacks, IsFeasible ] = MaximizeCriteria( CostTh, ImpactMatrix, ExpandedGraphStructure, Corresp,...
    ExtendedAssetID, StartID, StopID, ...
    ThreatControlMat, ChckPntControlMat, ImpactUpperBound, CostFun);
if(IsFeasible)
    res = struct('Criteria', Criteria, 'Controls', D, 'ProtectionRef', p, 'ControlsUsed', y,...
       'MaxImpact', fval, 'OptimalAttacks', OptimalAttacks);
    CurrResults = {res};
    for i=1:length(ImpactUpperBound)
        
        CurrImpactUpperBound = Criteria(2:end);
        CurrImpactUpperBound(i) = CurrImpactUpperBound(i) - lambda;
        
        TmpRes = IterativeEpsilonConstraint( CostTh , ImpactMatrix,...
            ExpandedGraphStructure, Corresp, ExtendedAssetID, StartID, StopID, ...
            ThreatControlMat, ChckPntControlMat, CurrImpactUpperBound, CostFun, lambda );
        
        CurrResults = [CurrResults(:), TmpRes(:)];
        
    end
end


end

