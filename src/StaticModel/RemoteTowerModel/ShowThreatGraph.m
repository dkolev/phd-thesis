function [ output_args ] = ShowThreatGraph( ThreatGraph, EnterID, AssetID )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
GraphStructure = biograph(ThreatGraph');
h = view(GraphStructure);
set(h.Nodes(EnterID), 'Shape', 'trapezium')
for i=1:length(EnterID)
    set(h.Nodes(EnterID(i)), 'ID', ['Entrance ', num2str(i)]);
end
set(h.Nodes(AssetID), 'Shape', 'ellipse');
for i=1:length(AssetID)
    set(h.Nodes(AssetID(i)), 'ID', ['SA ', num2str(i)]);
end

end

