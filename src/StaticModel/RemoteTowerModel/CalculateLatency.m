function [ PathLat ] = CalculateLatency( Path, Latencies, Capacities, FrameSize, maxFrameSize )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
N = size(Path, 1);

SupAssId = zeros(N, 1);
IdsRow = find(sum(Path, 2) == 1); %find  which assets the path crosses 
IdsColumn = find(sum(Path, 1) == 1);
Ids = union(IdsRow, IdsColumn);
SupAssId(Ids) = 1;

AssetLat = (SupAssId')*Latencies; %sum lat

CurrCap = Capacities(logical(Path));

NetworkLat = sum((FrameSize+maxFrameSize)./CurrCap);

PathLat = NetworkLat + AssetLat;

end

