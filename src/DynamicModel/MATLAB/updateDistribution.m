function [ m_new, betta_new, p_new ] = updateDistribution( m, betta_s, nu_s, p, D, dFP, dTP, pathList, controlList, attackTypes )
% N = size(pathList, 1), M - number of nodes
% attackTypes - for aeach threat in threat list - corresponding type of attack
% p and pathList should be ordered in the same way. p ~ N x 1
% controlList - list (array) of controls for EACH node, M x 1. If no security control -
% use 0.
% D is a list(!!) of expanded graph nodes which are related to the
% detection

%% Initial preparation
sigma_s = betta_s + nu_s;
sigma = sqrt(sigma_s);
s = m;
v_s = betta_s * nu_s / (nu_s + betta_s);

pathWithDetection = sum(ismember(pathList, D), 2) > 0;
pathWithNoDetection = logical(1 - pathWithDetection);

%evaluate best control before current detection
tmp = pathList(pathWithDetection, :);
tmpAttackTypes = attackTypes(pathWithDetection);
% graph structure assertion
if(length(unique(tmpAttackTypes)) > 1)
   disp('GA-GA-GA-GA!'); 
end
if(isempty(tmpAttackTypes))
    m_new = m;
    betta_new = betta_s; 
    p_new = p;
    return;
end
assert(length(unique(tmpAttackTypes)) == 1);

controlPosition = (ismember(tmp, D)); %should be unique for each row...
controlsBeforeCurrent = (1 - cumsum(controlPosition, 2));
tmp(tmp == 0) = 1;
%evaluate controls for each moment of time

bestControl = max(controlList(tmp) .* controlsBeforeCurrent, [], 2);

%just in case?...
listBestControl = zeros(length(p), 1);
listBestControl(pathWithDetection) = bestControl;

%% Evaluate back message to the experience and path probability
nrmCdfs = normcdf(- s + bestControl, 0, sigma); %check!!
nrmPdfs = normpdf( -s + bestControl, 0, sigma);
%calculate the coefficient
RealDetectionWeights = dTP * (1 - nrmCdfs) + dFP * nrmCdfs;
Z = dFP * sum(p(pathWithNoDetection)) + RealDetectionWeights' * p(pathWithDetection);

%calculate the mean
RealDetectionMeansTP = s * ( 1 - nrmCdfs ) + sigma_s * nrmPdfs;
RealDetectionMeansFP = s * nrmCdfs - sigma_s * nrmPdfs;

AverageNotNorm = s * dFP * sum(p(pathWithNoDetection)) +...
    (dTP * RealDetectionMeansTP + dFP * RealDetectionMeansFP)' * p(pathWithDetection);

mu_new = AverageNotNorm / Z;

% expected value.
m_new = v_s * (mu_new / nu_s + m / betta_s); %% here the "randomisation" of the s.c. comes

%calculate the new variance (complex one!)

DetectionSigmaTP = sigma_s * ( (1 - nrmCdfs) + (bestControl - s) .* nrmPdfs - ...
    sigma_s * ( ((nrmPdfs).^2) ./ (1 - nrmCdfs) ) );
DetectionSigmaFP = sigma_s * ( nrmCdfs - (bestControl - s) .* nrmPdfs - ...
    sigma_s * ( ((nrmPdfs).^2) ./ (nrmCdfs) ) ) ;

VarianceNotNorm = sigma_s * dFP * sum(p(pathWithNoDetection)) + ...
    (dTP * DetectionSigmaTP + dFP * DetectionSigmaFP)' * p(pathWithDetection);

sigma_s_new = VarianceNotNorm / Z;
betta_new = nu_s + sigma_s_new;

%calculate new path probabilities:

p_new = zeros(length(p), 1);
p_new(pathWithNoDetection) = p(pathWithNoDetection) * dFP;
p_new(pathWithDetection) = RealDetectionWeights .* p(pathWithDetection);
p_new = p_new / Z;


end

