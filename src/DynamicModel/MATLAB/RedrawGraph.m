function [ context, handle ] = RedrawGraph( handle, redPath, yelowPath, context, newNode, allDetections, corresp )

context.node = newNode;

[context, handle] = SetOldDetections(context, handle);



[handle, context] = DrawAttackerDetectionsNode( handle, context, allDetections, corresp );



set(handle.Edges(:), 'LineColor', [0, 1, 0]);


handle = SetEdgesColorFromPath(redPath, [1, 0, 0], handle);
handle = SetEdgesColorFromPath(yelowPath, [1, 1, 0], handle);


end

function handle = SetEdgesColorFromPath(pathList, color, handle)

goodEdges = zeros(0, 2);
for i = 1:size(pathList, 1)
   
    currPath = pathList(i, :);
    currPath = currPath(currPath > 0);
    
    nn = length(currPath);
    IDX = [[1:(nn-1)]', [2:nn]'];
    
    edges = currPath(IDX);
    
    goodEdges = [goodEdges; edges];
    goodEdges = unique(goodEdges, 'rows');
    
    
end

if(isempty(goodEdges))
   return; 
end
sourceNodesID = get(handle.Nodes(goodEdges(:, 1)), 'ID');%strsplit(num2str(goodEdges(:, 1)'), ' ');
sinkNodesID = get(handle.Nodes(goodEdges(:, 2)), 'ID'); %strsplit(num2str(goodEdges(:, 2)'), ' ');

for i=1:length(sinkNodesID)
    edge = getedgesbynodeid(handle, sourceNodesID{i}, sinkNodesID{i});
    set(edge, 'LineColor', color);
end
end

function [context, handle] = SetOldDetections(context, handle)

if(~isfield(context, 'detections'))
   context.detections = struct('oldNodes', [], 'LineWidth', [], 'LineColor', []); 
end
oldNodes = context.detections.oldNodes;
for i = 1:length(oldNodes)
    
    set(handle.Nodes(oldNodes(i)), 'LineWidth', context.detections.LineWidth(i));
    set(handle.Nodes(oldNodes(i)), 'LineColor', context.detections.LineColor(i, :));
    
end

end


