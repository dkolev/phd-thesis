# -*- coding: utf-8 -*-
"""
Created on Thu Jan 30 18:13:23 2020

@author: admin
"""

import numpy as np
import matplotlib.pyplot as plt
from time import sleep
from copy import deepcopy
import networkx as nx 

def draw_graph(A, picture_name, labs=None):
    plt.figure()
    g = nx.Graph()
    for i in range(A.shape[0]):
        for j in range(A.shape[1]):
            if A[i, j] > 0:
                g.add_edge(i, j)
    if labs is not None:
        g = nx.relabel_nodes(g, labs)
    nx.draw(g, with_labels = True) 
    plt.savefig(picture_name) 

def draw_nat_attack(to_draw = False):
    A = np.zeros(shape=(3, 3))
    A[0, 1] = 1;
    A[1, 2] = 1;
    lab_names = {0: 'START', 2: 'NAT', 'name': 'NAT'}
    if to_draw:
        draw_graph(A, 'nat.png', lab_names)
    return A, lab_names
    
def draw_ssh_attack(to_draw = False):
    A = np.zeros(shape=(9, 9))
    A[0, 1] = 1
    A[1, 2] = 1
    A[2, 3] = 1
    A[2, 4] = 1
    A[2, 5] = 1
    A[3, 6] = 1
    A[4, 7] = 1
    A[5, 8] = 1
    lab_names = {0: 'START', 6: 'DoH', 7: 'CCa', 8: 'CCo', 'name': 'SSH'}
    if to_draw:
        draw_graph(A, 'ssh.png', lab_names)
    return A, lab_names

def draw_ftp_attack(to_draw = False):
    A = np.zeros(shape=(4, 4))
    A[0, 1] = 1
    A[1, 2] = 1
    A[2, 3] = 1
    lab_names = {0: 'START', 3: 'DoH', 'name': 'FTP'}
    if to_draw:
        draw_graph(A, 'ftp.png', lab_names)
    return A, lab_names

def draw_dos_attack(to_draw = False):
    A = np.zeros(shape=(3, 3))
    A[0, 1] = 1;
    A[1, 2] = 1;
    lab_names = {0: 'START', 2: 'CCa', 'name': 'DOS'}
    if to_draw:
        draw_graph(A, 'dos.png', lab_names)  
    return A, lab_names

def draw_hb_attack(to_draw = False):
    A = np.zeros(shape=(3, 3))
    A[0, 1] = 1;
    A[1, 2] = 1;
    lab_names = {0: 'START', 2: 'DoH', 'name': 'HB'}
    if to_draw:
        draw_graph(A, 'hb.png', lab_names)
    return A, lab_names

def draw_wa_attack(to_draw = False):
    A = np.zeros(shape=(10, 10))
    A[0, 1] = 1
    A[1, 2] = 1
    A[2, 3] = 1 # SA
    A[2, 4] = 1
    A[4, 5] = 1
    A[5, 6] = 1 # SA
    A[2, 7] = 1
    A[7, 8] = 1
    A[8, 9] = 1 # SA
    lab_names = {0: 'START', 3: 'bf_DoH', 6: '(bf_DoH, xss_DoH)', 9: '(bf_DoH, sql_DoH)',
                 'name': 'WA'}
    if to_draw:
        draw_graph(A, 'wa.png', lab_names)
    return A, lab_names
    
def draw_inf_attack(to_draw = False):
    A = np.zeros(shape=(9, 9))
    A[0, 1] = 1
    A[1, 2] = 1
    A[2, 3] = 1
    A[2, 4] = 1
    A[2, 5] = 1
    A[3, 6] = 1
    A[4, 7] = 1
    A[5, 8] = 1
    lab_names = {0: 'START', 6: 'DoH', 7: 'CCa', 8: 'CCo', 'name': 'INF'}
    if to_draw:
        draw_graph(A, 'inf.png', lab_names)
    return A, lab_names

vae_det = {
        'INF': [0.72727273, 0.97],
        'DOS': [0.97460855, 0.96],
        'WA': [0.990792, 0.96],
        'HB': [0.83333334, 0.95],
        'SSH': [0.457110, 0.96],
        'FTP': [0.998323, 0.96],
        'NAT': [0, 1]
        }

nn_det = {
        'INF': [0.18181818, 0.97],
        'DOS': [0.9998958, 0.97],
        'WA': [0.999397, 0.97],
        'HB': [0.6666667, 0.97],
        'SSH': [0.987020, 0.97],
        'FTP': [0.903983, 0.97],
        'NAT': [0, 1]
        }

attack_correspondence = {
        'Infiltration': 'INF',
        'Web Attack – Brute Force': 'WA',
        'Web Attack – Sql Injection': 'WA',
        'Web Attack – XSS': 'WA'
        }

external_hosts_ips = {0: '192.168.10.50', 1: '192.168.10.51'}
nat_hosts = {2: '192.168.10.111'}
internal_hosts_ips = {3: '192.168.10.1', 4: '192.168.10.3', 5: '192.168.10.5', 6:'192.168.10.8',
                      7: '192.168.10.9', 8: '192.168.10.12', 9: '192.168.10.14', 10: '192.168.10.15', 11: '192.168.10.25'}

all_host_ips = deepcopy(internal_hosts_ips)
for v in external_hosts_ips:
    all_host_ips[v] = external_hosts_ips[v]

all_host_ips[2] = nat_hosts[2]

def add_sequential_attack_link(large_graph, descriptor, victim_host, 
                               attack_type, preliminary_conditions):
    start_node = descriptor['attacks'][(attack_type, victim_host)]['START']
    for kk in descriptor['SA']:
        curr_conds = deepcopy(descriptor['SA'][kk][3])
        curr_conds.append((descriptor['SA'][kk][0], descriptor['SA'][kk][1]))
        if(all([c in preliminary_conditions for c in curr_conds])):
            large_graph[kk, start_node] = 1
    return attack_graph

def add_to_graph(large_graph, descriptor, small_graph, victim_host, 
                 small_descriptor, preliminary_conditions = None):
    n = large_graph.shape[0]
    m = small_graph.shape[0]
    new_graph = np.zeros(shape=(n+m, n+m))
    N = n + m
    new_graph[0:n, 0:n] = large_graph
    new_graph[n:N, n:N] = small_graph
    attack_type = None
    for v in small_descriptor:
        if small_descriptor[v] == 'START':
            if len(preliminary_conditions) == 0:
                new_graph[0, n + v] = 1
            else:                    
                for kk in descriptor['SA']:
                    curr_conds = deepcopy(descriptor['SA'][kk][3])
                    curr_conds.append((descriptor['SA'][kk][0], descriptor['SA'][kk][1]))
                    if(all([c in curr_conds for c in preliminary_conditions])):
                        new_graph[kk, n + v] = 1
        if v == 'name':
            attack_type = small_descriptor[v]
    
    descriptor['attacks'][(attack_type, victim_host)] = {'SA': [], 'max_node': n + m - 1}
    for v in small_descriptor:
        if small_descriptor[v] != 'START' and v != 'name':
            curr_attack_specific = (attack_type, small_descriptor[v])
            descriptor['SA'][v + n] = [curr_attack_specific, victim_host, 
                      small_descriptor[v], preliminary_conditions]
            descriptor['attacks'][(attack_type , victim_host)]['SA'].append((attack_type, 
                      small_descriptor[v], preliminary_conditions, v + n))
        if small_descriptor[v] == 'START':
            descriptor['attacks'][(attack_type, victim_host)]['START'] = v + n
    descriptor['hosts'][all_host_ips[victim_host]].extend(range(descriptor['attacks'][(attack_type, victim_host)]['START'], 
              descriptor['attacks'][(attack_type, victim_host)]['max_node']+1))
    return new_graph


m_dos, lab_dos = draw_dos_attack()
m_hb, lab_hb = draw_hb_attack()
m_wa, lab_wa = draw_wa_attack()
m_nat, lab_nat = draw_nat_attack()
m_ssh, lab_ssh = draw_ssh_attack()
m_ftp, lab_ftp = draw_ftp_attack()
m_inf, lab_inf = draw_inf_attack()

all_attacks = [draw_dos_attack, draw_hb_attack, draw_wa_attack, draw_nat_attack,
               draw_ssh_attack, draw_ftp_attack, draw_inf_attack]

attack_dict = {}
for attack in all_attacks:
    m, d = attack()
    attack_dict[d['name']] = [m, d]


num_internal_hosts = 10
num_external_hosts = 2

external_hosts = range(2)

nat_host = 2
internal_hosts = range(3, 12)

pre_reqs_list = []
attack_sequence = [([[]], ['DOS', 'FTP', 'SSH', 'HB', 'WA']),
                   ([[('HB', 'DoH')], [('SSH', 'DoH')], [('WA', '(bf_DoH, sql_DoH)')]], ['INF'])]

attack_graph = np.ones(shape=(1,1))
descriptor = {'SA' : {}, 'attacks': {}, 'hosts': {}}
all_described_attacks = []
for i in external_hosts:
    descriptor['hosts'][all_host_ips[i]] = []
    attacks_happened_against_host = []
    for att in attack_sequence:
        pre_atts = att[0]
        for pre_att in pre_atts:            
            if all([att in attacks_happened_against_host for att in pre_att]):
                corrected_pre_att = [(t, i) for t in pre_att]
                for att_type in att[1]:
                    gr = attack_dict[att_type][0]
                    d = attack_dict[att_type][1]
                    if (att_type, i) not in descriptor['attacks']:
                        attack_graph = add_to_graph(attack_graph, descriptor, gr, i, 
                                                    d, corrected_pre_att)
                        for kk in d:
                            if d[kk] != 'START' and kk != 'name':
                                attacks_happened_against_host.append((att_type, d[kk]))
                                all_described_attacks.append(((att_type, d[kk]), i))
                    else:
                        attack_graph = add_sequential_attack_link(attack_graph, descriptor, i, 
                               att_type, corrected_pre_att)

gr_nat = attack_dict['NAT'][0]
d_nat = attack_dict['NAT'][1]

descriptor['hosts'][all_host_ips[nat_host]] = []
attack_graph = add_to_graph(attack_graph, descriptor, gr_nat, nat_host,
                            d_nat, [(('WA', '(bf_DoH, sql_DoH)'), 0)])



attack_graph = add_sequential_attack_link(attack_graph, descriptor, nat_host, 
                               'NAT', [(('HB', 'DoH'), 0)])

attack_graph = add_sequential_attack_link(attack_graph, descriptor, nat_host, 
                               'NAT', [(('SSH', 'DoH'), 0)])

all_described_attacks.append((('NAT', 'NAT'), nat_host))

attack_sequence = [([[(('NAT', 'NAT'), nat_host)]], ['DOS', 'FTP', 'SSH', 'HB', 'WA', 'INF'])]


for i in internal_hosts:
    descriptor['hosts'][all_host_ips[i]] = []
    attacks_happened_against_host = []
    for att in attack_sequence:
        pre_atts = att[0]
        for pre_att in pre_atts:            
            if all([att in all_described_attacks for att in pre_att]):
                corrected_pre_att = [t for t in pre_att]
                for att_type in att[1]:
                    gr = attack_dict[att_type][0]
                    d = attack_dict[att_type][1]
                    if (att_type, i) not in descriptor['attacks']:
                        attack_graph = add_to_graph(attack_graph, descriptor, gr, i, 
                                                    d, corrected_pre_att)
                        for kk in d:
                            if d[kk] != 'START' and kk != 'name':
                                attacks_happened_against_host.append((att_type, d[kk]))
                                all_described_attacks.append(((att_type, d[kk]), i))
                    else:
                        attack_graph = add_sequential_attack_link(attack_graph, descriptor, i, 
                               att_type, corrected_pre_att)

for v in descriptor['SA']:
    attack_graph[v, v] = 1

#draw_graph(attack_graph)
