function [ SourceErrorRate, ControlErrorRate ] = CalculateErrorRate( DataPaths, ControlPaths, ErrorRate )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
SourceErrorRate = zeros(size(DataPaths, 3), 1);
ControlErrorRate = zeros(size(ControlPaths, 3), 1);

for i=1:length(SourceErrorRate)
    Incomming = (sum(DataPaths(:,:,i), 2)>0)';
    Outcomming = (sum(DataPaths(:,:,i), 1)>0);
    InvolvedAssets = (Incomming | Outcomming);
    currER = ErrorRate(InvolvedAssets);
    SourceErrorRate(i) = prod(currER);
end

for i=1:length(ControlErrorRate)
    Incomming = (sum(ControlPaths(:,:,i), 2)>0)';
    Outcomming = (sum(ControlPaths(:,:,i), 1)>0);
    InvolvedAssets = (Incomming | Outcomming);
    currER = ErrorRate(InvolvedAssets);
    ControlErrorRate(i) = prod(currER);
end

end

