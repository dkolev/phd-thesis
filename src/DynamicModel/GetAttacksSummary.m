function [ SA ] = GetAttacksSummary( redPath, types, attackCodes )
% assumption - node ID is == to SA ID

M = size(redPath, 2);
N = size(redPath, 1);
IDX = sum(redPath > 0, 2);
IDX = IDX * N + [1:N]'; %due to first 0 in redPath

saList = redPath(IDX);
aC = attackCodes(types);

saListUnique = unique(saList);
SA = cell(length(saListUnique), 2);
for i=1:length(saListUnique)
   
    SA{i, 1} = saListUnique(i);
    SA{i, 2} = unique(aC(saList == saListUnique(i)));
    
end


end

