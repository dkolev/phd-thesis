# -*- coding: utf-8 -*-
"""
Created on Mon Aug 19 12:18:21 2019

@author: admin
"""

import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
from LAMBOptimiser import LAMBOptimizer, create_optimizer

class SimpleNeuralStatistitian:
    
    def __init__(self, c_dim, z_dim, x_dim, n_samples_c, n_sampes_z):
        self.c_dim = c_dim
        self.z_dim = z_dim
        self.x_dim = x_dim
        self.n_samples_c = n_samples_c
        self.n_samples_z = n_sampes_z
        self.loss_smooth = 0.001
        self.bsize = 5
        self.create_ns_model()
        self.build_logprob_net()
        
    def select_batch(self, X, batch_size):
        idx = np.random.randint(0, X.shape[0] - batch_size, 1)[0]
        return X[idx:(idx + batch_size), :]        
     
    def build_net(self, net_input, net_structure):
        net = net_input
        for l in net_structure:
            net = l(net)
        return net
    
    def sampling(self, z_mean, z_log_var, n_samples, dim):
        epsilon = tf.random.normal([n_samples, dim])
        return z_mean + tf.exp(0.5 * z_log_var) * epsilon
    
    def create_ns_model(self):
        self.input_dataset = tf.placeholder(tf.float32, shape=(None, self.x_dim))
        self.input_assessed_vectors = tf.placeholder(tf.float32, shape=(None, self.x_dim))
        net = self.input_dataset
        net = tf.layers.Dense(self.c_dim / 3 + 2 * self.x_dim / 3, activation=tf.nn.selu)(net)
        net = tf.expand_dims(tf.reduce_mean(net, axis=0), axis=0)
        net = tf.layers.Dense(2 * self.c_dim / 3 + self.x_dim / 3, activation=tf.nn.selu)(net)
        self.q_c_cond_x_mean = tf.layers.Dense(self.c_dim)(net)
        self.q_c_cond_x_logvar = tf.layers.Dense(self.c_dim)(net)
        
        self.q_c_samples = self.sampling(self.q_c_cond_x_mean, self.q_c_cond_x_logvar, 1, self.c_dim)
        #self.q_c_samples = tf.expand_dims(self.q_c_samples, axis=0)
        
        self.p_z_cond_c_struct = [tf.layers.Dense(2 * self.c_dim / 3 + self.z_dim / 3, activation=tf.nn.selu),
                           #tf.layers.Dense((self.c_dim + 2 * self.z_dim) / 3, activation=tf.nn.selu),
                           tf.layers.Dense(self.z_dim)]
        
        self.p_z_cond_c_logvar_struct = self.p_z_cond_c_struct[:-1]
        self.p_z_cond_c_logvar_struct.append(tf.layers.Dense(self.z_dim))
        
        
        self.p_z_cond_c_mean = self.build_net(self.q_c_samples, self.p_z_cond_c_struct)
        self.p_z_cond_c_logvar = self.build_net(self.q_c_samples, self.p_z_cond_c_logvar_struct)
        
        repeated_sample = tf.tile(self.q_c_samples, [tf.shape(self.input_assessed_vectors)[0], 1])
        self.q_z_cond_cx_input = tf.concat([self.input_assessed_vectors, repeated_sample], axis=1)
        inp_dim = int(self.q_z_cond_cx_input.shape[1])
        
        self.q_z_cond_xc_structure = [tf.layers.Dense(2 * inp_dim / 3 + self.z_dim / 3, activation=tf.nn.selu),
                           #tf.layers.Dense((inp_dim+ 2 * self.z_dim) / 3, activation=tf.nn.selu),
                           tf.layers.Dense(self.z_dim)]
        self.q_z_cond_xc_logvar_structure = self.q_z_cond_xc_structure[:-1]
        self.q_z_cond_xc_logvar_structure.append(tf.layers.Dense(self.z_dim))
        
        self.q_z_cond_xc_mean = self.build_net(self.q_z_cond_cx_input, self.q_z_cond_xc_structure)
        self.q_z_cond_xc_logvar = self.build_net(self.q_z_cond_cx_input, self.q_z_cond_xc_logvar_structure)
        
        self.q_z_cond_xc_sample = self.sampling(self.q_z_cond_xc_mean, self.q_z_cond_xc_logvar, 1, self.z_dim)
        
        inp_dim = self.z_dim
        self.p_x_cond_z_struct = [tf.layers.Dense(inp_dim / 3 + self.x_dim / 3, activation=tf.nn.selu),
                           #tf.layers.Dense((inp_dim+ 2 * self.x_dim) / 3, activation=tf.nn.selu),
                           tf.layers.Dense(self.x_dim)]
        
        self.p_x_cond_qz_mean = self.build_net(self.q_z_cond_xc_sample, self.p_x_cond_z_struct)
        
        log_p = -0.5 * tf.reduce_sum(tf.square(self.p_x_cond_qz_mean - self.input_assessed_vectors))
        
        num_repeats = tf.shape(self.q_z_cond_xc_logvar)[0]
        p_z_cond_c_logvar_repeated = tf.tile(self.p_z_cond_c_logvar, [num_repeats, 1])
        p_z_cond_c_mean_repeated = tf.tile(self.p_z_cond_c_mean, [num_repeats, 1])
        
        kl_div_z = -0.5 * tf.reduce_sum( -1 + p_z_cond_c_logvar_repeated - self.q_z_cond_xc_logvar + (tf.exp(self.q_z_cond_xc_logvar) + \
                            tf.square(p_z_cond_c_mean_repeated - self.q_z_cond_xc_mean)) / \
                            tf.exp(p_z_cond_c_logvar_repeated))
        kl_div_c = -0.5 * tf.reduce_sum(-1 - self.q_c_cond_x_logvar + tf.exp(self.q_c_cond_x_logvar) + \
                                       tf.square(self.q_c_cond_x_mean))
        
        self.loss = -log_p - kl_div_z - kl_div_c
        optimizer = tf.train.AdamOptimizer()
        
        #self.optimizer = create_optimizer(self.loss, 0.001, 500000, False)
        self.optimizer = optimizer.minimize(self.loss)
    
    def build_logprob_net(self):
        self.sampling_size = 5
        self.p_z_cond_c_sample = tf.expand_dims(tf.reduce_mean(self.sampling(self.q_z_cond_xc_mean, self.q_z_cond_xc_logvar, self.sampling_size, self.z_dim),\
                                                               axis=0), axis=0)
        self.p_x_cond_pz_mean = self.build_net(self.p_z_cond_c_sample, self.p_x_cond_z_struct)
        self.logprob = -0.5 * tf.reduce_sum(tf.square(self.p_x_cond_pz_mean - self.input_assessed_vectors))
            
    def distance(self, data):
        bsize = self.bsize
        distances = np.zeros((data.shape[0] - bsize + 1,))
        for i in range(bsize, data.shape[0]):
            sample = data[i - 1]
            context = data[i-bsize:i, :]
            sample = np.reshape(sample, (1, self.x_dim)) 
            context_sample = np.reshape(context, (-1, self.x_dim)) 
            res = self.session.run([self.logprob], feed_dict={self.input_dataset : context_sample, 
                               self.input_assessed_vectors: sample})
            distances[i-bsize] = res[0]
        return -distances
    
    def fit(self, X, Y):
        Xp = X[Y == 0, :]
        self.session = tf.Session()
        self.session.run(tf.global_variables_initializer())
        curr_loss = 0
        for i in range(200000):
            b = self.select_batch(Xp, 5)
            [loss] = self.session.run([self.loss], feed_dict={self.input_dataset: b, self.input_assessed_vectors: b})
            if not np.isfinite(loss):
                continue
            if i > 10000 and loss > curr_loss * 10e7:
                continue
            [opt]  = self.session.run([self.optimizer], feed_dict={self.input_dataset: b, self.input_assessed_vectors: b})
            alpha = 1.0 / np.float(i + 1)
            if(alpha < self.loss_smooth):
                alpha = self.loss_smooth
            curr_loss = (1 - alpha) * curr_loss + alpha * loss
            if i % 50 == 0:
                print(curr_loss)
        
        dists = self.distance(X)
        idx = np.argsort(dists)
        sd = dists[idx]
        Ys = Y[idx]
        Yidx = Ys > 0
        Yidx = Yidx.astype('float32')
        tn = np.cumsum(1 - Yidx) / np.sum(1 - Yidx)
        tp = (np.sum(Yidx) - np.cumsum(Yidx))/ np.sum(Yidx)
        plt.plot(tn, tp)
        difftn = tn[1:] - tn[:-1]
        auc = np.sum(difftn * tp[1:])
        threshold = np.min(sd[tn > 0.95])
        Yvals = np.unique(Y)
        Yvals = Yvals[Yvals != 0]
        table = {}
        recall = np.sum((Ys == 0) * (sd < threshold)) / np.sum((Ys == 0))
        tpv = np.sum((Ys > 0) * (sd >= threshold)) / np.sum((Ys > 0))
        precision = np.sum((Ys == 0) * (sd < threshold)) / np.sum(sd < threshold)
        table[0] = [auc, recall, tpv, precision]
        for y in Yvals:
            tpvy = np.sum((Ys == y) * (sd >= threshold)) / np.sum(Ys == y)
            Ysy = Yidx[(Ys == y) + (Ys == 0)]
            tny = np.cumsum(1 - Ysy) / np.sum(1 - Ysy)
            tpy = (np.sum(Ysy) - np.cumsum(Ysy))/ np.sum(Ysy) 
            difftn = tny[1:] - tny[:-1]
            aucy = np.sum(difftn * tpy[1:])
            table[y] = [tpvy, aucy]
        return table