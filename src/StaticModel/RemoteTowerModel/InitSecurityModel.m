function [  ] = InitSecurityModel( ImpactPerAsset, ThreatGraph, AssetID, EnterID,...
    ChckPntControlMat, ThreatControlMat, CostFun, TmpCostFun, ProcControlMat, ProcCost, GeoNodes, CyberNodes, PossibleTypesOfAttacksMatrix)
%Initialize the security model structures.
AttackTypesNum = size(ThreatControlMat, 1);
%ImpactPerAsset = ImpactCalcFun(InternalGraph, InternWeights, AttackTypesNum); %matrix asset x attackTypeNum x ImpactTypes, filled with impacts
CritNum = size(ImpactPerAsset, 3);
AssetNum = length(AssetID);
%ImpactTypeNum = size(ImpactPerAsset, 3);
AttackTypesNum = size(ThreatControlMat, 1);

tmp = sum(ThreatGraph(AssetID, :), 1);
AssetAccessID = find(tmp > 0);

% Corresp: ChckPointNum x 2. Corres{i, 1} - ith initial check point ID.
% Corresp{i, 2} - matrix of corresponding Check-edges in the Exp Graph.
% Corresp{i, 2}(j, :) - edge, that corresponds to Corres{i, 1} node, attack
% type j.
% ExpandedAssetID : AssetNum x AttackNum. The same for EnterID
%
[ExpandedGraphStructure, StartID, StopID, ExpandedAssetID, ExpandedEnterID, Corresp] =...
    CreateExpandedGraph4VC(ThreatGraph, AttackTypesNum, AssetID, EnterID, ...
    GeoNodes, CyberNodes, PossibleTypesOfAttacksMatrix);

CheckPointsNum = size(Corresp, 1);
N = size(ExpandedGraphStructure, 1);

CostTh = 40000;
TmpCostTh = 45500;
%% Different optimization problems here

%ImpactUpperBound = ones(CritNum-1, 1);

% [ Criteria, D, p, x, z, nu, K, y, fval, fvalt, OptimalAttacks, IsFeasible, xCorr, xType ] = MaximizeCriteria(CostTh, TmpCostTh, ImpactPerAsset,...
%     ExpandedGraphStructure, Corresp, ExpandedAssetID, StartID, StopID, ...
%     ThreatControlMat, ProcControlMat, ChckPntControlMat, ImpactUpperBound, CostFun, TmpCostFun, ProcCost);

ImpactUpperBound = inf(CritNum-1, 1);

% Res = ParetoOptimalCosts(ImpactPerAsset,...
%     ExpandedGraphStructure, Corresp, ExpandedAssetID, StartID, StopID, ...
%     ThreatControlMat, ProcControlMat, ChckPntControlMat, ImpactUpperBound, CostFun, TmpCostFun, ProcCost);

Res = ParetoOptimalSecurity(CostTh, TmpCostTh, ImpactPerAsset,...
    ExpandedGraphStructure, Corresp, ExpandedAssetID, StartID, StopID, ...
    ThreatControlMat, ProcControlMat, ChckPntControlMat, ImpactUpperBound, CostFun, TmpCostFun, ProcCost, [], 1);
%    res = struct('Criteria', Criteria, 'Controls', D, 'ProtectionRef', p, 'ControlsUsed', x,...
%       'ToleratedAssets', nu, 'ProcControls', K, 'ProcControlsUsed', y,...
%       'xCorr', xCorr, 'xType', xType, 'DeployControlCost', fval, 'ProcControlCost', fvalt, 'OptimalAttacks', OptimalAttacks);
D = Res{1}.Controls;
p = Res{1}.ProtectionRef;
x = Res{1}.ControlsUsed;
nu = Res{1}.ToleratedAssets;
K = Res{1}.ProcControls;
y = Res{1}.ProcControlsUsed;
xCorr = Res{1}.xCorr;
xType = Res{1}.xType;
fval = Res{1}.DeployControlCost;
fvalt = Res{1}.ProcControlCost;
OptimalAttacks = Res{1}.OptimalAttacks;
Criteria = Res{1}.Criteria;

%% 
if(isempty(Res))
   disp('Insufficient funds for given protection level');
   return; 
end

% ATMSecurityGame = struct('IntGraph', InternalGraph, 'InternWeights', InternWeights,...
%     'ImpactPerAsset', ImpactPerAsset, 'ThreatGraph', ThreatGraph, 'AssetID', AssetID,...
%     'AssetAccessID',  AssetAccessID, 'ExpandedGraphStructure', ExpandedGraphStructure, 'StartID', StartID,...
%     'StopID', StopID, 'ExpandedAssetID', ExpandedAssetID, 'ExpandedEnterID', ExpandedEnterID);

%% Full coverage test
% [ Aineq, bineq, Aeq, beq, f ] = PrepareLProblem(...
%     ExpandedGraphStructure, StartID, StopID,...
%     Corresp, ThreatControlMat, ChckPntControlMat, CostFun);
% 
% [ D, p, y, fval ] = SolveCutLP( f, Aineq, bineq, Aeq, beq,...
%     CheckPointsNum, AttackTypesNum, N);


GraphStructure = biograph(ThreatGraph');
h = view(GraphStructure);
set(h.Nodes(EnterID), 'Shape', 'trapezium')
for i=1:size(ThreatGraph,1)
    set(h.Nodes(i), 'ID', num2str(i));
end
for i=1:length(CyberNodes)
    set(h.Nodes(CyberNodes(i)), 'Color', [0, 0.4, 0.6]);
end
for i=1:length(EnterID)
    set(h.Nodes(EnterID(i)), 'ID', ['Entrance ', num2str(i)]);
end
set(h.Nodes(AssetID), 'Shape', 'ellipse');
for i=1:length(AssetID)
    set(h.Nodes(AssetID(i)), 'ID', ['SA ', num2str(i)]);
end
clc;

st = sprintf('Initial cost for a given protection security system: %f \n', fval);
disp(st);

st = sprintf('Permanent cost for a given protection security system: %f \n', fvalt);
disp(st);
Ids = find(x>0);
for i = 1:length(Ids)
    chPointNumb = xCorr(Ids(i));
    controlNumb = xType(Ids(i));
    st = sprintf('Control type %d should be placed at node %d \n', controlNumb, Corresp{chPointNumb, 1});
    disp(st);
    set(h.Nodes(Corresp{chPointNumb, 1}), 'ID', [num2str(Corresp{chPointNumb, 1}), ' c', num2str(controlNumb)]);
    set(h.Nodes(Corresp{chPointNumb, 1}), 'LineColor', [1,0,0]);
end

ProcIds = find(y==1);
for i = 1:length(ProcIds)
    st = sprintf('Procedural control type %d should be placed \n', ProcIds(i));
    disp(st);
end

AssetNotProtected = reshape(nu, AssetNum, AttackTypesNum);
[r, c] = find(AssetNotProtected > 0); % 1 - tolerated, 0 - protected
for i=1:length(r)
    st = sprintf('Attack type %d to asset %d is tolerated. \n', c(i), r(i));
    disp(st);
end
if(isempty(r))
    disp('No attacks tolerated, full protection.');
end 

disp('For given constraints optimal (for criteria 1) security placement guarantees:');

for i=1:length(Criteria)
    st = sprintf('Objective function %d not higher than %f \n', i, Criteria(i));
    disp(st);
end




end

