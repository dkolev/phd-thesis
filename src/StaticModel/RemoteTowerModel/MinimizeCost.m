function [Criteria, D, p, x, z, nu, K, y, fval, fvalt, OptimalAttacks, IsFeasible, xCorr, xType  ] = MinimizeCost(TempCostTh, ImpactMatrix, ExpandedGraphStructure, Corresp,...
    ExtendedAssetID, StartID, StopID, ...
    ThreatControlMat, ProcControlMat, ChckPntControlMat, ImpactUpperBound, CostFun, TmpCostFun, ProcCost)
%UNTITLED12 Summary of this function goes here
% Here the minimization of criteria 1 is being applied.
% ImpactUpperBound - constraints only for the crit from #2 to end 

[ Aineq, bineq, Aeq, beq, f, CostRow, CostRowt, Ddim, pdim, xDim, zDim, nuDim, Kdim, yDim, xCorr, xType ] = PrepareLProblemCostMinimization(...
    ExpandedGraphStructure, StartID, StopID,...
    Corresp, ThreatControlMat, ProcControlMat, ChckPntControlMat, CostFun, TmpCostFun, ProcCost, ExtendedAssetID, TempCostTh);


[Aeq, beq] = ConsiderConstraints(ImpactMatrix, ImpactUpperBound, Aeq, beq,...
    Ddim, pdim, xDim, zDim, nuDim, Kdim, yDim);

[ D, p, x, z, nu, K, y , ~, IsFeasible ] = SolveCutLP( f, Aineq, bineq, Aeq, beq,...
    Ddim, pdim, xDim, zDim, nuDim, Kdim, yDim);


if(IsFeasible)
    fval = CostRow * [D; p; x; z; nu; K; y];
    fvalt = CostRowt * [D; p; x; z; nu; K; y];
    [Criteria, OptimalAttacks] = GetMultiObjectiveCriteriaValues( ExpandedGraphStructure, ImpactMatrix, D, K, Corresp, ExtendedAssetID, StartID, StopID );
else
    fvalt = [];
    fval = [];
    Criteria = [];
    OptimalAttacks = [];
end
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here


end

