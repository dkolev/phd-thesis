package TestApplication;


import kafka.javaapi.producer.Producer;
import GammaXmlMessages.*;
import GammaXmlMessages.IdsAlarmPayload.IdsAlarmSeverityLevel;
import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;

import java.io.Console;
import java.io.File;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Random;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class TestApp 
{
    private static Producer<String, String> producer;
    private final Properties properties = new Properties();
    
    private static List<String> threats = Arrays.asList("THijack", "T-IMC1", "T_AngryCat");

    public TestApp() {
        properties.put("metadata.broker.list", "localhost:9092, localhost:9093");
        properties.put("serializer.class", "kafka.serializer.StringEncoder");
        properties.put("partitioner.class", "test.kafka.SimplePartitioner");
        properties.put("request.required.acks", "1");
        ProducerConfig config = new ProducerConfig(properties);
        producer = new Producer<String, String>(config);
    }
    
    public static String generateAlarmMessage(HeaderParams header, IdsAlarmPayload payload)
    {
    	String result = null;
    	try
    	{
	    	IdsMessage msg = new IdsMessage(header, payload);
	    	List<IdsMessage> msgs = Arrays.asList(msg);
	    	IdsXmlDataFile idsXml = new IdsXmlDataFile(msgs);
	    	result = idsXml.getFormedMessage();
    	}
    	catch(Exception e)
    	{
    		
    	}
    	return result;
    }
    
    public static IdsAlarmPayload generatePayload(IdsAlarmSeverityLevel severity, Double certain, String threatName)
    {
    	return new IdsAlarmPayload(severity, certain, threatName, "0, 1, 2, 3");
    }
    
    public static HeaderParams generateHeader(EnvelopeUid Uid, Double Longt, Double Lat, String Sensitivity, String Priority, String Subject)
    {
    	Date when = new Date();
    	return new HeaderParams( Uid, when, Longt, Lat, Sensitivity, Priority, Subject);
    }
    
	public static String getStringFromDocument(Document doc)
	{
	    try
	    {
	       DOMSource domSource = new DOMSource(doc);
	       StringWriter writer = new StringWriter();
	       StreamResult result = new StreamResult(writer);
	       TransformerFactory tf = TransformerFactory.newInstance();
	       Transformer transformer = tf.newTransformer();
	       transformer.transform(domSource, result);
	       return writer.toString();
	    }
	    catch(TransformerException ex)
	    {
	       ex.printStackTrace();
	       return null;
	    }
	}

    public static void main(String[] args) throws Exception {
        new TestApp();
        Random random = new Random();
        GammaConsumer consumer = new GammaConsumer("42sol5.iae.nl", 9092);
        GammaProducer producer = new GammaProducer("42sol5.iae.nl:9092");
        String topic = "GAMMA_XML";
        while(true) 
        {
            String threat = threats.get(random.nextInt(threats.size()));
            IdsAlarmSeverityLevel sever = IdsAlarmSeverityLevel.getValue("MEDIUM");
            Double certain = random.nextDouble() * 100;
            EnvelopeUid Uid = new EnvelopeUid("11", "0");
            HeaderParams hp = generateHeader( Uid, 29.346091, -3.396503, null, null, "EVENT DETECTED");
        	File fXmlFile = new File("C:/GAMMA/resource/Message Example CI.txt");
        	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        	DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        	//Document doc = dBuilder.parse(fXmlFile);
        	//String msg = getStringFromDocument(doc);
        	//producer.sendCustomMessage(topic, msg);
        	//IdsXmlDataFile msgs = new IdsXmlDataFile(doc);
        	//Element elem = doc.getDocumentElement();
        	//msg = elem.getTextContent();
        	//Element elem = doc.getDocumentElement();
        	//producer.sendThreatMessage(topic, uidSystemId, uidSequenceId, posixTime, severity, certain, threat, content);
            IdsAlarmPayload ip = generatePayload(sever, certain, threat);
            String msg = generateAlarmMessage(hp, ip);
            producer.sendCustomMessage(topic, msg);
            //System.out.println(msg);
            try
            {
            	List<GammaXmlDataFile> data = consumer.getMessagesParsedForTopic(topic);
            	for(GammaXmlDataFile str : data)
            	{
            		System.out.println(str.getMessageContentType(0));
            	}
				Thread.sleep(100);
            	
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				break;
			}
        }
        //producer.close();
    }

}
