package TestApplication;

import kafka.javaapi.producer.Producer;
import GammaXmlMessages.*;
import GammaXmlMessages.IdsAlarmPayload.IdsAlarmSeverityLevel;
import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;

import java.io.Console;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Random;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class GammaProducer 
{
    private Producer<String, String> producer;
    private final Properties properties = new Properties();
    
    private static List<String> threats = Arrays.asList("THijack", "T-IMC1", "T_AngryCat");

    public GammaProducer() {
        properties.put("metadata.broker.list", "localhost:9092, localhost:9093");
        properties.put("serializer.class", "kafka.serializer.StringEncoder");
        properties.put("partitioner.class", "TestApplication.CostantPartitioner");
        properties.put("request.required.acks", "1");
        ProducerConfig config = new ProducerConfig(properties);
        producer = new Producer(config);
    }
    
    public GammaProducer(String brockerConnection)
    {
    	//properties.put("client.id", "aepm_test");
        properties.put("metadata.broker.list", brockerConnection);
        properties.put("serializer.class", "kafka.serializer.StringEncoder");
        properties.put("partitioner.class", "TestApplication.CostantPartitioner");
        properties.put("request.required.acks", "1");
        ProducerConfig config = new ProducerConfig(properties);
        producer = new Producer(config);    	
    }
    
    public static String generateAlarmMessage(HeaderParams header, IdsAlarmPayload payload)
    {
    	String result = null;
    	try
    	{
	    	IdsMessage msg = new IdsMessage(header, payload);
	    	List<IdsMessage> msgs = Arrays.asList(msg);
	    	IdsXmlDataFile idsXml = new IdsXmlDataFile(msgs);
	    	result = idsXml.getFormedMessage();
    	}
    	catch(Exception e)
    	{
    		
    	}
    	return result;
    }
    
    public static IdsAlarmPayload generatePayload(IdsAlarmSeverityLevel severity, Double certain, String threatName, String content)
    {
    	return new IdsAlarmPayload(severity, certain, threatName, content);
    }
    
    public static HeaderParams generateHeader(EnvelopeUid Uid, Double Longt, Double Lat, String Sensitivity, String Priority, String Subject)
    {
    	Date when = new Date();
    	return new HeaderParams( Uid, when, Longt, Lat, Sensitivity, Priority, Subject);
    }
    
    public void testFunction(String msg)
    {
    	System.out.println(msg);
    }

    public void sendThreatMessage(String topic, String uidSystemId, String uidSequenceId, long posixTime, String severity, double certain, String threat, String content) throws Exception
    {
        IdsAlarmSeverityLevel sever = IdsAlarmSeverityLevel.getValue(severity);
        EnvelopeUid Uid = new EnvelopeUid(uidSystemId, uidSequenceId);
        // TODO: set normal coordinates
        double longtitude = 4.351710;
        double latitude = 50.8503;
        HeaderParams hp = generateHeader( Uid, longtitude, latitude, null, null, "EVENT DETECTED");
        IdsAlarmPayload ip = generatePayload(sever, certain, threat, content);
            
        String msg = generateAlarmMessage(hp, ip);
        KeyedMessage<String, String> kafkaMsg = new KeyedMessage<String, String>(topic, "0", msg);
        producer.send(kafkaMsg);
    }
    
    public void sendThreatMessage(String topic, String uidSystemId, String uidSequenceId, long posixTime, String severity, double certain, String threat, String content, String subject) throws Exception
    {
        IdsAlarmSeverityLevel sever = IdsAlarmSeverityLevel.getValue(severity);
        EnvelopeUid Uid = new EnvelopeUid(uidSystemId, uidSequenceId);
        // TODO: set normal coordinates
        double longtitude = 4.351710;
        double latitude = 50.8503;
        HeaderParams hp = generateHeader( Uid, longtitude, latitude, null, null, subject);
        IdsAlarmPayload ip = generatePayload(sever, certain, threat, content);
            
        String msg = generateAlarmMessage(hp, ip);
        KeyedMessage<String, String> kafkaMsg = new KeyedMessage<String, String>(topic, "0", msg);
        producer.send(kafkaMsg);
    }
    
    public void sendThreatMessage(String topic, String uidSystemId, String uidSequenceId, double longtitude, double latitude, long posixTime, String severity, double certain, String threat, String content, String subject) throws Exception
    {
        IdsAlarmSeverityLevel sever = IdsAlarmSeverityLevel.getValue(severity);
        EnvelopeUid Uid = new EnvelopeUid(uidSystemId, uidSequenceId);
        // TODO: set normal coordinates
        HeaderParams hp = generateHeader( Uid, longtitude, latitude, null, null, subject);
        IdsAlarmPayload ip = generatePayload(sever, certain, threat, content);
            
        String msg = generateAlarmMessage(hp, ip);
        KeyedMessage<String, String> kafkaMsg = new KeyedMessage<String, String>(topic, "0", msg);
        producer.send(kafkaMsg);
    }
    
    public void sendCustomMessage(String topic, String msg)
    {
    	KeyedMessage<String, String> kafkaMsg = new KeyedMessage<String, String>(topic, "0", msg);
    	producer.send(kafkaMsg);
    }
    
    protected void finalize ( ) 
    {
    	producer.close();
    }
}
