import numpy as np
import matplotlib.pyplot as plt
from time import sleep
from copy import deepcopy
import networkx as nx 

def draw_graph(A, picture_name, labs=None):
    plt.figure()
    g = nx.Graph()
    for i in range(A.shape[0]):
        for j in range(A.shape[1]):
            if A[i, j] > 0:
                g.add_edge(i, j)
    if labs is not None:
        g = nx.relabel_nodes(g, labs)
    nx.draw(g, with_labels = True) 
    plt.savefig(picture_name) 

transition_matrix = deepcopy(attack_graph)
#transition_matrix = np.zeros(shape=(14, 14))
#transition_matrix[0, np.array([1, 2, 3])] = 1
#transition_matrix[1, np.array([7, 13])] = 1
#transition_matrix[2, np.array([7, 8])] = 1
#transition_matrix[3, np.array([4, 5, 6])] = 1
#transition_matrix[4, np.array([3, 5, 8])] = 1
#transition_matrix[5, np.array([4, 9, 12, 8])] = 1
#transition_matrix[6, np.array([5, 12, 7])] = 1
#transition_matrix[7, np.array([6, 11, 12, 13])] = 1
#transition_matrix[8, np.array([9, 10])] = 1
#transition_matrix[9, np.array([8, 11])] = 1
#transition_matrix[10, np.array([10])] = 1
#transition_matrix[11, np.array([11])] = 1
#transition_matrix[12, np.array([12])] = 1
#transition_matrix[13, np.array([13])] = 1

sa_idx = list(descriptor['SA'].keys())

transition_matrix[np.eye(transition_matrix.shape[0]).astype(np.bool)] = 0.0
for sidx in sa_idx:
    transition_matrix[sidx, sidx] = 1
not_started_prob = 0.9
normaliser = np.sum(transition_matrix, axis=1)
transition_matrix = transition_matrix / np.expand_dims(normaliser, 1)
transition_matrix[0, 0] = not_started_prob
transition_matrix[0, 1:] = transition_matrix[0, 1:] / np.sum(transition_matrix[0, 1:]) * \
     (1 - not_started_prob)


#A1 = np.zeros(shape=(5, 5))
#A1[0, 1] = 1
#A1[1, 2] = 1
#A1[2, np.array([3, 4])] = 1
#labels = {3: 'sa1', 4: 'sa2'}
#draw_graph(A1, "init_graph.png", labels)
#
#A2 = np.zeros(shape=(4, 4))
#A2[0, 1] = 1
#A2[1, 2] = 1
#A2[1, 3] = 1
#labels = {2: 'sa3', 3: 'sa4'}
#draw_graph(A2, 'seq_attack_1.png', labels)
#
#A3 = np.zeros(shape=(4, 4))
#A3[0, 1] = 1
#A3[0, 2] = 1
#A3[1, 3] = 1
#A3[2, 3] = 1
#labels = {3: 'sa5'}
#draw_graph(A3, 'seq_attack_3.png', labels)
#
#
#Af = np.zeros(shape=(17, 17))
#Af[np.ix_(np.arange(5), np.arange(5))] = A1
#Af[np.ix_(np.arange(5, 9), np.arange(5, 9))] = A2
#Af[np.ix_(np.arange(9, 13), np.arange(9, 13))] = A2
#Af[np.ix_(np.arange(13, 17), np.arange(13, 17))] = A3
#Af[2, 5] = 1
#Af[2, 9] = 1
#Af[10, 13] = 1
#labels = {3: 'sa1', 4: 'sa2', 7: 'sa(1, 3)', 8: 'sa(1, 4)', 11: 'sa(2, 3)', 12: 'sa(2, 4)', 16: 'sa(2, 3, 5)'}
#draw_graph(Af, 'seq_attack_joint.png', labels)



#draw_graph(transition_matrix)

#np.array([10, 11, 12, 13])

vae_det_matr = np.zeros(shape=(transition_matrix.shape[0], 2))
nn_det_matr = np.zeros(shape=(transition_matrix.shape[0], 2))

for k in descriptor['attacks']:
    curr_start = descriptor['attacks'][k]['START']
    curr_stop = descriptor['attacks'][k]['max_node']
    value_to_set_vae = np.array(vae_det[k[0]])
    vae_det_matr[curr_start:curr_stop, :] = np.array([1 - value_to_set_vae[1], value_to_set_vae[0]])
    value_to_set_nn = np.array(nn_det[k[0]])
    nn_det_matr[curr_start:curr_stop, :] = np.array([1 - value_to_set_nn[1], value_to_set_nn[0]])
    for descr in descriptor['attacks'][k]['SA']:
        curr_sa_id = descr[-1]
        nn_det_matr[curr_sa_id, :] = np.array([1 - value_to_set_vae[1], value_to_set_vae[0]])
        vae_det_matr[curr_sa_id, :] = np.array([1 - value_to_set_nn[1], value_to_set_nn[0]])

detector_info = {'VAE': vae_det_matr, 'NN': nn_det_matr}

max_alloc_node = 10

start_det_probs = np.zeros((transition_matrix.shape[0], 2))
start_detectors_allocation = np.zeros((transition_matrix.shape[0],))
num_detectors = int(len(sa_idx) * 1.3) - len(sa_idx)
for sidx in sa_idx:
    detector_type_idx = int(np.random.uniform() > 0.5)
    start_detectors_allocation[sidx] = detector_type_idx + 1
    start_det_probs[sidx, :] = \
    detector_info[list(detector_info.keys())[detector_type_idx]][sidx, :]

all_nodes = np.array(range(transition_matrix.shape[0]))
all_nodes_no_sa = np.setdiff1d(all_nodes, sa_idx)
numbers_to_set = np.random.permutation(len(all_nodes_no_sa))[0:num_detectors]
for i in range(num_detectors):
    curr_node_idx = all_nodes_no_sa[int(numbers_to_set[i])]
    detector_type_idx = int(np.random.uniform() > 0.5)
    start_detectors_allocation[curr_node_idx] = detector_type_idx + 1
    start_det_probs[curr_node_idx, :] = \
        detector_info[list(detector_info.keys())[detector_type_idx]][curr_node_idx, :]

#probs = np.zeros(shape=(transition_matrix.shape[0], 2))
#set false positives
#probs[np.array([1, 2, 3]), 0] = 0.1
#probs[8, 0] = 0.5
#probs[np.array([4, 7, 9]), 0] = 0.2
#probs[sa_idx, 0] = 0.0

#set false negatives
#probs[np.array([1, 2, 3]), 1] = 0.8
#probs[np.array([4, 7, 8, 9]), 1] = 0.9
#probs[sa_idx, 1] = 1

detectors_idx = np.where(start_det_probs[:, 1] > 0)[0]

start_prob = np.zeros(shape=(start_det_probs.shape[0],))
start_prob[0] = 1.0

#generate_false_positives = True
curr_prob_distr = start_prob

plt.ion()

fig = plt.figure()
ax = fig.add_subplot(111)

distr = None
distr_sqr = None
prob_vals = []
prob_vals_projected = []
num_summations = 0

def estimate_distribution(probs, prev_distr, fp_idx):
    detectors_idx = np.where(probs[:, 1] > 0)[0]
    try:
        no_fp_idx = np.setdiff1d(detectors_idx, fp_idx)
    except:
        print('Fail(')
    diag_vals = np.ones(shape=(probs.shape[0],))
    if len(fp_idx) > 0:
        diag_vals[fp_idx] = probs[fp_idx, 1] / probs[fp_idx, 0]
    diag_vals[no_fp_idx] = (1 - probs[no_fp_idx, 1]) / (1 - probs[no_fp_idx, 0])
    total_detection_matrix = np.diag(diag_vals)
    curr_prob_distr = np.matmul(total_detection_matrix, np.matmul(transition_matrix.transpose(), prev_distr))
    curr_prob_distr /= np.sum(curr_prob_distr)
    return curr_prob_distr

def simmetric_kl_div(dstr1, dstr2):
    dstr1[dstr1 == 0] = 1e-6
    dstr2[dstr2 == 0] = 1e-6
    dstr1 = dstr1 / np.sum(dstr1)
    dstr2 = dstr2 / np.sum(dstr2)
    res = 0.5 * (np.matmul(dstr1, np.log(dstr2)) + np.matmul(np.log(dstr1), dstr2)) 
    return res

    

fp_average_rates = []
kl_divergence = []
abs_probdistr_diff = []
stds = []

#for j in range(100):
#j = 10
#probs[np.array([1, 2, 3]), 0] = 0.005 * j + 0.001
#probs[8, 0] = 0.005 * j + 0.01
#probs[np.array([4, 7, 9]), 0] = 0.005 * j + 0.01
#probs[sa_idx, 0] = 0.005 * j + 0.01
#fp_average_rates.append(0.005 * j)
def calculate_stats_mc(generate_false_positives, probs, start_prob_distr):
    distr = None
    curr_prob_distr = start_prob_distr
    for i in range(10000):
        fp_idx = np.array([])
        if generate_false_positives:
            fp_idx_rand = np.random.uniform(size=probs.shape[0])
            fp_idx = np.where(probs[:, 0] > fp_idx_rand)
        curr_prob_distr = estimate_distribution(probs, curr_prob_distr, fp_idx)
        if i > 100:
            if distr is None:
                distr = curr_prob_distr
                distr_sqr = curr_prob_distr * curr_prob_distr
                num_summations = 1
            else:
                distr = distr / (num_summations + 1) * num_summations + curr_prob_distr / (num_summations + 1)
                distr_sqr = distr_sqr * (num_summations / (num_summations + 1)) + curr_prob_distr * curr_prob_distr / (num_summations + 1)
                num_summations += 1
    distr_std = np.sqrt(distr_sqr - distr * distr)
    #stds.append(distr_std)
    #res_std_lower = res - 3 * distr_std[10:]
    #res_std_upper = res + 3 * distr_std[10:]
    return distr, distr_std
    #prob_distr = start_prob
#estimated value
#for i in range(70):
#    tmp = np.zeros_like(prob_distr)
#    all_probs = []
#    for j in range(pow(2, len(detectors_idx))):
#        code = [int(x) for x in bin(j)[2:]]
#        code.reverse()
#        fp_idx = detectors_idx[np.where(np.array(code) > 0)]
#        tmp_prob_distr = estimate_distribution(prob_distr, fp_idx)
#        no_fp_idx = np.setdiff1d(detectors_idx, fp_idx)
#        fp_prob = np.prod(probs[fp_idx, 0])
#        detection_probability = np.prod(1 - probs[no_fp_idx, 0]) * fp_prob
#        tmp += detection_probability * tmp_prob_distr
#        all_probs.append(detection_probability)
#    prob_distr = tmp
#kl_div = -simmetric_kl_div(prob_distr, distr)
#print(kl_div)
#kl_divergence.append(kl_div)

#abs_diff = np.max(np.abs(prob_distr - distr))
#abs_probdistr_diff.append(abs_diff)
        #print('Curr estimate')
        #print(prob_distr[10:])
        #print('Obtained numerical estimate, lower and upper')
        #print(res_std_lower)
        #print(res_std_upper)
        #print(' ')

    
    

def modify_solution(probs, detector_info, detector_allocation, sd):
    new_probs = deepcopy(probs)
    new_det_alloc = deepcopy(detector_allocation)
    position_without_detectors = np.setdiff1d(np.where(new_det_alloc[1:] == 0)[0] + 1, sa_idx)
    position_with_detectors = np.setdiff1d(np.where(new_det_alloc > 0)[0], sa_idx)
    print('Without and with detectors: ' + str([len(position_without_detectors), len(position_with_detectors)]))
    for j in range(sd):
        ind1 = position_with_detectors[np.random.randint(0, len(position_with_detectors))]
        ind2 = position_without_detectors[np.random.randint(0, len(position_without_detectors))]
        curr_det_type = list(detector_info.keys())[int(new_det_alloc[ind1]) - 1]
        new_det_alloc[ind2] = new_det_alloc[ind1]
        new_det_alloc[ind1] = 0
        new_probs[ind1] = np.array([0, 0])
        position_with_detectors = np.setdiff1d(position_with_detectors, [ind1])
        
        position_without_detectors = np.append(position_without_detectors, [ind1])
        new_probs[ind2] = deepcopy(detector_info[curr_det_type][ind2])
        position_with_detectors = np.append(position_with_detectors, [ind2])
        position_without_detectors = np.setdiff1d(position_without_detectors, [ind2])
        ind3 = position_with_detectors[np.random.randint(0, len(position_with_detectors))]
        if np.sum(new_det_alloc[position_without_detectors]) > 0:
            print('error detected!')
        to_change = np.random.uniform() > 0.5
        if to_change:
            curr_det_type = int(new_det_alloc[ind3] - 1) #list(detector_info.keys())[detector_allocation[ind3]]
            new_det_type = (curr_det_type + 1) % len(detector_info)
            new_det_name = list(detector_info.keys())[new_det_type]            
            new_probs[ind3] = deepcopy(detector_info[new_det_name][ind3])
            new_det_alloc[ind3] = new_det_type + 1
    return new_probs, new_det_alloc
        
def traget_function(d_m, d_v, d_m_nfp):
    res_std_upper = d_m + 3 * d_v
    return 5 * (1 - d_m_nfp[0]) + np.sum(res_std_upper[4:]) 

def evaluate_target_function(probs, start_distr):
    distr_mean, distr_var = calculate_stats_mc(True, probs, start_distr)
    distr_mean_nfp, _ = calculate_stats_mc(False, probs, start_distr)
    tf = traget_function(distr_mean, distr_var, distr_mean_nfp)   
    return tf

vals = []
temperats = []


def simulated_annealing(curr_probs, start_prob_distr, detector_info, start_detectors_allocation, temperature):
    temp_start = temperature
    best_probs = deepcopy(curr_probs)
    best_alloc = deepcopy(start_detectors_allocation)
    curr_tf = evaluate_target_function(curr_probs, start_prob_distr)
    k = 0
    last_upd = 0
    sd = 1
    for i in range(10):
        sd = max([int(temperature * 3000), 1])
        candidate_probs, candidate_alloc = modify_solution(best_probs, detector_info, best_alloc, sd)
        candidate_tf = evaluate_target_function(candidate_probs, start_prob_distr)
        prob_threshold = min([1.0, np.exp(-(candidate_tf - curr_tf) / temperature)])
        print('Comparing values ' + str(candidate_tf) + ' and ' + str(curr_tf))
        if np.random.rand() < prob_threshold:
            best_probs = candidate_probs
            best_alloc = candidate_alloc
            last_upd = i
            curr_tf = candidate_tf
            temperature = temp_start / np.sqrt(k + 1)
            k = k + 1
            print('New current point: ' + str([candidate_tf, temperature]))
            temperats.append(temperature)
        if i - last_upd > sd * 25:
            sd = min([3, sd + 1])
        else:
            sd = 1    
        print('Current step: ' + str(i))
        vals.append(curr_tf)
    return best_probs, best_alloc 

#enhanced_probs, enhanced_allocation = simulated_annealing(start_det_probs, start_prob, 
#                                         detector_info, start_detectors_allocation, 0.05)
 

def generate_reaction_for_nn_and_vae(host_ip, label_ml_markup):
    attack_type_graph = None
    if label_ml_markup > 0:
        for k in labdict:
            if labdict[k] == label_ml_markup:
                label_ml_name = k
                break
        if label_ml_name is None:
            raise Exception('Inknown ML label!')
        attack_type_graph = attack_correspondence[label_ml_name]

    graph_host_number = None
    for k in all_host_ips:
        if all_host_ips[k] == host_ip:
            graph_host_number = k
            break
    result_fp = []
    result_tp = []
    if graph_host_number is None:
        raise Exception('Inknown Host IP!')
    denom_const = len(vae_det)
    if attack_type_graph is not None:
        denom_const = denom_const - 1
    for att_type in vae_det:
        idx = 1
        
        result = result_fp
        vae_const = np.power(vae_det[att_type][idx], 1.0 / denom_const)
        nn_const = np.power(nn_det[att_type][idx], 1.0 / denom_const)
        if attack_type_graph == att_type:
            idx = 0
            result = result_tp
            vae_const = 1 - vae_det[att_type][idx]
            nn_const = 1 - nn_det[att_type][idx]
        is_vae_activated = vae_const < np.random.uniform()
        is_nn_activated = nn_const < np.random.uniform()
        if(is_vae_activated):
            result.append((att_type, graph_host_number, 1))
        if(is_nn_activated):
            result.append((att_type, graph_host_number, 2))
            #host_nodes_range = np.array(descriptor['hosts'][host_ip])
            #host_attack_probs = probs[host_nodes_range, 0]
            #detections_idx = host_attack_probs < np.random.uniform(shape=host_attack_probs.shape)
            #gen_idx = host_nodes_range[detections_idx]

    return result_fp, result_tp

def get_host_attack_nodes(host_ip, attack_name):
    graph_host_number = None
    for k in all_host_ips:
        if all_host_ips[k] == host_ip:
            graph_host_number = k
            break
    min_val = descriptor['attacks'][(attack_type_graph, graph_host_number)]['START']
    max_val = descriptor['attacks'][(attack_type_graph, graph_host_number)]['max_node']
    
    host_nodes_range = np.array([range(min_val, max_val)])
    return host_nodes_range

def evaluate_detection_quality_tp(prob_distr, host_ip, attack_name):
    host_nodes = get_host_attack_nodes(host_ip, attack_name)
    return np.sum(prob_distr[host_nodes])

def evaluate_detection_quality_fp(prob_distr):
    return prob_distr[0]

def generate_detections_for_attack(host_ip, reacted_algorithms, allocation):
    active_nodes_idx = []
    for (attack_type_graph, graph_host_number, algo_id) in reacted_algorithms:
        
        min_val = descriptor['attacks'][(attack_type_graph, graph_host_number)]['START']
        max_val = descriptor['attacks'][(attack_type_graph, graph_host_number)]['max_node']
        
        host_nodes_range = np.array([range(min_val, max_val)])
        
        detections_lidx = allocation[host_nodes_range] == algo_id
        if detections_lidx.any():
            curr_idx = list(host_nodes_range[detections_lidx])
            active_nodes_idx.extend(curr_idx)
    return active_nodes_idx

def create_roc_curve(counter):
    measures = np.array([])
    labels = np.array([])
    for k in counter:
        lab = 1
        data_to_add = None
        if k == 'BENIGHT':
            lab = 0
            data_to_add = np.array(counter[k])
        else:
            data_to_add = np.array(counter[k])[:, 0]
        measures = np.concatenate((measures, data_to_add))
        labels = np.concatenate((labels, lab * np.ones(data_to_add.shape)))
    idx = np.argsort(measures)
    labels_sorted = labels[idx]
    tp = np.cumsum(labels_sorted) / np.sum(labels_sorted)
    fp = np.cumsum(1 - labels_sorted) / np.sum(1 - labels_sorted)
    plt.plot(fp, tp)

#create_roc_curve(enahnced_counter_per_label)
#create_roc_curve(random_counter_per_label)

distribution_enhanced = np.zeros(transition_matrix.shape[0])
distribution_enhanced[0] = 1.0

enahnced_counter_per_label = {'BENIGHT': []}
random_counter_per_label = {'BENIGHT': []}

distribution_random = np.zeros(transition_matrix.shape[0])
distribution_random[0] = 1.0 

nn_global_tp = 0
vae_global_tp = 0

nn_counter_per_label = {'BENIGHT': 0}
vae_counter_per_label = {'BENIGHT': 0}

global_counter = {'BENIGHT': 0}

nn_global_fp = 0
vae_global_fp = 0

for i in range(data.shape[0]):
    if i % 1000 == 0:
        print('Processing data item ' + str(i) + ' from ' + str(data.shape[0]))
    curr_host_ip = destination_ip_labels[i]
    lab = labs[i]
    if lab == 4:
        curr_host_ip = source_ip_labels[i]
    real_ip = None
    for k in source_ip_dicts:
        if source_ip_dicts[k] == curr_host_ip:
            real_ip = k
            break
    if real_ip is None:
        continue
    if real_ip not in descriptor['hosts']:
        continue
    reactions_fp, reactions_tp = generate_reaction_for_nn_and_vae(real_ip, lab)
    reactions_idx_tp_enhanced = generate_detections_for_attack(real_ip, reactions_tp, enhanced_allocation)
    reactions_idx_fp_enhanced = generate_detections_for_attack(real_ip, reactions_fp, enhanced_allocation)
    
    reactions_enhanced = deepcopy(list(reactions_idx_fp_enhanced))
    reactions_enhanced.extend(list(reactions_idx_tp_enhanced))
    reactions_enhanced = np.unique(reactions_enhanced)
    
    reactions_idx_tp_random = generate_detections_for_attack(real_ip, reactions_tp, start_detectors_allocation)
    reactions_idx_fp_random = generate_detections_for_attack(real_ip, reactions_fp, start_detectors_allocation)
    
    reactions_random = deepcopy(list(reactions_idx_fp_random))
    reactions_random.extend(list(reactions_idx_tp_random))
    reactions_random = np.unique(reactions_random)


    distribution_enhanced = estimate_distribution(enhanced_probs, distribution_enhanced, reactions_enhanced)
    distribution_random = estimate_distribution(start_det_probs, distribution_random, reactions_random)
    
    label_ml_name = None
    if lab > 0:
        for k in labdict:
            if labdict[k] == lab:
                label_ml_name = k
                break
        if label_ml_name is None:
            raise Exception('Inknown ML label!')
        attack_type_graph = attack_correspondence[label_ml_name]
        if attack_type_graph not in nn_counter_per_label:
            nn_counter_per_label[attack_type_graph] = nn_det[attack_type_graph][0]
            vae_counter_per_label[attack_type_graph] = vae_det[attack_type_graph][0]
            
            random_counter_per_label[attack_type_graph] = [(distribution_random[0], evaluate_detection_quality_tp(distribution_random, real_ip, attack_type_graph))]
            enahnced_counter_per_label[attack_type_graph] = [(distribution_enhanced[0], evaluate_detection_quality_tp(distribution_enhanced, real_ip, attack_type_graph))]
            
            global_counter[attack_type_graph] = 1
        else:
            nn_counter_per_label[attack_type_graph] += nn_det[attack_type_graph][0]
            vae_counter_per_label[attack_type_graph] += vae_det[attack_type_graph][0]
            
            random_counter_per_label[attack_type_graph].append((distribution_random[0], evaluate_detection_quality_tp(distribution_random, real_ip, attack_type_graph)))
            enahnced_counter_per_label[attack_type_graph].append((distribution_enhanced[0], evaluate_detection_quality_tp(distribution_enhanced, real_ip, attack_type_graph)))
            
            global_counter[attack_type_graph] += 1
    else:
        nn_counter_per_label['BENIGHT'] += 1 - 0.97
        vae_counter_per_label['BENIGHT'] += 1- 0.97
            
        random_counter_per_label['BENIGHT'].append(evaluate_detection_quality_fp(distribution_random))
        enahnced_counter_per_label['BENIGHT'].append(evaluate_detection_quality_fp(distribution_enhanced))
            
        global_counter['BENIGHT'] += 1
    
create_roc_curve(enahnced_counter_per_label)
create_roc_curve(random_counter_per_label)