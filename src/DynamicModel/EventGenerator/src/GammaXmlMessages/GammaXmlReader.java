package GammaXmlMessages;

import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.validation.*;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class GammaXmlReader 
{
	
	private DocumentBuilderFactory dbf;
	
	private DocumentBuilder db;
	
	private Validator _validator;
	
	String _msg = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><messages><message><envelope><uid><system_id>11</system_id><sequence_number>0</sequence_number></uid><when><since_epoch>1440517426688</since_epoch></when><subject>EVENT DETECTED</subject><location><lat>-3.396503</lat><lon>29.346091</lon></location></envelope><payload><alarm><severity level=\"MEDIUM\"/><certainty>3.354337038292543</certainty><threat>THijack</threat></alarm></payload></message></messages>";
	
	
	public GammaXmlReader(String PathToDtd) throws Exception
	{
		
		dbf = DocumentBuilderFactory.newInstance();
		
		dbf.setNamespaceAware(true);
		
	}
	
	public Document ParseXmlDocument(String input) throws Exception
	{
		DocumentBuilder builder = dbf.newDocumentBuilder();
		
		//byte[] msgBytes = input.getBytes();
		
		return builder.parse(new InputSource(new StringReader(input)));
	}
}
