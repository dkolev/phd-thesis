function [ AdjMatr, StartID, StopID, ExpandedAssetID, ExpandedEnterID, Correspondance ] = ...
    CreateExpandedGraph4VC( ThreatGraph, AttackTypeNum, AssetID, EnterID,...
    GeoNodes, CyberNodes, PossibleTypesOfAttacksMatrix)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
% PossibleTypesOfAttacksMatrix - first row: geonodes, second row:
% cybernodes
%% Split vertices for cutting in the initial graph
n = size(ThreatGraph, 1);

V2C = setdiff(1:n, [AssetID, EnterID]); %vertex to cut

tmp = repmat(V2C, n, 1);

Correspondance = cell(length(V2C), 2);
%preparing for vertex cut
NewEntryIndeces = sum(tmp < repmat((1:n)', 1, length(V2C)), 2) + (1:n)';
NewExitIndeces = NewEntryIndeces;
NewExitIndeces(V2C) = NewExitIndeces(V2C) + 1;

ThrGr = zeros(n + length(V2C));

ThrGr(NewEntryIndeces, NewExitIndeces) = ThreatGraph;

NewGeoArcs = [];
NewCyberArcs = []; %we ned to know what should we cut.

for i=1:length(V2C)
    ThrGr(NewExitIndeces(V2C(i)), NewEntryIndeces(V2C(i))) = 1;
    Correspondance{i, 1} = V2C(i);
    if(ismember(V2C(i), GeoNodes))
        NewGeoArcs = [NewGeoArcs; NewEntryIndeces(V2C(i)), NewExitIndeces(V2C(i))];
    end
    if(ismember(V2C(i), CyberNodes))
        NewCyberArcs = [NewCyberArcs, NewEntryIndeces(V2C(i)), NewExitIndeces(V2C(i))];
    end        
end

ThreatGraph = ThrGr;
AssetID = NewEntryIndeces(AssetID)';
EnterID = NewEntryIndeces(EnterID)';

n = size(ThreatGraph, 1);

%% Create block matrix (expanded graph)
%maping to the extanding graph
%check if given types of attacks are 
Blocks = cell(AttackTypeNum, 1);
tmpThreatGraph = ThreatGraph;
for i=1:AttackTypeNum
    tmpThreatGraph = ThreatGraph;
    if(PossibleTypesOfAttacksMatrix(i, 1)==0)
        tmpThreatGraph(AssetID, NewGeoArcs(:, 2)) = 0;
    end
    if(PossibleTypesOfAttacksMatrix(i, 2)==0)
        tmpThreatGraph(AssetID, NewCyberArcs(:, 2)) = 0;
    end
    Blocks{i} = tmpThreatGraph;
end

ToAdd = 0:n:( (n-1)*AttackTypeNum );

tmp = blkdiag(Blocks{:}); % create block-diagonal matrix (layer-based graph)

%prepare for vertex-cut
N = size(tmp, 1);
%% Add Start node
N = size(tmp, 1);
StartID = N+1;

tmp = [tmp; zeros(1, N)];
tmp = [tmp, zeros(N+1, 1)];

EnterID_n = repmat(EnterID, AttackTypeNum, 1) + repmat(ToAdd', 1, length(EnterID)); 
EnterID_n = EnterID_n';
EnterID_nn = EnterID_n(:);

tmp(EnterID_nn, end) = 1;

%% Add Stop node
N = size(tmp, 1);
StopID = N+1;

tmp = [tmp; zeros(1, N)];
tmp = [tmp, zeros(N+1, 1)];

AssetID_n = repmat(AssetID, AttackTypeNum, 1) + repmat(ToAdd', 1, length(AssetID)); 
AssetID_n = AssetID_n';
AssetID_nn = AssetID_n(:);

tmp(end, AssetID_nn) = 1;

AdjMatr = tmp;
ExpandedAssetID = AssetID_n;
ExpandedEnterID = EnterID_n;

%% Create Correspondance between initial nodes and edges in exp. graph.

V2C_ent = NewEntryIndeces(V2C);
V2C_ext = NewExitIndeces(V2C);

V2C_ent = repmat(V2C_ent, 1, AttackTypeNum) + repmat(ToAdd, length(V2C), 1);
V2C_ext = repmat(V2C_ext, 1, AttackTypeNum) + repmat(ToAdd, length(V2C), 1);

for i=1:length(V2C)
    Correspondance{i, 2} = [V2C_ent(i, :)',V2C_ext(i, :)'];
end

end

