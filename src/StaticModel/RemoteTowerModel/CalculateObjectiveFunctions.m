function [ ImpactMatrix ] = CalculateObjectiveFunctions( GraphStructure, SupportingAssetStructure, Source, Sink, SourceFrameSize, ControlFrameSize, maxFrameSize,...
    SourceDataRate, ControlDataRate, SourceLatLimitations, SourceCapLimitations, ControlLatLimitations, ControlCapLimitations,...
    SourceTRLimitations, ControlTRLimitations, SafetyDataAssets, CapacityDataAssets, AttackTypeNum)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
AssetN = size(SupportingAssetStructure, 1);
ImpactMatrix = zeros(AssetN, AttackTypeNum, 4);
%%
S = zeros(AssetN, AttackTypeNum);
C = zeros(AssetN, AttackTypeNum);
H = zeros(AssetN, AttackTypeNum);
M = zeros(AssetN, AttackTypeNum);
for i=1:AttackTypeNum
    for j=1:AssetN
        SupportingAssetStructureA = AttackSA(i, j, SupportingAssetStructure);
        [ S(j, i), C(j, i)] = CalculateNetworkObjectiveFunction( GraphStructure, SupportingAssetStructureA, Source, Sink, SourceFrameSize, ControlFrameSize, maxFrameSize,...
        SourceDataRate, ControlDataRate, SourceLatLimitations, SourceCapLimitations, ControlLatLimitations, ControlCapLimitations,...
        SourceTRLimitations, ControlTRLimitations, SafetyDataAssets, CapacityDataAssets);
        
        H(j, i) = SupportingAssetStructure{j, 3} - SupportingAssetStructureA{j, 3}; %human losses
        
        M(j, i) = SupportingAssetStructure{j, 5} - SupportingAssetStructureA{j, 5};
    end 
end
ImpactMatrix(:, :, 1) = S;
ImpactMatrix(:, :, 2) = C;
ImpactMatrix(:, :, 3) = H;
ImpactMatrix(:, :, 4) = M;



end
