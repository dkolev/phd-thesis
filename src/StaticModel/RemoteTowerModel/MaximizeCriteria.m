function [ Criteria, D, p, x, z, nu, K, y, fval, fvalt, OptimalAttacks, IsFeasible, xCorr, xType ] = MaximizeCriteria( CostTh, TempCostTh, ImpactMatrix, ExpandedGraphStructure, Corresp,...
    ExtendedAssetID, StartID, StopID, ...
    ThreatControlMat, ProcControlMat, ChckPntControlMat, ImpactUpperBound, CostFun, TmpCostFun, ProcCost)
%UNTITLED12 Summary of this function goes here
% Here the minimization of criteria 1 is being applied.
% ImpactUpperBound - constraints only for the crit from #2 to end 

[ Aineq, bineq, Aeq, beq, f, CostRow, CostRowt, Ddim, pdim, xDim, zDim, nuDim, Kdim, yDim, xCorr, xType ] = PrepareLProblemImpactMinimization(...
    ExpandedGraphStructure, StartID, StopID,...
    Corresp, ThreatControlMat, ProcControlMat, ChckPntControlMat, CostFun, TmpCostFun, ProcCost,...
    CostTh, TempCostTh, ImpactMatrix, ExtendedAssetID);

[Aeq, beq] = ConsiderConstraints(ImpactMatrix(:, :, 2:end), ImpactUpperBound, Aeq, beq,...
    Ddim, pdim, xDim, zDim, nuDim, Kdim, yDim);

[ D, p, x, z, nu, K, y , ~, IsFeasible ] = SolveCutLP( f, Aineq, bineq, Aeq, beq,...
    Ddim, pdim, xDim, zDim, nuDim, Kdim, yDim);


if(IsFeasible)
    fval = CostRow * [D; p; x; z; nu; K; y];
    fvalt = CostRowt * [D; p; x; z; nu; K; y];
    [Criteria, OptimalAttacks] = GetMultiObjectiveCriteriaValues( ExpandedGraphStructure, ImpactMatrix, D, K, Corresp, ExtendedAssetID, StartID, StopID );
else
    fvalt = [];
    fval = [];
    Criteria = [];
    OptimalAttacks = [];
end

