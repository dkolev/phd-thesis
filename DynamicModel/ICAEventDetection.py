# -*- coding: utf-8 -*-
"""
Created on Sun Jun  2 18:38:53 2019

@author: admin
"""

import sklearn.decomposition
import numpy as np
import matplotlib.pyplot as plt

class ICAEventDetector:
    
    transformer = None
    alpha = 0.95
    dist_thr = None
    
    def __init__(self, n_components, tp_thresh = 0.9):
        self.transformer = sklearn.decomposition.FastICA(n_components)
        self.alpha = tp_thresh
    
    def fit(self, data, Y):
        data_pos = data[Y == 0, :]
        self.transformer.fit_transform(data_pos)
        dists = self.distance(data)
        idx = np.argsort(dists)
        sorted_dists = dists[idx]
        Ys = Y[idx]
        tn = np.cumsum(1 - Ys) / np.sum(1 - Ys)
        tp = (np.sum(Ys) - np.cumsum(Ys))/ np.sum(Ys)
        plt.plot(tn, tp)
        threshold_idx = np.where(tp < self.alpha)[0][0] - 1
        self.dist_thr = sorted_dists[threshold_idx]
      
    def predict(self, data):
        return self.distance(data) < self.dist_thr
        
    def distance(self, data):
        data_proj = self.transformer.inverse_transform(self.transformer.transform(data))
        distance = np.sqrt(np.sum(np.power(data_proj - data, 2), 1))
        return distance
    
    def validate(self, data, Y):
        Yp = self.predict(data)
        fp = np.sum(np.logical_and(Yp == 1, Y == 0)) / np.sum(Y == 0)
        precision = np.sum(np.logical_and(Yp == 1, Y == 1)) / np.sum(Yp == 1)
        return (fp, precision)