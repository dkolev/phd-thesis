function [ CriteriaValue, OptimalAttacks ] = GetMultiObjectiveCriteriaValues( ExpandedGraphStructure, ImpactMatrix, D, Corresp, ExtendedAssetID, StartID, StopID )
%UNTITLED13 Summary of this function goes here
%   Detailed explanation goes here
ObjectiveNumber = size(ImpactMatrix, 3);
CriteriaValue = zeros(ObjectiveNumber, 1);
OptimalAttacks = cell(ObjectiveNumber, 1);

for i=1:ObjectiveNumber
   [CriteriaValue(i), OptimalAttacks{i}] = ...
       CalculateImpact(ExpandedGraphStructure, ImpactMatrix(:, :, i), D, Corresp, ...
       ExtendedAssetID, StartID, StopID);
end

end

