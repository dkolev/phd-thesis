function [ D, p, y, z, fval, IsFeasible ] = SolveCutLP( f, Aineq, bineq, Aeq, beq, EdgesForCuttingNum, AttackTypesNum, N, AssetNum)
%UNTITLED11 Summary of this function goes here
%   Detailed explanation goes here
ctype = repmat('B', 1, length(f));
ctype(end) = 'C';
[x,fval]= cplexmilp(f, Aineq, bineq, Aeq, beq, [], [], [], [], [], ctype); 
IsFeasible = true;
if(~isempty(x))
   Ddim = EdgesForCuttingNum*AttackTypesNum;
    D = x(1:Ddim);
    p = x((Ddim+1):(Ddim+N));
    y = x((Ddim+N+1):(end-AssetNum));
    z = x((end-AssetNum+1):end); 
else
    D = [];
    p = [];
    y = [];
    z = [];
    IsFeasible = false;
end

end

