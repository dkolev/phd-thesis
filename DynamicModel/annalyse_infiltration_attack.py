# -*- coding: utf-8 -*-
"""
Created on Sun Jun  2 13:30:50 2019

@author: admin
"""

import sklearn
import sklearn.svm as svm
import numpy as np
import numpy.random as rand
import sklearn.decomposition
import matplotlib.pyplot as plt
import ICAEventDetection as ica
import NNEventDetector as nn
import VaeAutoEncoder as vae
import SimpleNeuralStatistitian as sns
import scipy.signal as sig
import TrainableNn as tnn
import SimpleTrainableNn as stn



no_attack_lab = 'BENIGN'
time_interval = 10 * 60 

external_ips = ['205.174.165.73']
servers_internal = ['192.168.10.50', '192.168.10.51']
servers_external = ['205.174.165.68','205.174.165.66']


def distribute_data_to_ranges(timestamps):
    time_start = np.min(timestamps)
    time_end = np.max(timestamps)
    num_timestamps = int(np.ceil((time_end - time_start) / time_interval))
    time_ranges_start =  time_start + np.float64(range(num_timestamps)) * time_interval
    time_ranges_end = time_ranges_start + time_interval
    time_idx = np.floor((timestamps - time_start) / time_interval)
    return time_idx

def filter_dataset(dataset):
    mask = np.logical_not(np.isfinite(dataset))
    bad_rows = np.where(mask)[0]
    good_rows_idx = np.ones(dataset.shape[0])
    good_rows_idx[bad_rows] = 0
    datasetf = dataset[good_rows_idx == 1, :]
    return datasetf, good_rows_idx

def create_local_time_freq(timestamps):
    different_times = {}
    for v in timestamps:
        if v in different_times.keys():
            different_times[v] += 1
        else:
            different_times[v] = 1
    result = np.zeros(timestamps.shape)
    for i in range(len(timestamps)):
        result[i] = different_times[timestamps[i]]
    return result

def normalise_dataset(dataset):
    #mean_val = np.reshape(np.mean(dataset, 0), (1, dataset.shape[1]))
    min_val = np.reshape(np.min(dataset, 0), (1, dataset.shape[1]))
    #std_val = np.reshape(np.std(dataset, 0), (1, dataset.shape[1]))
    #std_val[np.abs(std_val) < 1e-7 ] = 1
    datasetn = np.log(1 + dataset - np.repeat(min_val, dataset.shape[0], 0)) 
    #datasetn[datasetn > 10] = 10
    #datasetn[datasetn < -10] = -10
    return datasetn

def split_dataset_sequentially(data, Y, train_part=0.7, num_parts=5):
    diff_labs = np.unique(Y)
    data_train, data_test, lab_train, lab_test = None, None, None, None
    for y in diff_labs:
        data_type = data[Y == y, :]
        print("Processing label " + str(y) + " with " + str(data_type.shape[0]) + " samples!")
        part_size = int(data_type.shape[0] * train_part / num_parts)
        possible = True
        selection_idx = 0
        curr_start_idx = 0
        while possible:
            idx = rand.randint(curr_start_idx, max([curr_start_idx, data_type.shape[0] - \
                                                    (num_parts - selection_idx) * part_size]), 1)[0]
            if data_test is None:
                data_test = data_type[curr_start_idx:idx, :]
                lab_test = y * np.ones((data_test.shape[0],))
            else:
                data_test = np.concatenate((data_test, data_type[curr_start_idx:idx, :]))
                lab_test = np.concatenate((lab_test, y * np.ones((idx - curr_start_idx,))))
            selection_idx += 1
            stop_idx = min([idx + part_size, data_type.shape[0]])
            if data_train is None:
                data_train = data_type[idx:stop_idx, :]
                lab_train = y * np.ones((data_train.shape[0],))
            else:
                data_train = np.concatenate((data_train, data_type[idx:stop_idx, :]))
                lab_train = np.concatenate((lab_train, y * np.ones((stop_idx - idx,))))
            curr_start_idx = stop_idx
            possible = stop_idx < data_type.shape[0] and selection_idx < num_parts
        if curr_start_idx != data_type.shape[0]:
            data_test = np.concatenate((data_test, data_type[curr_start_idx:, :]))
            lab_test = np.concatenate((lab_test, y * np.ones((data_type.shape[0] - curr_start_idx,))))
    return data_train, lab_train, data_test, lab_test
            
        

def train_event_detector(data, Y):
    #data_rnd = rand.randint(0, 10, data.shape[0])
    data_train, lab_train, data_test, lab_test = split_dataset_sequentially(data, Y)
    #classifier = svm.OneClassSVM(verbose=True, kernel='linear')
    #transform = sns.SimpleNeuralStatistitian(10, 15, data.shape[1], 1, 1)
    #table_sns = transform.fit(data, Y)
    #transform = nn.NeuralNetEventDetector(1200000)
    #table_ann = transform.fit(data, Y) 
    transform = stn.ClassificationNn(data.shape[1], len(np.unique(Y)))
    table_cnn = transform.fit(data_train, lab_train, data_test, lab_test)
    transform = tnn.ClassificationVariationalAutoEncoder(50, data.shape[1], len(np.unique(Y)))
    table_vae = transform.fit(data_train, lab_train, data_test, lab_test)
    #diff = transform.distance(data)
    #print(table_vae)
    #print(table_ann)
    print(table_vae)
    print(table_cnn)
    return table_vae, table_cnn

def analyse_infiltration_attack(data, links, timestamps, labels):
    #internal_server_connections = [(v, i) for v, i in enumerate(links) if v[1]  in servers_internal]
    #external_server_connections = [(v, i) for v, i in enumerate(links) if v[1]  in servers_external]
    dataf, good_rows = filter_dataset(data)
    timestamps = timestamps[good_rows == 1]
    labels = labels[good_rows == 1]
    #local_freq = create_local_time_freq(timestamps)
    #dataf = np.c_[dataf, local_freq]
    datafn = normalise_dataset(dataf)
    #time_idx = distribute_data_to_ranges(timestamps)
    #data_proortion_train = data.shape[0] 
    train_data = datafn
    train_labels = labels
    transform = train_event_detector(train_data, train_labels)
    return transform
    


cl = analyse_infiltration_attack(np.float64(data), [], time_vals, labs)