function [ output_args ] = AEPM1( paths, types, ipAddress, port, brockerConnString, controlDetections )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
% Convention - detector is placed on the second (!) node of the expanded
% graph
%TODO: now according to the path structure it is possible to make a cyber
%attack against maneouvring area. Need to filter the paths according to the
%attack types

load('DetectionData29092015.mat');
airportImage = imread('templateAirport.png');
RegNodesNum = size(ThreatGraph, 1);
ExpandNodesNum = size(ExpandedGraphStructure, 1);

myReceiver = TestApplication.GammaConsumer(ipAddress, port);
mySender = TestApplication.GammaProducer(brockerConnString);

[~, initPath] = ConvertExpandPathList( paths, Corresp, ExpandedAssetID, ExpandedEnterID,...
AssetID', EnterID', StartID, StopID, ExpandNodesNum );

% Setup detectors
[EventDetectorsFP, EventDetectorsTP, ExpEventDetectorsFP, ExpEventDetectorsTP, linkedNodes] = ...
    SetupDetectorsAirportModel(Corresp, controlDetections);

%SetupSecuritySkills; 

% Draw graph
[handle, GraphStructure] = ShowGraph( ThreatGraph, EnterID, CyberNodes, AssetID, [] );
context.oldNode = [];

[ ~, airportImageContext ] = InitAirportDrawing( airportImage );

[SecurityControlSkills, SecurityControlsExpanded] = ...
    SetupSecuritySkills(Corresp, RegNodesNum, ExpandNodesNum, AttackTypesNum);

%setup predictor parameters
betta = 1;
nu_s = 1;
mu = 0;
p = ones(size(paths, 1), 1) / size(paths, 1); % no prior knowledge of attack


topic = 'GAMMA_XML';

while(1)
    detections = GetDetections(myReceiver, topic, controlDetections);
    if(isempty(detections))
       pause(1);
       continue;
    end
    for i = 1:size(detections, 1)
        allDetections = detections{i, 1};
        auxInfo = detections{i, 2};
        if(isempty(allDetections))
            pause(1);
            continue;
        end
        disp('New detections!');
        for j = 1:length(allDetections)
            IDX = find(ExpEventDetectorsFP(:, 1) == allDetections(j));
            dFP = ExpEventDetectorsFP(IDX, 2);
            dTP = ExpEventDetectorsTP(IDX, 2);
            for jj = 1:length(IDX)
                [ m_new, betta_new, p_new ] = ...
                    updateDistribution( mu, betta, nu_s, p, allDetections(j), dFP(jj), dTP(jj), paths, SecurityControlsExpanded, types );

                mu = m_new;
                betta = betta_new;
                p = p_new;
            end
        end
        initDet = [];
        % select 'red' and 'yellow' paths
        % TODO: this approach is idiotic. It should depend on the expected FP rate for path. Change it, bastard!!
        idxRed = p == max(p);
        prob = sum(p(idxRed));
        redPath = initPath(idxRed, :);
        redTypes = types(idxRed);
        attackMessage = strjoin(AttackTypes(unique(redTypes)));
        currTime = uint64(java.lang.System.currentTimeMillis());
        seqNumb = num2str(currTime, '%15.0f');
        subject = 'ATTACK_PREDICTION';
        content = GetListOfSaUnderAttack(paths(idxRed, :), AssetNames, AttackTypes, AssetID, ExpandedAssetID);
        longtitude = auxInfo.longtitude;
        latitude = auxInfo.latitude;
        mySender.sendThreatMessage(topic, '100', seqNumb, longtitude, latitude, currTime, 'LOW', prob, attackMessage, content, subject);
        %disp(['Attack Type Probability: ' num2str(sum(p(types == currType)))]);
        pause(0.1);
    end
end
end

function contentMsg = GetListOfSaUnderAttack(paths, assetNames, AttackTypes, AssetID, ExpandedAssetID)

allNodes = unique(paths(:));
memberMatr = ismember(ExpandedAssetID, allNodes);
contentMsg = 'Predicted Attacks: ';
for i = 1:length(AssetID)
   
    if(sum(memberMatr(i, :)) > 0)
       
        subMsg = ['against ', assetNames{i}, ': '];
        currTypes = strjoin(AttackTypes(find(memberMatr(i, :)==1)));
        subMsg = [subMsg, currTypes, ';'];
        contentMsg = [contentMsg, subMsg];
        
    end
    
end

end

function detections = GetDetections(receiver, topic, controlDetections)

detections = cell(0, 2);
try
    messages = receiver.getMessagesParsedForTopic(topic);
catch e
   return; 
end
if(messages.size() == 0)
    return;
end
for i = 0:(messages.size()-1) 
    currMsg = messages.get(i);
    currData = currMsg.getMessageContent(0);
    currType = currMsg.getMessageContentType(0);
    if(~isempty(currData))
       
        try
            [newDetections, auxInfo] = ...
                ParseMessageContent(currData, currType, controlDetections, currMsg);
            if(any(isnan(newDetections)))
               continue; 
            end
            detections{end + 1, 1} = newDetections;
            detections{end, 2} = auxInfo;
        catch
            
        end
        
    end
end

end

function [detections, auxData] = ParseMessageContent(content, type, controlDetections, msg)
type = char(type);
if(strcmp(type, 'string'))
    detections = str2num(char(content));
else
    res = find(strcmp(controlDetections(:, 1), type));
    detections = controlDetections{res(1), 3}; % VERY DIRTY SOLUTION!
end
auxData = struct;
auxData.longtitude = 4.765577;
auxData.latitude = 52.310342;
end

function [EventDetectorsFP, EventDetectorsTP, ExpEventDetectorsFP, ExpEventDetectorsTP, linkedNodes] = ...
    SetupDetectorsAirportModel(corresp, controlDetections)
%setup detectors and translate them to expanded graph

LogDetectorFP = [0.001, 0, 0, 0.001, 0, 0];
LogDetectorTP = [0.8, 0, 0, 0.6, 0, 0];

SignalDlrDetectorFP = [0, 0, 0.01, 0, 0, 0.01];
SignalDlrDetectorTP = [0, 0, 0.95, 0, 0, 0.90];

CameraDetectorFP = [0.001, 0.001, 0.001, 0, 0.001, 0];
CameraDetectorTP = [0.7, 0.9, 0.8, 0, 0.9, 0];

SignalDetectorFP = [0, 0, 0.003, 0, 0, 0.003];
SignalDetectorTP = [0, 0, 0.9, 0, 0, 0.9];

CameraNodes = [19:31, 36:38];
SignalNodes = [28:31, 36:39, 41:44];
LogNodes = [58, 60:64, 66:69];
%todo: extract as a function and make a conf file
dlrID = find(strcmp(controlDetections(:, 1), 'sacom_alarm'), 1, 'first');
DlrNodes = controlDetections{dlrID, 2};
EventDetectorsFP = ...
    [CameraNodes', CameraDetectorFP(ones(length(CameraNodes), 1), :);...
    SignalNodes', SignalDetectorFP(ones(length(SignalNodes), 1), :);...
    LogNodes', LogDetectorFP(ones(length(LogNodes), 1), :);...
    DlrNodes', SignalDlrDetectorFP(ones(length(DlrNodes), 1), :)];

EventDetectorsTP = ...
    [CameraNodes', CameraDetectorTP(ones(length(CameraNodes), 1), :);...
    SignalNodes', SignalDetectorTP(ones(length(SignalNodes), 1), :);...
    LogNodes', LogDetectorTP(ones(length(LogNodes), 1), :);...
    DlrNodes', SignalDlrDetectorTP(ones(length(DlrNodes), 1), :)];

[ExpEventDetectorsFP, ExpEventDetectorsTP, linkedNodes] = ...
    ExpandEventDetetors(EventDetectorsFP, EventDetectorsTP, corresp);

end

function [ExpEventDetectorsFP, ExpEventDetectorsTP, linkedNodes] = ExpandEventDetetors(EventDetectorsFP, EventDetectorsTP, corresp)
ExpEventDetectorsFP = zeros(0, 2);
ExpEventDetectorsTP = zeros(0, 2);
N = size(EventDetectorsFP, 1);
linkedNodes = zeros(N, size(corresp{1, 2}, 1)); %matrix with NumAttackTypes columns
nodeList = cell2mat(corresp(:, 1));
for i = 1:N
   currNode = EventDetectorsFP(i, 1);
   detectableAttacks = find(EventDetectorsTP(i, 2:end) > 0);
   ID = find(nodeList == currNode);
   if(isempty(ID))
      continue; 
   end
   tmp = corresp{ID, 2}; %matrix of expanded links that correspond to the initial node with the detector
   detectNodes = tmp(detectableAttacks, 2);
   ExpEventDetectorsFP = [ExpEventDetectorsFP; [detectNodes, EventDetectorsFP(i, detectableAttacks + 1)']];
   ExpEventDetectorsTP = [ExpEventDetectorsTP; [detectNodes, EventDetectorsTP(i, detectableAttacks + 1)']];
   linkedNodes(i, detectableAttacks) = detectNodes';
end

end

function [ExpControls] = ExpandSecurityControls(Controls, corresp, ExpandNodesNum)
%Controls - matrix with number of rows equal to InitNodesNum, number of
%columns - AttackTypeNum
ExpControls = zeros(ExpandNodesNum, 1);
IDX = find(sum(Controls, 2) > 0);

correspNodes = cell2mat(corresp(:, 1));

for i = 1:length(IDX)
   currNode = IDX(i);
   correspIDX = find(correspNodes == currNode);
   detectableAttacks = find(Controls(currNode, :) > 0);
   tmp = corresp{correspIDX, 2}; %matrix of expanded links that correspond to the initial node with the detector
   expNodes = tmp(detectableAttacks, 2); %convention - second node is used for control placement
   expSkills = Controls(currNode, detectableAttacks)';
   ExpControls(expNodes) = expSkills;
end

end

function [SecurityControlSkills, SecurityControlsExpanded] = SetupSecuritySkills(Corresp, InitNodesNum, ExpandNodesNum, AttackNums)

SecurityControlSkills = zeros(InitNodesNum, AttackNums);
% setup security
%                         CYB PHY INT CYB_I PHY_I INT_I
SecurityControlSkills(19, :) = [0 5 3 0 1 1];
SecurityControlSkills(20, :) = [0 4 2 0 1 1];
SecurityControlSkills(22, :) = [0 5 4 0 2 2];
SecurityControlSkills(28, :) = [2 6 2 0 4 2];
SecurityControlSkills(30, :) = [2 6 4 0 4 2];
SecurityControlSkills(58, :) = [3 0 0 2 0 0];
SecurityControlSkills(63, :) = [4 0 0 2 0 0];
SecurityControlSkills(61, :) = [6 0 0 4 0 0];
SecurityControlSkills(60, :) = [6 0 0 4 0 0];
SecurityControlSkills(19, :) = [0 5 3 0 1 1];
SecurityControlSkills(57, :) = [3 3 3 1 3 2];
SecurityControlSkills(43, :) = [2 3 3 1 3 2];

SecurityControlsExpanded = ExpandSecurityControls(SecurityControlSkills, Corresp, ExpandNodesNum);

end

function ControlStrengthPerPath = GetControlStrengthPerPath(path, attackTypes, controls)
% builds a matrix which represents for each path for each node of the path
% the skill of the  corresponding control. Attack types are taken into
% account.
ControlStrengthPerPath = zeros(size(path));
controlsAugm = [controls; zeros(1, size(controls, 2))];
attackTypesUnique = unique(attackTypes);
for i = 1:length(attackTypesUnique)
    IDX = (attackTypes == attackTypesUnique(i));
    ControlStrengthPerPath(IDX, :) = controls;
end

end

function xmlDescription = ProduceThreatsDescription(redPath, redTypes, AttackTypes, probabilities, ranks, impacts, AssetID, ExpandedAssetID, assetNames)

docNode = com.mathworks.xml.XMLUtils.createDocument... 
    ('detected_threats');

docRootNode = docNode.getDocumentElement();

for i=1:size(redPath, 1)
   
    attType = AttackTypes{redTypes(i)};
    path = path(i, :);
    
    lzID = find(path == 0, 1, 'first');
    assetExpId = path(lzID - 2);
    [r, c] = ExpandedAssetID == assetExpId;
    assetName = assetNames{AssetID(r)};
    
    
    prob = probabilities(i);
    rank = ranks(i);
    impact = impacts(i);
    threatElem = ProduceThreatDescription(docNode, path, attType, prob, rank, impact, assetName);
    
end

xmlDescription = [];

end

function threatElement = ProduceThreatDescription(docNode, path, type, prob, rank, impact, assetName)

threatElement = docNode.createElement('threat'); 
threatName = [assetName, ' : ', type];
threatDescription = ['Attack ', type, ' ', 'against ' assetName];
%threat name
threatNameElem = docNode.createElement('threat_name');
threatNameElem.appendChild... 
        (docNode.createTextNode(threatName));
threatElement.appendChild(threatNameElem);   

%threat description
threatDescriptionElem = docNode.createElement('threat_description');
threatDescriptionElem.appendChild... 
        (docNode.createTextNode(threatDescription));% by now the decription
threatElement.appendChild(threatDescriptionElem); 
    
%threat internal description
threatIDescriptionElem = CreateInternalDescription(docNode, path);
threatElement.appendChild(threatIDescriptionElem); 




end


function descrElement = CreateInternalDescription(docNode, path)

descrElement = docNode.createElement('threat_description_internal');
path = path(path > 0); %leave only non-empty nodes
for i = 2:length(path)
   
    nodeElem = docNode.createElement('node');
    nodeElem.setAttribute('id', num2str(path(i)));
    nodeElem.setAttribute('name', ['Node ' num2str(path(i))]);
    nodeElem.setAttribute('type', 'Checkpoint');
    descrElement.appendChild(nodeElem);
    
end

end


