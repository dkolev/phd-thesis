ThreatGraphRMT = zeros(37);

%% RMT center part - physical
ThreatGraphRMT([36,37], 35) = 1;
ThreatGraphRMT(35, 34) = 1;
ThreatGraphRMT(34, 17) = 1;

%% Airside part - physical 
% Supporting assets connection
    
 % The structure itself
    ThreatGraphRMT(25:29, 24) = 1; %from start of the airside to the meadle
    ThreatGraphRMT(25:26, 25:26) = [0, 1; 1, 0];
    ThreatGraphRMT(26:27, 26:27) = [0, 1; 1, 0];
    ThreatGraphRMT(27:28, 27:28) = [0, 1; 1, 0];
    ThreatGraphRMT(28:29, 28:29) = [0, 1; 1, 0];
    
    ThreatGraphRMT(32:33, 32:33) = [0, 1; 1, 0];
    ThreatGraphRMT(31:32, 31:32) = [0, 1; 1, 0];
    ThreatGraphRMT(30, 29) = 1;
    
    ThreatGraphRMT(33, [25, 26]) = 1;
    ThreatGraphRMT(32, [26, 27]) = 1;
    ThreatGraphRMT(31, [27, 28]) = 1;
    
    ThreatGraphRMT(24, [23, 21]) = 1;
    ThreatGraphRMT(23, 22) = 1;
    ThreatGraphRMT(22, 20) = 1;
    ThreatGraphRMT(20, 18) = 1;
    ThreatGraphRMT(21, 19) = 1;
    
%% Close-to-assets points
   ThreatGraphRMT = [ThreatGraphRMT, [eye(16); zeros(21, 16)]];
   ThreatGraphRMT = [ThreatGraphRMT; zeros(16, size(ThreatGraphRMT, 2))];
   
   % Check-point connection - airside
   CloseCheckPoints = 37;
   ThreatGraphRMT(13+CloseCheckPoints, 25) = 1; %radar and check-point
   ThreatGraphRMT([6, 9, 10, 14]+CloseCheckPoints, 32) = 1; %remote controlled info sources
   ThreatGraphRMT([7, 8]+CloseCheckPoints, 31) = 1; %runway facility
   ThreatGraphRMT(15+CloseCheckPoints, 33) = 1; %weather
   ThreatGraphRMT([3, 4]+CloseCheckPoints, 30) = 1; %connection to RMT center + aircrew
   
   % Check-point connection - RMT part
   ThreatGraphRMT([1,5]+CloseCheckPoints, 36) = 1; % RMT facility
   ThreatGraphRMT([2,12,11,16]+CloseCheckPoints, 37) = 1; %communication of RMT
%% RMT center part - cyber
   
   ThreatGraphRMT = [ThreatGraphRMT, zeros(53, 24)];
   ThreatGraphRMT = [ThreatGraphRMT; zeros(24, 77)];
   
   %attacks
   ThreatGraphRMT = ThreatGraphRMT';
   
   ThreatGraphRMT(55, 1) = 1;
   ThreatGraphRMT(58, 3) = 1;
   ThreatGraphRMT(62, 4) = 1;
   ThreatGraphRMT(64, 6) = 1;
   ThreatGraphRMT(65, 7) = 1;
   ThreatGraphRMT(66, 8) = 1;
   ThreatGraphRMT(68, 9) = 1;
   ThreatGraphRMT(69, 10) = 1;
   ThreatGraphRMT(57, 11) = 1;
   ThreatGraphRMT(72, 13) = 1;
   ThreatGraphRMT(60, 12) = 1;
   ThreatGraphRMT(74, 14) = 1;
   ThreatGraphRMT(75, 15) = 1;
   ThreatGraphRMT(77, 16) = 1;

   ThreatGraphRMT = ThreatGraphRMT';
   
   %cyber structure
   
   %ThreatGraphRMT(55, 54) = 1;
   ThreatGraphRMT([57, 60], 55) = 1;
   ThreatGraphRMT(58, 57) = 1;
   ThreatGraphRMT(62, 60) = 1;
   ThreatGraphRMT([64,65,66,68,69,72,74,75], 62) = 1;
   ThreatGraphRMT(62, [64,68,69,72,74]) = 1;
   ThreatGraphRMT([77, 55], 60) = 1;
   %ThreatGraphRMT(77, 76) = 1;
   ThreatGraphRMT(55, 77) = 1;
   %ThreatGraphRMT(64, 63) = 1;
   %ThreatGraphRMT(68, 67) = 1;
   %ThreatGraphRMT(69, 70) = 1;
   %ThreatGraphRMT(72, 71) = 1;
   %ThreatGraphRMT(74, 73) = 1;
   %ThreatGraphRMT(77, 76) = 1;
   
   %external communications
   ThreatGraphRMT(55, 38) = 1;
   ThreatGraphRMT(57, 48) = 1;
   ThreatGraphRMT(60, 49) = 1;
   ThreatGraphRMT(62, 41) = 1;
   ThreatGraphRMT(64, 43) = 1;
   ThreatGraphRMT(68, 46) = 1;
   ThreatGraphRMT(69, 47) = 1;
   ThreatGraphRMT(72, 50) = 1;
   ThreatGraphRMT(74, 51) = 1;
   ThreatGraphRMT(77, 53) = 1;
   ThreatGraphRMT(58, 40) = 1;
  
%% Solving mistakes
   ThreatGraphRMT(25:29, 24) = 0;
   ThreatGraphRMT = [ThreatGraphRMT(:, 1:23), zeros(77, 5), ThreatGraphRMT(:, 25:end)];
   ThreatGraphRMT(25, 24) = 1;
   ThreatGraphRMT(26, 25) = 1;
   ThreatGraphRMT(27, 26) = 1;
   ThreatGraphRMT(28, 27) = 1;
   ThreatGraphRMT(29, 28) = 1;
   
   ThreatGraphRMT = [ThreatGraphRMT(1:23, :); zeros(5, size(ThreatGraphRMT, 2)); ThreatGraphRMT(25:end, :)];
   ThreatGraphRMT([24, 25, 26], 23) = 1;
   ThreatGraphRMT([27, 28], 21) = 1;
   
   BadPoints = ((sum(ThreatGraphRMT, 1) == 0) & (sum(ThreatGraphRMT, 2)' == 0));
   ThreatGraphRMT(BadPoints, :) = [];
   ThreatGraphRMT(:, BadPoints) = [];
   ThreatGraphRMT(5, 56) = 1;
%% Adding parameters
    NodeNum = size(ThreatGraphRMT, 1);
    CyberNodes = 58:NodeNum;
    GeoNodes = 1:57;
    GeoAccessPoints = GeoNodes(sum(ThreatGraphRMT(1:16, GeoNodes), 1) > 0);
    CyberAccessPoints = CyberNodes(sum(ThreatGraphRMT(1:16, CyberNodes), 1) > 0);
    EntryPoints = 17:19;
    
    