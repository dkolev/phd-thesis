function [ D, p, y, z, nu, fval, IsFeasible ] = SolveCutLP( f, Aineq, bineq, Aeq, beq, EdgesForCuttingNum, AttackTypesNum, N, ydim, AssetNum)
%UNTITLED11 Summary of this function goes here
%   Detailed explanation goes here
ctype = repmat('B', 1, length(f));
Ddim = EdgesForCuttingNum*AttackTypesNum;
ctype(Ddim + N + ydim + 1) = 'C';
[x,fval]= cplexmilp(f, Aineq, bineq, Aeq, beq, [], [], [], [], [], ctype); 
IsFeasible = true;
if(~isempty(x))
    D = x(1:Ddim); %where the controls should be placed
    p = x((Ddim+1):(Ddim+N)); % node labels
    y = x((Ddim+N+1):(Ddim+N+ydim));
    z = x(Ddim+N+ydim+1); %optimization criteria
    nu = x( (Ddim+N+ydim+2):end ); %slack variables
else
    D = [];
    p = [];
    y = [];
    z = [];
    nu = [];
    IsFeasible = false;
end

end

