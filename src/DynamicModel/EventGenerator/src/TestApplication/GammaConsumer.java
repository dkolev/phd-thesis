package TestApplication;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.w3c.dom.Document;

import GammaXmlMessages.GammaXmlDataFile;
import GammaXmlMessages.GammaXmlReader;
import GammaXmlMessages.IdsXmlDataFile;
import kafka.api.FetchRequest;
import kafka.api.FetchRequestBuilder;
import kafka.api.PartitionOffsetRequestInfo;
import kafka.common.ErrorMapping;
import kafka.common.TopicAndPartition;
import kafka.consumer.ConsumerConfig;
import kafka.javaapi.*;
import kafka.javaapi.consumer.SimpleConsumer;
import kafka.javaapi.message.ByteBufferMessageSet;
import kafka.message.MessageAndOffset;
import kafka.producer.ProducerConfig;
import kafka.utils.Utils;


public class GammaConsumer 
{
	private final Properties properties = new Properties();
	private SimpleConsumer consumer;
	
	private final int _fetchrequestBuffer = 100000;
	private final int _defaultPartition = 0;
	
	private String clientName = "Gamma_Client";
	
	long lastOffset;
	
	public GammaConsumer(String a_zookeeper, int port)
	{
        Properties props = new Properties();
        props.put("zookeeper.connect", a_zookeeper);
        props.put("group.id", "1");
        ConsumerConfig config = new ConsumerConfig(props);
        lastOffset = -1;
        consumer = new SimpleConsumer(a_zookeeper, port, 10000, 10 * 1024000, clientName); //10 secs and 10MB data
	}
	
	public String[] getAllMessages(String topic)
	{
		if(lastOffset < 0)
		{
			lastOffset = getLastOffset(consumer, topic, _defaultPartition, kafka.api.OffsetRequest.LatestTime(), clientName); // reads from the moment when it was connected
		}
		String[] result = null;
		FetchRequest fetchRequest = new FetchRequestBuilder()
				.clientId(clientName)
				.addFetch(topic, _defaultPartition, lastOffset, _fetchrequestBuffer)
				.build();
		
		FetchResponse fetchResponse = consumer.fetch(fetchRequest);
		
		if (fetchResponse.hasError()) {
            // Something went wrong!
            short code = fetchResponse.errorCode(topic, _defaultPartition);
            System.out.println("Error fetching data from the Broker." + " Reason: " + code);
		}
		else
		{
			ByteBufferMessageSet messages = fetchResponse.messageSet(topic, _defaultPartition);
			List<String> msgs = new ArrayList<String>();
			for(MessageAndOffset msg : messages) 
			{
				ByteBuffer payload = msg.message().payload();
				byte[] bytes = new byte[payload.limit()];
				payload.get(bytes);
				try 
				{
					msgs.add(new String(bytes, "UTF-8"));
				} 
				catch (UnsupportedEncodingException e) 
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				lastOffset = msg.nextOffset();
			}
			result = (String[]) msgs.toArray(new String[msgs.size()]);
		}	
		return result;
	}
	
	public List<GammaXmlDataFile> getMessagesParsedForTopic(String topic)
	{
		List<GammaXmlDataFile> result = new ArrayList<GammaXmlDataFile>();
		
		String[] msgs = getAllMessages(topic);
		
		if(msgs == null)
		{
			return result;
		}
		
		for(String message : msgs)
		{
			try
			{
				GammaXmlReader reader = new GammaXmlReader("");
				Document doc = reader.ParseXmlDocument(message);
				IdsXmlDataFile newGammaMsg = new IdsXmlDataFile(doc);
				result.add(newGammaMsg);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		return result;
	}
	
	public static long getLastOffset(SimpleConsumer consumer, String topic, int partition, long whichTime, String clientName) 
	{
		TopicAndPartition topicAndPartition = new TopicAndPartition(topic, partition);
		Map<TopicAndPartition, PartitionOffsetRequestInfo> requestInfo = new HashMap<TopicAndPartition, PartitionOffsetRequestInfo>();
		requestInfo.put(topicAndPartition, new PartitionOffsetRequestInfo(whichTime, 1));
		kafka.javaapi.OffsetRequest request = new kafka.javaapi.OffsetRequest(
					requestInfo, kafka.api.OffsetRequest.CurrentVersion(), clientName);
		OffsetResponse response = consumer.getOffsetsBefore(request);
		
		if (response.hasError()) {
			System.out.println("Error fetching data Offset Data the Broker. Reason: " + response.errorCode(topic, partition) );
			return 0;
		}
		long[] offsets = response.offsets(topic, partition);
		return offsets[0];
	}

	protected void finalize( )
	{
		consumer.close();
	}
}
