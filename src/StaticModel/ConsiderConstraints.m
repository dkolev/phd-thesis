function [Aeq_new, beq_new, ProtectionRequired] = ConsiderConstraints(ImpactMatrix, ImpactUpperBound, ExtendedAssetID, Aeq, beq, Ddim, pdim, xdim)

CritNum = length(ImpactUpperBound);
tmp = reshape(ImpactUpperBound, [1,1, CritNum]);
tmp = repmat(tmp, size(ImpactMatrix, 1), size(ImpactMatrix, 2));

ProtectionRequired = sum(ImpactMatrix >= tmp, 3);
ProtectionRequired = ProtectionRequired > 0;

IDs = ExtendedAssetID(ProtectionRequired);
if(~isempty(IDs))
    NewLimNum = length(IDs);

    NewRows = zeros(NewLimNum, pdim);
    LinIDs = NewLimNum*(IDs-1) + [1:NewLimNum];
    NewRows(LinIDs) = 1;

    Aeq_new = [zeros(NewLimNum, Ddim), NewRows, zeros(NewLimNum, xdim)];
    beq_new = ones(NewLimNum, 1);

    Aeq_new = [Aeq; Aeq_new];
    beq_new = [beq; beq_new];
else
    Aeq_new = Aeq;
    beq_new = beq;
end


end