function [ Criteria, D, p, y, z, fval, OptimalAttacks, IsFeasible ] = MaximizeCriteria( CostTh, ImpactMatrix, ExpandedGraphStructure, Corresp,...
    ExtendedAssetID, StartID, StopID, ...
    ThreatControlMat, ChckPntControlMat, ImpactUpperBound, CostFun)
%UNTITLED12 Summary of this function goes here
% Here the minimization of criteria 1 is being applied.
% ImpactUpperBound - constraints only for the crit from #2 to end 

AttackTypeNum = size(ThreatControlMat, 1);
CheckPointNum = size(ChckPntControlMat, 1);
zdim = 1;

ToMinimize = ImpactMatrix(:, :, 1);

[ Aineq, bineq, Aeq, beq, f, CostRow ] = PrepareLProblemImpactMinimization(...
    ExpandedGraphStructure, StartID, StopID,...
    Corresp, ThreatControlMat, ChckPntControlMat, CostFun,...
    CostTh, ToMinimize, ExtendedAssetID);

Ddim = CheckPointNum*AttackTypeNum;
pdim = size(ExpandedGraphStructure, 1);
y_z_dim = length(f)-Ddim-pdim;


[Aeq, beq] = ConsiderConstraints(ImpactMatrix(:, :, 2:end), ImpactUpperBound, ExtendedAssetID, Aeq, beq,...
    Ddim, pdim, y_z_dim);

[ D, p, y, z, Im1, IsFeasible ] = SolveCutLP( f, Aineq, bineq, Aeq, beq,...
    CheckPointNum, AttackTypeNum, pdim, zdim);
fval = CostRow * [D; p; y];

if(IsFeasible)
    [Criteria, OptimalAttacks] = GetMultiObjectiveCriteriaValues( ExpandedGraphStructure, ImpactMatrix, D, Corresp, ExtendedAssetID, StartID, StopID );
else
    Criteria = [];
    OptimalAttacks = [];
end

