function [ CurrResults ] = ParetoOptimalCosts( ImpactMatrix, ExpandedGraphStructure, Corresp,...
    ExtendedAssetID, StartID, StopID, ...
    ThreatControlMat, ProcControlMat, ChckPntControlMat, ImpactUpperBound, CostFun, TmpCostFun, ProcCost, TmpCostTh)

[ Criteria, D, p, x, ~, nu, K, y, fval, fvalt, ~, IsFeasible, xCorr, xType ] = MinimizeCost(TmpCostTh, ImpactMatrix,...
    ExpandedGraphStructure, Corresp, ExtendedAssetID, StartID, StopID, ...
    ThreatControlMat, ProcControlMat, ChckPntControlMat, ImpactUpperBound, CostFun, TmpCostFun, ProcCost);
    disp(fval);
    CurrResults = [];
if(IsFeasible)
    res = struct('Criteria', Criteria, 'Controls', D, 'ProtectionRef', p, 'ControlsUsed', x,...
       'ToleratedAssets', nu, 'ProcControls', K, 'ProcControlsUsed', y,...
       'xCorr', xCorr, 'xType', xType, 'DeployControlCost', fval, 'ProcControlCost', fvalt);
    CurrResults = {res};
    
    lambda = min([ProcCost, TmpCostFun])/2;
    TmpCostTh = fvalt - lambda;
         
    TmpRes = ParetoOptimalCosts( ImpactMatrix, ExpandedGraphStructure, Corresp,...
    ExtendedAssetID, StartID, StopID, ...
    ThreatControlMat, ProcControlMat, ChckPntControlMat, ImpactUpperBound, CostFun, TmpCostFun, ProcCost, TmpCostTh);
    
    if(~isempty(TmpRes))
        CurrResults = [CurrResults(:); TmpRes(:)];
    end
        
end



end

