package GammaXmlMessages;


import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import scala.Console;

public class IdsAlarmPayload extends MessagePayload
{
	public static final String alarmElementName = "alarm";
	private static final String severityElementName = "severity";
	private static final String severityLevelAttrName = "level";
	private static final String certaintyElementName = "certainty";
	private static final String threatElementName = "threat";
	private static final String contentElementName = "content";
	/*<!ELEMENT alarm (severity,certainty,threat,references?,content?)>
    <!ELEMENT severity EMPTY>
    <!ATTLIST severity level (LOW|MEDIUM|HIGH|CRITICAL) #REQUIRED>
    <!-- Certainty of the occurence of the anomaly. 0% < certainty <=100% -->
    <!ELEMENT certainty (#PCDATA)>
    <!-- Type of threat (e.g. hijack) -->
    <!ELEMENT threat (#PCDATA)>
    <!-- references to other messages (e.g. a set of messages that together caused the alarm to trigger)-->
    <!ELEMENT references (reference+)>
    <!-- Generic content. This field can be used for any additional data. -->
    <!ELEMENT content ANY>*/
	public enum IdsAlarmSeverityLevel  
	{
		LOW("LOW"), MEDIUM("MEDIUM"), HIGH("HIGH"), CRITICAL("CRITICAL");
		
		private final String value;
		IdsAlarmSeverityLevel(String val)
		{
			value = val;
		}
		
		public String getStringValue()
		{
			return value;
		}
		
		public static IdsAlarmSeverityLevel getValue(String str)
		{
			IdsAlarmSeverityLevel res = null;
			for(IdsAlarmSeverityLevel val : IdsAlarmSeverityLevel.values())
			{
				if(val.getStringValue().equals(str))
				{
					res = val;
					break;
				}
			}
			return res;
		}
	};
	
	IdsAlarmSeverityLevel severityLevel;
	
	Double certainty;
	
	String threat;
	
	IdsAlarmContent content;
	
	public IdsAlarmPayload(IdsAlarmSeverityLevel severity, Double certain, String threatName)
	{
		severityLevel = severity;
		certainty = certain;
		threat = threatName;
		content  = null;
	}
	
	public IdsAlarmPayload(IdsAlarmSeverityLevel severity, Double certain, String threatName, String icontent)
	{
		severityLevel = severity;
		certainty = certain;
		threat = threatName;
		System.out.println(icontent);
		content = new StringIdsAlarmContent(icontent);		
	}
	
	
	public IdsAlarmPayload(Element elem) throws Exception
	{
		if(! elem.getTagName().equals(alarmElementName))
		{
			throw new Exception("Incorrect IDS alarm tag name!");
		}
		int severityNum = 0;
		int certaintyNum = 0;
		int threatNum = 0;
		
		int contentNum = 0;
		
		NodeList childs = elem.getChildNodes();
		for(int i = 0; i < childs.getLength(); i++)
		{
			Node currNode = childs.item(i);
			if(currNode.getNodeType() == Node.ELEMENT_NODE)
			{
				Element currElem = (Element) currNode;
				if(currElem.getTagName().equals(severityElementName))
				{
					severityNum++;
					String severStr = currElem.getAttribute(severityElementName);
					severityLevel = IdsAlarmSeverityLevel.getValue(severStr);
				}
				
				if(currElem.getTagName().equals(certaintyElementName))
				{
					certaintyNum++;
					certainty = Double.parseDouble(currElem.getTextContent());
				}
				if(currElem.getTagName().equals(threatElementName))
				{
					threatNum++;
					threat = currElem.getTextContent();
				}
				
				if(currElem.getTagName().equals(contentElementName))
				{
					contentNum++;
					
					content = IdsAlarmContent.getAlarmContent(currElem);
				}
				
			}
		}
		if((severityNum != 1) || (certaintyNum != 1) || (threatNum != 1) || (contentNum > 1))
		{
			throw new Exception("Incorect IDS alarm message structure!");
		}
	}


	public Element serialisePayload(Document doc)
	{
		Element result = null;
		try 
		{
			result = setupRootElement(doc);
			
			Element alarmElem = doc.createElement(alarmElementName);
			
			// severity
			Element severity = doc.createElement(severityElementName);
			Attr sevLevel = doc.createAttribute(severityLevelAttrName);
			sevLevel.setValue(severityLevel.getStringValue());
			severity.setAttributeNode(sevLevel);
			alarmElem.appendChild(severity);
			
			alarmElem = addSimpleElementValue(alarmElem, doc, certaintyElementName, certainty);
			
			alarmElem = addSimpleElementValue(alarmElem, doc, threatElementName, threat);
			
			alarmElem = addSimpleElementValue(alarmElem, doc, contentElementName, content.what());
			
			result.appendChild(alarmElem);
		
		}
		catch (Exception e)
		{
			result = null;
		}
		
		return result;
	}
	
	public String GetContent()
	{
		return content.what();
	}
	
	public String GetContentType()
	{
		return content.getType();
	}
	 
}
