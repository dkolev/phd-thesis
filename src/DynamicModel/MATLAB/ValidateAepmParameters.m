function [ numPredictedScenarios, numPredictedAssets, isCorrect  ] = ValidateAepmParameters( scenarioNum )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
alpha = 1;
[ numPredictedScenarios, numPredictedAssets, isCorrect ] = ValidateAepmModel( scenarioNum, 1 );
iterNum = 20; 
mul = 0.7;
for i = 2:iterNum
    alpha = alpha * mul;
    [ numPredictedScenarios(i, :), numPredictedAssets(i ,:), isCorrect(i) ] = ValidateAepmModel( scenarioNum, alpha );
end

end

