function [ Paths ] = CreatePaths( GraphMatrix, Source, Sink )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
N = size(GraphMatrix, 1);
Template = zeros(N);
Graph = biograph(GraphMatrix');
Paths = cell(length(Source), length(Sink));
for i=1:length(Source)
    for j=1:length(Sink)
        [n_u, CurrPath] = shortestpath(Graph, Source(i), Sink(j));
        CurrPath = [CurrPath(1:(end-1)); CurrPath(2:end)]';
        Ids = (CurrPath(:, 1)-1)*N + CurrPath(:, 2);
        CurrPath = Template;
        CurrPath(Ids) = 1;
        Paths{i, j} = CurrPath;
    end
end

end

