function [ PossibleSourceDataRate, PossibleControlDataRate ] = CalculateTotalCapacity( Graph, DataPaths, ControlPaths, Capacity, SourceDataRate, ControlDataRate )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

for i=1:length(SourceDataRate)
    DataPaths(:,:,i) = DataPaths(:,:,i)*SourceDataRate(i);
end

for i=1:length(SourceDataRate)
    ControlPaths(:,:,i) = ControlPaths(:,:,i)*ControlDataRate(i);
end

DataRatePerLink = sum(ControlPaths, 3) + sum(DataPaths, 3);

Overload = (DataRatePerLink - Capacity); % where there is an exceedance of the capacity
Overload = Overload.*(Overload > 0);

DataPerCent = Graph;
DataPerCent(Overload > 0) = Capacity(Overload>0) ./ DataRatePerLink(Overload > 0);
DataPerCent((Overload == 0)&logical(Graph)) = 1;

%% CHANGE IT, BASTARD!! IT DEPENDS ON FRAME SIZE!!!

SourceDataLoss = ones(length(SourceDataRate),1); %not loss, but transfer percent

for i=1:length(SourceDataRate)
    SourceDataLoss(i) = min(DataPerCent(logical(DataPaths(:, :, i)))); %cool, yeah
end

ControlDataLoss = ones(length(SourceDataRate),1);
for i=1:length(SourceDataRate)
    ControlDataLoss(i) = min(DataPerCent(logical(ControlPaths(:, :, i))));
end

PossibleSourceDataRate = SourceDataRate.*(SourceDataLoss');
PossibleControlDataRate = ControlDataRate.*(ControlDataLoss');

end

