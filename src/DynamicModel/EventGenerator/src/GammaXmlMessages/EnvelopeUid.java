package GammaXmlMessages;

import java.lang.*;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class EnvelopeUid
{
	public static final String uidSystemIDElemName = "system_id";
	public static final String uidSequenceNumberName = "sequence_number";
	
	private String uid_system_id;
	private String uid_sequence_number;	
	
	public EnvelopeUid(String systemId, String seqNumber)
	{
		uid_system_id = systemId;
		uid_sequence_number = seqNumber;
	}
	
	public EnvelopeUid(Element elem) throws Exception
	{
		int systemIDNum = 0;
		int sequenceIdNum = 0;
		
		NodeList childs = elem.getChildNodes();
		for(int i = 0; i < childs.getLength(); i++)
		{
			Node currNode = childs.item(i);
			if(currNode.getNodeType() == Node.ELEMENT_NODE)
			{
				Element currElem = (Element) currNode;
				if(currElem.getTagName().equals(uidSystemIDElemName))
				{
					systemIDNum++;
					uid_system_id = currElem.getTextContent();
				}
				if(currElem.getTagName().equals(uidSequenceNumberName))
				{
					sequenceIdNum++;
					uid_sequence_number = currElem.getTextContent();
				}
			}
		}
		if((systemIDNum != 1) || (sequenceIdNum != 1))
		{
			throw new Exception("UID is not well-formed!");
		}
	}
	
	public String GetSystemId()
	{
		return uid_system_id;
	}
	
	public String GetSequenceNumber()
	{
		return uid_sequence_number;
	}
}