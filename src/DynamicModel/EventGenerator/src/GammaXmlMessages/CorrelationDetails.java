package GammaXmlMessages;

public class CorrelationDetails 
{
	public static final String corrDetailsElementName = "correlation_details";
	public final String system_name;
	public final String atc_freq;
	
	public CorrelationDetails(String _systemName, String _atcFreq)
	{
		system_name = _systemName;
		atc_freq = _atcFreq;
	}
}
