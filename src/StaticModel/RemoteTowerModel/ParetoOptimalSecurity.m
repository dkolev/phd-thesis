function [ CurrResults ] = ParetoOptimalSecurity( CostTh, TmpCostTh, ImpactMatrix,...
    ExpandedGraphStructure, Corresp, ExtendedAssetID, StartID, StopID, ...
    ThreatControlMat, ProcControlMat, ChckPntControlMat, ImpactUpperBound, CostFun, TmpCostFun, ProcCost, lambda, layer)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
CurrResults = [];
[ Criteria, D, p, x, z, nu, K, y, fval, fvalt, OptimalAttacks, IsFeasible, xCorr, xType ] = MaximizeCriteria(CostTh, TmpCostTh, ImpactMatrix,...
     ExpandedGraphStructure, Corresp, ExtendedAssetID, StartID, StopID, ...
     ThreatControlMat, ProcControlMat, ChckPntControlMat, ImpactUpperBound, CostFun, TmpCostFun, ProcCost);
if(IsFeasible)
    if(isempty(lambda))
        lambda = zeros(1, size(ImpactMatrix, 3));
        for i=1:size(ImpactMatrix, 3)
            CurrCrit = ImpactMatrix(:, :, i);
            CurrCrit = CurrCrit(:);
            CurrCrit = abs(diff(sort(CurrCrit)));
            lambda(i) = min(CurrCrit(CurrCrit > 0));
        end
    end
    res = struct('Criteria', Criteria, 'Controls', D, 'ProtectionRef', p, 'ControlsUsed', x,...
       'ToleratedAssets', nu, 'ProcControls', K, 'ProcControlsUsed', y,...
       'xCorr', xCorr, 'xType', xType, 'DeployControlCost', fval, 'ProcControlCost', fvalt, 'OptimalAttacks', OptimalAttacks);
    CurrResults = {res};
    for i=1:length(ImpactUpperBound)
        disp(['layer ' num2str(layer) ' branch ' num2str(i)]); 
        disp('%%%');
        CurrImpactUpperBound = ImpactUpperBound;
        CurrImpactUpperBound(i) = Criteria(i+1) - lambda(i+1)/2;
        if(CurrImpactUpperBound(i) < 0)
            continue;
        end
        TmpRes = ParetoOptimalSecurity( CostTh, TmpCostTh, ImpactMatrix,...
            ExpandedGraphStructure, Corresp, ExtendedAssetID, StartID, StopID, ...
            ThreatControlMat, ProcControlMat, ChckPntControlMat, CurrImpactUpperBound, CostFun, TmpCostFun, ProcCost, lambda, layer+1);
        
        CurrResults = [CurrResults(:), TmpRes(:)];
        
    end
end


end

