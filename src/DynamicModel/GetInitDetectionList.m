function [ initDet ] = GetInitDetectionList( allDetections, corresp, attackCodes )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
initDetections = zeros(length(allDetections), 2);
for i = 1:size(initDetections, 1)
   
   [initDetections(i, 1), initDetections(i, 2)] = getInitNode( allDetections(i), corresp);
    
end

dt = unique(initDetections(:, 1));
initDet = cell(length(dt), 2);

for i = 1:length(dt)
    initDet{i, 1} = dt(i);
    currAttacks = unique(initDetections( initDetections(:, 1) == dt(i), 2));
    currCodes = attackCodes(currAttacks);
    initDet{i, 2} = currCodes;
end


end

function [initNode, attackType] = getInitNode(expNode, corresp)
initNode = 0;
for i = 1:size(corresp, 1)
   
    if(ismember(expNode, corresp{i, 2}(:)))
        [attackType, c] = find( corresp{i, 2} == expNode );
        initNode = corresp{i, 1};
        break;
    end
    
end

end

