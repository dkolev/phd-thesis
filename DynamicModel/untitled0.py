# -*- coding: utf-8 -*-
"""
Created on Sun May 12 16:12:36 2019

@author: admin
"""

#datafiles_to_read = ['Tuesday-WorkingHours.pcap_ISCX.csv', 
#                     'Thursday-WorkingHours-Morning-WebAttacks.pcap_ISCX.csv',
#                     'Thursday-WorkingHours-Afternoon-Infilteration.pcap_ISCX.csv',
#                     'Friday-WorkingHours-Afternoon-DDos.pcap_ISCX.csv', 
#                     'Monday-WorkingHours.pcap_ISCX.csv', 
#                     'Friday-WorkingHours-Morning.pcap_ISCX.csv',
#                     'Friday-WorkingHours-Afternoon-PortScan.pcap_ISCX.csv',
#                     'Wednesday-workingHours.pcap_ISCX.csv']
#                     
datafiles_to_read = ['Thursday-WorkingHours-Morning-WebAttacks.pcap_ISCX.csv',
                     'Thursday-WorkingHours-Afternoon-Infilteration.pcap_ISCX.csv']
import cv2
import numpy as np
import pandas as pd
import scipy
from datetime import datetime
import time

#import annalyse_infiltration_attack

def value_to_dict(vals, valdict_in = None):
    valdict = {}
    if valdict_in is not None:
        valdict = valdict_in

    new_vals = np.zeros(len(vals))
    i = 0
    k = 0
    if valdict_in is not None:
        k = max(valdict_in.values()) + 1
    for v in vals:
        if v in valdict.keys():
            new_vals[i] = valdict[v]
        else:
            if not isinstance(v, str) and np.isnan(v):
                new_vals[i] = -1
            else:
                valdict[v] = k
                k += 1
                new_vals[i] = k-1
        i += 1
    return new_vals, valdict

def convert_time_to_ms(timestamps):
    time_vals = np.zeros(len(timestamps))
    i = 0
    for v in timestamps:
        try:
            parsed_time = datetime.strptime(v, "%d/%m/%Y %H:%M")
            time_vals[i] = time.mktime(parsed_time.timetuple())
        except:
            if not isinstance(v, str) and not np.isnan(v):
                parsed_time = datetime.strptime(v, "%d/%m/%Y %H:%M:%S")
                time_vals[i] = time.mktime(parsed_time.timetuple())
            else:
                if i > 0:
                    time_vals[i] = time_vals[i-1]
                else:
                    time_vals[i] = 0
        i += 1
    return time_vals
dtm = None
dt_flows = None
for datafile_to_read in datafiles_to_read:
    #dt = pd.read_csv(datafile_to_read, encoding="ISO-8859-1")
    dt_flows_tmp = pd.read_csv('TrafficLabelling/' + datafile_to_read, encoding="ANSI")
    if dtm is None:
        dtm = dt_flows_tmp.as_matrix()[:, 7:]
        dt_flows = dt_flows_tmp
    else:
        tmp = dt_flows_tmp.as_matrix()[:, 7:]
        dtm = np.concatenate((dtm, tmp))
        dt_flows = pd.concat([dt_flows, dt_flows_tmp])

labels = dtm[:, -1]
data = dtm[:, :-1]

labs, labdict = value_to_dict(labels)

#dtm[:, 0], portdict = value_to_dict(dtm[:, 0]) 
source_ip_labels, source_ip_dicts = value_to_dict(dt_flows[' Source IP'])
destination_ip_labels, ip_dicts = value_to_dict(dt_flows[' Destination IP'], source_ip_dicts)
data = data[labs >= 0]
time_vals = convert_time_to_ms(dt_flows[' Timestamp'])
time_vals1 = time_vals[labs >= 0]
time_vals = time_vals1
source_ip_labels = source_ip_labels[labs >= 0]
destination_ip_labels = destination_ip_labels[labs >= 0]
labs = labs[labs >= 0]


#cl, fp_rate = annalyse_infiltration_attack.analyse_infiltration_attack(dtm, [], time_vals, labs)
