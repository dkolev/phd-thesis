function [ res ] = GetExpGraphWithControls( ExpGraphStructure, D, Corresp, K, ExtendedAssetID )
%UNTITLED8 Summary of this function goes here
%   Detailed explanation goes here
AttackTypeNum = size(Corresp{1, 2}, 1);
CheckPointNum = size(Corresp, 1);

N = size(ExpGraphStructure, 1);
GraphCut = zeros(N);

ProcProtectedControls = ExtendedAssetID(:);
ProcProtectedControls = ProcProtectedControls(logical(K(:)));
PN = length(ProcProtectedControls);
GraphCut(:, ProcProtectedControls) = ExpGraphStructure(:, ProcProtectedControls); 
%cut the attacks of given type to given asset

IDs = find(D==1);
for i=1:length(IDs)
    AttackTypeID = ceil(IDs(i) / CheckPointNum);
    CheckPointID = IDs(i) - (AttackTypeID-1)*CheckPointNum;
    CuttedEdge = Corresp{CheckPointID, 2}(AttackTypeID, :);
    GraphCut(CuttedEdge(2), CuttedEdge(1)) = 1;
end

res = ExpGraphStructure - GraphCut;
end

