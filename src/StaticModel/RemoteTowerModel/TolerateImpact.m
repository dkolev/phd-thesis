function [ ExpandedGraphStructure_new, Aineq_new, bineq_new ] = TolerateImpact( ExpandedGraphStructure, ExpandedAssetID, Aineq, bineq,...
    AttacksToTolerate, AssetsToTolerate, StopID, CheckPointNum )
% IMPOTANT!! Here AssetsToTolerate is an a LINEAR INTERNAL ID, not Graph
% ID!!
AttackNum = size(ExpandedAssetID, 2);
% We have to remove the link from the supporting asset AssetToTolerate
% to STOP of attack type AttackToTolerate 
% type
%% Find the ID of the link and remove it from the graph
ExpandedGraphStructure_new = ExpandedGraphStructure;
NumberOfEdges = sum(ExpandedGraphStructure(:));
SpecStructure = cumsum(ExpandedGraphStructure(:));
N = size(ExpandedGraphStructure, 1); %indicates the number of every "1" in the matrix
rowIDs = [];
for i=1:length(AttacksToTolerate)
    ID = ExpandedAssetID(AssetsToTolerate(i), AttacksToTolerate(i));
    LinearID = (ID-1)*N + StopID;
    rowIDs = [rowIDs, SpecStructure(LinearID)];
    ExpandedGraphStructure_new(StopID, ID) = 0;
end

%% Find the number of the row and remove it
Ddim = CheckPointNum*AttackNum;
rowIDs = rowIDs + Ddim; % structure of A_ineq. First Ddim rows are about coverage,
% second - about node label consistency, third - about affected assets

% IN CASE OF MODEL CHANGE APPEND ALL NEW ROWS TO THE END!!! 

RowNum = size(Aineq, 1);
GoodIds = setdiff(1:RowNum, rowIDs);
Aineq_new = Aineq(GoodIds, :);
bineq_new = bineq(GoodIds);


end

