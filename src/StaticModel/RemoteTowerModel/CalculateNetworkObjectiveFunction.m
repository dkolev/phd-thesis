function [ S, C] = CalculateNetworkObjectiveFunction( GraphStructure, SupportingAssetStructure, Source, Sink, SourceFrameSize, ControlFrameSize, maxFrameSize,...
    SourceDataRate, ControlDataRate, SourceLatLimitations, SourceCapLimitations, ControlLatLimitations, ControlCapLimitations,...
    SourceTRLimitations, ControlTRLimitations, SafetyDataAssets, CapacityDataAssets)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
%first create the data
SAnum = size(SupportingAssetStructure, 1);
[ Capacities, Latencies, TrueRate ] = FormCapacityLatencyVectors( SupportingAssetStructure );
%% Path calculation
DataPaths = CreatePaths(GraphStructure, Source, Sink); % paths from sources to the RMT
DataPaths = reshape(DataPaths, [1, 1, length(DataPaths)]);
DataPaths = cell2mat(DataPaths);

ControlPaths = CreatePaths(GraphStructure, Sink, Source); %path from RMT to sources
ControlPaths = reshape(ControlPaths, [1, 1, length(ControlPaths)]);
ControlPaths = cell2mat(ControlPaths);


%% Computation of the main system parameters
[ LatencySource, LatencyControl ] =...
    CalculateTotalLatency(DataPaths, ControlPaths, Latencies, Capacities, Source, SourceFrameSize, ControlFrameSize, maxFrameSize );
[ PossibleSourceDataRate, PossibleControlDataRate ] =...
    CalculateTotalCapacity(GraphStructure, DataPaths, ControlPaths, Capacities, SourceDataRate, ControlDataRate );
[ SourceTrueRate, ControlTrueRate ] = CalculateErrorRate( DataPaths, ControlPaths, TrueRate );


%% Exceedance Calculation
SourceLatencyExceeded = LatencySource > SourceLatLimitations;
SourceCapacityExceeded = PossibleSourceDataRate' < SourceCapLimitations';
SourceErrorExceeded = SourceTrueRate < SourceTRLimitations;

ControlLatencyExceeded = LatencyControl > ControlLatLimitations;
ControlCapacityExceed = PossibleControlDataRate' < ControlCapLimitations';
ControlErrorExceeded = ControlTrueRate < ControlTRLimitations;

%% Caculation of the objective functions. To be changed!!
ProblemMatrix = [SourceLatencyExceeded, SourceCapacityExceeded,...
    SourceErrorExceeded, ControlLatencyExceeded, ControlCapacityExceed,...
    ControlErrorExceeded];

ProblemID = double((sum(ProblemMatrix, 2) > 0));

SafetyDataAssetsL = ismember(Source, SafetyDataAssets);
CapacityDataAssetsL = ismember(Source, CapacityDataAssets);

S = ((SafetyDataAssetsL*ProblemID) > 0);

C = ((CapacityDataAssetsL*ProblemID) > 0);

end

