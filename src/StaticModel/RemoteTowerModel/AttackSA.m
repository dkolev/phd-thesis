function [ NewSupportingAssetStructure ] = AttackSA( AttackType, AssetNum, SupportingAssetStructure )
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here
switch(AttackType)
    
    case 1 
        % physiscal attack
        SupportingAssetStructure{AssetNum, 4} = 0;
        
    case 2
        % compromise of information
        SupportingAssetStructure{AssetNum, 1} = inf;
        tmp = SupportingAssetStructure{AssetNum, 2};
        SupportingAssetStructure{AssetNum, 2} = zeros(1, length(tmp));
        SupportingAssetStructure{AssetNum, 3} = 0;
        SupportingAssetStructure{AssetNum, 5} = 0;
        
    case 3 
        %interference
        tmp = SupportingAssetStructure{AssetNum, 2};
        SupportingAssetStructure{AssetNum, 2} = zeros(1, length(tmp));
        SupportingAssetStructure{AssetNum, 5} = SupportingAssetStructure{AssetNum, 5}/2;
        
end

NewSupportingAssetStructure = SupportingAssetStructure;

end

