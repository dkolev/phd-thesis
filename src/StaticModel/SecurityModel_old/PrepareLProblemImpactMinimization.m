function [ Aineq, bineq, Aeq, beq, f CostRow] = PrepareLProblemImpactMinimization(...
    ExpandedGraphStructure, StartID, StopID,...
    Corresp, ThreatControlMat, ChckPntControlMat, CostFun,...
    CostTh, ImpactMatrix, ExpandedAssetID)
% Prepares the input for BLP solver
% our variable [D, p, x]

pdim = size(ExpandedGraphStructure, 1);
CheckPointNum = size(ChckPntControlMat, 1);
N = size(ExpandedGraphStructure, 1);
ControlNum = size(ThreatControlMat, 2);
AttackTypeNum = size(ThreatControlMat, 1);
Ddim = CheckPointNum*AttackTypeNum;
AssetNum = length(ExpandedAssetID(:));

%% Prepare Aineq, bineq and f
% Prepare eq. D <= Ax and cost for x
Aineq = zeros(Ddim, 0);
f = [];
%stupid indexing... D = [N1.1, N1.2, ...N1.CheckPointNum N2.1, ...
%NAttackTypeNum.CheckPointNum]
for i=1:ControlNum %for all controls
    for j=1:size(ChckPntControlMat, 1) % for all check points
        ControlMatr = zeros(CheckPointNum, AttackTypeNum);
        if(ChckPntControlMat(j, i))
           ControlMatr(j, :) = ThreatControlMat(:, i)'; %stupid method... must be vectorized!
           tmpN = ControlMatr(:);
           Aineq = [Aineq, tmpN];
           f = [f, CostFun(i)];% cost depends only on the type of control!
        end
    end 
end
xDim = length(f);
% no dependance on p => zeros
Aineq = [zeros(Ddim, N), Aineq];
% minus D => -eye
Aineq = [-eye(Ddim), Aineq];
%prepare -Dij - pi + pj <=0

%prepare matrix for D
TmpIneq = zeros(N*N, Ddim);
for i = 1:AttackTypeNum
    for j = 1:CheckPointNum
        IdMatrix = Corresp{j, 2};
        Id = (IdMatrix(i, 1)-1)*N + IdMatrix(i, 2);
        D_id = CheckPointNum*(i-1) + j; %which component of d corresponds to the link between IdMatrix(i, :)
        TmpIneq(Id, D_id) = -1;
    end
end

%prepare matrix for p
Pineq1 = repmat(eye(N), N, 1);
Pineq2 = reshape(Pineq1, N, N*N)';

Pineq = -Pineq2 + Pineq1;

CutIneq = [TmpIneq, Pineq, zeros(N*N, xDim)];
CutIneq = CutIneq(logical(ExpandedGraphStructure(:)), :);

Aineq = [-Aineq; CutIneq];
bineq = zeros(size(Aineq, 1), 1);

%% Prepare Aeq and CostRow
% p(START) = 0; p(STOP) = 1;
p1 = zeros(1, size(Aineq, 2));
p2 = p1;
p1(Ddim + StartID) = 1;
p2(Ddim + StopID) = 1;
Aeq = [p1; p2];
beq = [0; 1];
f = [zeros(1, Ddim + pdim), f];
CostRow = f;
%% Additional problem expanding. Adding z

% add cost threshold
Aineq = [Aineq; f];
bineq = [bineq; CostTh];

%create p and z constraints: U*p + z <=0
tmp = eye(N);

tmp = tmp(ExpandedAssetID(:), :);
tmp = diag(-ImpactMatrix(:))*tmp;
tmp = [zeros(AssetNum, Ddim), tmp, zeros(AssetNum, xDim)];
tmp = [tmp, -ones(AssetNum, 1)];

Aineq = [Aineq, zeros(size(Aineq, 1), 1)];
Aineq = [Aineq; tmp];
bineq = [bineq; -ImpactMatrix(:)];

% update the size of Aeq
Aeq = [Aeq, zeros(size(Aeq, 1), 1)];

f = [zeros(1, Ddim + pdim + xDim), 1];

end

