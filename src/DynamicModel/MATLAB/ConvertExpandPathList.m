function [ initPathList, repeatInitPathList ] = ConvertExpandPathList( pathList, corresp, ExpAssetID, ExpEntranceID,...
    AssetID, EntranceID, StartNode, EndNode, ExpNodesNum  )
% converst pathList from expanded graph nodes to 1) path in the initial
% graph 2) path in the initial graph, but with repeating nodes (each node
% is repeated twice for corresponding edge in the expanded graph
% TODO: initPathList incomplete
corMatrix = zeros(0, 3);
for i = 1:size(corresp, 1)
   
    IDX = corresp{i, 1};
    matr = corresp{i, 2};
    corMatrix = [corMatrix; [matr, IDX(ones(size(matr, 1), 1))]];
    
end
aN = length(AssetID);
eN = length(EntranceID);

for i = 1:size(ExpEntranceID, 2)
   
    corMatrix = [corMatrix; [StartNode(ones(eN, 1)), ExpEntranceID(:, i), EntranceID]];
    
end

for i = 1:size(ExpAssetID, 2)
   
    corMatrix = [corMatrix; [ExpAssetID(:, i), EndNode(ones(aN, 1)), AssetID]];
    
end
%in order to compare pairs of indeces
pairIndexCorr = [corMatrix(:, 1) + 1i * corMatrix(:, 2), corMatrix(:, 3)];
indepIndexCorr = [corMatrix(:, 1), corMatrix(:, 3); corMatrix(:, 2), corMatrix(:, 3)];

% hack!
tmpCorrMatrix = zeros(ExpNodesNum, 1);
tmpCorrMatrix(indepIndexCorr(:, 1)) = indepIndexCorr(:, 2);
tmpCorrMatrix([StartNode, EndNode]) = 0;

initPathList = zeros(size(pathList));
repeatInitPathList = initPathList;

%IDX = find(pathList > 0);

repeatInitPathList(pathList(:) > 0) = tmpCorrMatrix( pathList( pathList(:) > 0));


end

