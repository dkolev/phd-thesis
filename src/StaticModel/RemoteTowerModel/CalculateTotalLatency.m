function [ LatencySource, LatencyControl ] = CalculateTotalLatency( DataPaths, ControlPaths, Latencies, Capacities, Source, SourceFrameSize,...
    ControlFrameSize, maxFrameSize )
%UNTITLED8 Summary of this function goes here
%   Detailed explanation goes here

LatencySource = zeros(length(Source), 1);
LatencyControl = zeros(length(Source), 1);

for i=1:length(Source)
    LatencySource(i) = CalculateLatency(DataPaths(:, :, i),Latencies,  Capacities, SourceFrameSize(i), maxFrameSize);
    LatencyControl(i) = CalculateLatency(ControlPaths(:, :, i),Latencies,  Capacities, ControlFrameSize(i), maxFrameSize);
end





end

