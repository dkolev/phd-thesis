package GammaXmlMessages;

import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class ThreatDescription extends XmlElementBuilder
{
	private static final String threatElementName = "threat";
	
	private static final String threatNameElementName = "threat_name";
	private static final String threatDescriptionElementName = "threat_description";
	private static final String threatProbabilityElementName = "probability";
	private static final String threatRankElementName = "rank";
	private static final String threatImpactElementName = "impact";
	
	String threat_name;
	
	String threat_description;
	
	List<GraphNode> threat_description_internal;
	
	Double probability;
	
	Integer rank;
	
	Double impact;
	
	public ThreatDescription(String ThreatName, String ThreatDescription, Double Prob, Integer Rank, Double Impact)
	{
		
		// TODO: add threat_description_internal
		threat_name = ThreatName;
		threat_description = ThreatDescription;
		probability = Prob;
		rank = Rank;
		impact = Impact;
	}
	
	public Element serialiseThreat() throws ParserConfigurationException
	{
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		
		Document threat = docBuilder.newDocument();	
		
		Element threatElem = threat.createElement(threatElementName);
		
		threatElem = addSimpleElementValue(threatElem, threat, threatNameElementName, threat_name);
		
		threatElem = addSimpleElementValue(threatElem, threat, threatDescriptionElementName, threat_description);
		
		threatElem = addSimpleElementValue(threatElem, threat, threatProbabilityElementName, probability);
		
		threatElem = addSimpleElementValue(threatElem, threat, threatRankElementName, rank);
		
		threatElem = addSimpleElementValue(threatElem, threat, threatImpactElementName, impact);
		
		return threatElem;
	}
	
}
