package GammaXmlMessages;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public abstract class XmlElementBuilder 
{
	protected Element addSimpleElementValue(Element parent, Document doc, String elementName, Object elementValue)
	{
		if(elementValue != null)
		{
			Element thElem = doc.createElement(elementName);
			String val = elementValue.toString();
			thElem.appendChild(doc.createTextNode(val));
			parent.appendChild(thElem);			
		}
		return parent;
	}
}
