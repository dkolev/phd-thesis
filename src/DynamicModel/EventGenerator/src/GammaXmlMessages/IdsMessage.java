package GammaXmlMessages;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class IdsMessage
{
	private static final String messageElementName = "message";
	
	protected MessagePayload data;
	
	protected HeaderParams header;
	
	public IdsMessage(HeaderParams envelope, MessagePayload payload)
	{
		try
		{
			
			data = payload;
			
			header = envelope;
		}
		catch (Exception e)
		{
			
		}
	}
	
	
	public IdsMessage(Element elem) throws Exception
	{
		if(! elem.getTagName().equals(messageElementName))
		{
			throw new Exception("wrong message format!");
		}
		int dataCounter = 0;
		int headerCounter = 0;
		NodeList childs = elem.getChildNodes();
		for(int i = 0; i < childs.getLength(); i++)
		{
			Node currNode = childs.item(i);
			if(currNode.getNodeType() == Node.ELEMENT_NODE)
			{
				Element currElement = (Element) currNode;
				if(currElement.getTagName().equals(HeaderParams.headerElementName))
				{
					headerCounter++;
					header = new HeaderParams(currElement);
				}
				if(currElement.getTagName().equals(MessagePayload.payloadElementName))
				{
					dataCounter++;
					data = MessagePayload.createInstanceFromElement(currElement);
				}
			}
		}
		
		if((headerCounter != 1) || (dataCounter != 1))
		{
			throw new Exception("wrong message format (number of headers or payloads is wrong)!");
		}
	}
	
	
	public Element serialiseMessage(Document doc)
	{
		
		Element msgElem = doc.createElement(messageElementName);
		
		msgElem.appendChild(header.serialise(doc));
		
		msgElem.appendChild(data.serialisePayload(doc));
		
		return msgElem;
	}
	
	
	public String getContent()
	{
		String result = null;
		if( data instanceof IdsAlarmPayload )
		{
			result = ((IdsAlarmPayload) data).GetContent();
		}
		return result;
	}
	
	public String getuisSystem()
	{
		return header.uid.GetSystemId();
	}
	
	public String getContentType()
	{
		String result = null;
		if( data instanceof IdsAlarmPayload )
		{
			result = ((IdsAlarmPayload) data).GetContentType();
		}
		return result;
	}
	
	public String getSubject()
	{
		String result = header.subject;
		return result;
	}
	
	public Double getCoordinatesLatitude()
	{
		return header.lat;
	}
	
	public Double getCoordinatesLongtitude()
	{
		return header.longt;
	}
	
}
