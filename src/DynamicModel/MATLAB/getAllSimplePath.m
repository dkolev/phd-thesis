function [ paths, attackTypes ] = getAllSimplePath( AdjMatr, stopNodes, startNode, maxPathLen, assetID )

paths = zeros(0, maxPathLen);
attackTypes = zeros(0, 1);
for j = 1:length(startNode)
    
    IDX = find(AdjMatr(startNode(j), :) > 0);
    
    for i = 1:length(IDX)
        disp(i);
        currPath = zeros(1, maxPathLen);
        currPath(1) = startNode(j);
        currPath(2) = IDX(i);
        [paths, attackTypes] = wideSearch( AdjMatr, assetID, currPath, 2, paths, attackTypes, stopNodes );
    end

end

end

function [paths, attackTypes] = wideSearch( AdjMatr, assetID, currPath, currPathLength, paths, attackTypes, stopNodes )

lastNode = currPath(currPathLength);
if(ismember(lastNode, stopNodes))
   paths = [paths; currPath];
   [~, currType] = find(assetID == currPath(currPathLength-1));
   attackTypes = [attackTypes; currType];
else
   IDX = find( AdjMatr(lastNode, :) > 0 );
   for i = 1:length(IDX)
      newPath = currPath;
      if(~ismember(IDX(i), newPath(1:currPathLength)))
          newPath(currPathLength + 1) = IDX(i);
          [paths, attackTypes] = wideSearch( AdjMatr, assetID, newPath, currPathLength + 1, paths, attackTypes, stopNodes );
      end
   end
end

end

