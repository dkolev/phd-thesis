package GammaXmlMessages;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DlrIdsAlarmContent extends IdsAlarmContent
{
	private CorrelationDetails _cd;
	private ConformanceMonitoring _cm;
	private StressDetection _sd;
	private SpeakerVerification sv;
	
	private String _content;
	
	public DlrIdsAlarmContent(Element elem) throws Exception
	{
		contentType = "sacom_alarm";
		int corrNum = 0; int confMonitorNum = 0; int stressNum = 0;int speakerVerNum = 0;
		NodeList childs = elem.getChildNodes();
		for(int i = 0; i < 	childs.getLength(); i++)
		{
			Node n = childs.item(i);
			if(n.getNodeType() != Node.ELEMENT_NODE)
			{
				continue;
			}
			Element currElement = (Element) n;
			if(currElement.getTagName().equals(CorrelationDetails.corrDetailsElementName))
			{
				corrNum++;
			}
			if(currElement.getTagName().equals(ConformanceMonitoring.confMonitoringElementName))
			{
				confMonitorNum++;
			}
			if(currElement.getTagName().equals(StressDetection.stressDetectionElementName))
			{
				stressNum++;
			}
			if(currElement.getTagName().equals(SpeakerVerification.speakerVerificationDetails))
			{
				speakerVerNum++;
			}
		}
		if((corrNum == 1) || (confMonitorNum == 1) || (stressNum == 1) || (speakerVerNum == 1))
		{
			// TODO: parse it properly
			_content = elem.getTextContent();
		}
		else
		{
			throw new Exception("Incorrect DLR alarm content");
		}
	}

	@Override
	public Element serialiseContent(Document doc) {
		// TODO Do not ned this function. Throw exception if called
		return null;
	}

	@Override
	public String what() {
		// TODO Auto-generated method stub
		return _content;
	}

	
}
