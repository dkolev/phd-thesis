function [ Criteria, D, p, y, fval, OptimalAttacks, IsFeasible ] = MaximizeCriteria_BAD( CostTh, ImpactMatrix, ExpandedGraphStructure, Corresp, ExtendedAssetID, StartID, StopID, ...
    ThreatControlMat, ChckPntControlMat, ImpactUpperBound, CostFun)
%UNTITLED12 Summary of this function goes here
% Here the minimization of criteria 1 is being applied.
% ImpactUpperBound - constraints only for the crit from #2 to end 
AttackTypeNum = size(ThreatControlMat, 1);
CheckPointNum = size(ChckPntControlMat, 1);
ToMinimize = ImpactMatrix(:, :, 1);
[ Aineq, bineq, Aeq, beq, f ] = PrepareLProblem(...
    ExpandedGraphStructure, StartID, StopID,...
    Corresp, ThreatControlMat, ChckPntControlMat, CostFun); % creating an LP problem

Ddim = CheckPointNum*AttackTypeNum;
pdim = size(ExpandedGraphStructure, 1);
ydim = length(f)-Ddim-pdim;

[Aeq, beq, ProtectionRequired] = ConsiderConstraints(ImpactMatrix(:, :, 2:end), ImpactUpperBound, ExtendedAssetID, Aeq, beq,...
    Ddim, pdim, ydim);

IsFeasible = true;

[ D, p, y, fval ] = SolveCutLP( f, Aineq, bineq, Aeq, beq,...
    CheckPointNum, AttackTypeNum, pdim);

Values = ToMinimize(~ProtectionRequired); % which of the risks we can tolerate
Values = sort(Values(:), 'ascend'); % sort impact in ascending manner
i = 1;
while((fval > CostTh) && (i <= length(Values))) % tolerating the risks -> remove edges from SA to STOP in increasing manner.
    [AssetsToTolerate, AttacksToTolerate] = find(ToMinimize <= Values(i)); % find attacks with "low" impact
    
    [ ExpandedGraphStructure_new, Aineq_new, bineq_new ] = TolerateImpact( ExpandedGraphStructure, ExtendedAssetID, Aineq, bineq,...
        AttacksToTolerate, AssetsToTolerate, StopID, CheckPointNum ); % tolerate them
    
    [ D, p, y, fval ] = SolveCutLP( f, Aineq_new, bineq_new, Aeq, beq,...
        CheckPointNum, AttackTypeNum, pdim); % solve the problem
    
    i = i+1;
end

if( (i >= length(Values)) && (fval > CostTh) ) % infeasible constraints.
    D = [];
    y = [];
    p = [];
    fval = [];
    IsFeasible = false;
    Criteria = [];
    OptimalAttacks = [];
else
    [Criteria, OptimalAttacks] = GetMultiObjectiveCriteriaValues( ExpandedGraphStructure, ImpactMatrix, D, Corresp, ExtendedAssetID, StartID, StopID );
end

end

