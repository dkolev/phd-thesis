function [ airportImageContext ] = UpdateAirportImage( airportImageContext, currAdversaryPosition )

pix2nodes = airportImageContext.pix2node;
if(ismember(currAdversaryPosition, pix2nodes(:, 1)))
   
    %prevLineImage = airportImageContext.WithLinesImage;
    prevNode = airportImageContext.PrevAdversaryPosition;
    if(~isempty(prevNode))
        if(prevNode ~= currAdversaryPosition)
            lineImage = UpdatePathLines( airportImageContext, currAdversaryPosition );
            airportImageContext.WithLinesImage = lineImage;
        end
    end
    
    adversaryImage = airportImageContext.WithLinesImage;
    idxNow = currAdversaryPosition == pix2nodes(:, 1);
    nodeCoord = pix2nodes(idxNow, 2:3);
    adversaryImage = insertShape(adversaryImage, 'FilledCircle', [nodeCoord, 5], 'Color', 'red');
    airportImageContext.CurrImage = adversaryImage;
    airportImageContext.PrevAdversaryPosition = currAdversaryPosition;
end


end

function lineImage = UpdatePathLines( airportImageContext, currNode )

    prevNode = airportImageContext.PrevAdversaryPosition;
    pix2node = airportImageContext.pix2node;
    
    lineImage = airportImageContext.WithLinesImage;
    
    idxPrev = (pix2node(:, 1) == prevNode);
    idxNow =  (pix2node(:, 1) == currNode);
    
    prevCoordinates = pix2node(idxPrev, 2:3);
    currCoordinates = pix2node(idxNow, 2:3);
    
    lineImage = insertShape(lineImage, 'Line', [ pix2node(idxPrev, 2:3), pix2node(idxNow, 2:3) ], 'Color', 'red');
    

end

