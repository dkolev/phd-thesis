package GammaXmlMessages;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class StringIdsAlarmContent extends IdsAlarmContent 
{
	private static final String contentElementName = "content";
	private String content;
	public StringIdsAlarmContent(String cont)
	{
		contentType = "string";
		content = cont;
	}
	
	public StringIdsAlarmContent(Element elem)
	{
		contentType = "string";
		content = elem.getTextContent();
	}
	
	public Element serialiseContent(Document doc)
	{
		Element contentElem = doc.createElement(contentElementName);
		addSimpleElementValue(contentElem, doc, contentElementName, content);
		return contentElem;
	}
	
	public String what()
	{
		return content;
	}

	
}
