function [ Aineq, bineq, Aeq, beq, f, CostRow, CostRowt, Ddim, pdim, xDim, zDim, nuDim, Kdim, yDim, xCorr, xType] = PrepareLProblemCostMinimization(...
    ExpandedGraphStructure, StartID, StopID,...
    Corresp, ThreatControlMat, ProcControlMat, ChckPntControlMat, CostFun, TempCostFun, ProcCost, ExpandedAssetID, TempCostTh)
% Prepares the input for BLP solver
% our variable [D, p, x, z, nu, K, y]

pdim = size(ExpandedGraphStructure, 1);
CheckPointNum = size(ChckPntControlMat, 1);
N = size(ExpandedGraphStructure, 1);
ControlNum = size(ThreatControlMat, 2);
AttackTypeNum = size(ThreatControlMat, 1);
Ddim = CheckPointNum*AttackTypeNum;
AssetNum = length(ExpandedAssetID(:));
xCorr = [];
xType = [];
N_reduced = N/AttackTypeNum;

%% Prepare Aineq, bineq and f
% Prepare eq. D <= Ax and cost for x
Aineq = zeros(Ddim, 0);
f = [];
ft = [];
%stupid indexing... D = [N1.1, N1.2, ...N1.CheckPointNum N2.1, ...
%NAttackTypeNum.CheckPointNum]
for i=1:ControlNum %for all controls
    for j=1:size(ChckPntControlMat, 1) % for all check points
        ControlMatr = zeros(CheckPointNum, AttackTypeNum);
        if(ChckPntControlMat(j, i) == 1)
           ControlMatr(j, :) = ThreatControlMat(:, i)'; %stupid method... must be vectorized!
           tmpN = ControlMatr(:);
           Aineq = [Aineq, tmpN];
           xCorr = [xCorr, j];
           xType = [xType, i];
           f = [f, CostFun(i)];% cost depends only on the type of control!
           ft = [ft, TempCostFun(i)];
        end
    end 
end
xDim = length(f);
% no dependance on p => zeros
Aineq = [zeros(Ddim, N), Aineq];
% minus D => -eye
Aineq = [-eye(Ddim), Aineq];
%prepare -Dij - pi + pj <=0
ExpandedEnterID = ExpandedGraphStructure(:, StartID)==1;

CheckPointGraph = ExpandedGraphStructure;
CheckPointGraph(ExpandedAssetID, :) = 0;
CheckPointGraph(StopID, :) = 0;
CheckPointGraph(:, StartID) = 0;
CheckPointGraph(:, ExpandedEnterID) = 0;
for i = 1:size(Corresp, 1)
    IdMatrix = Corresp{i, 2};
    for j = 1:AttackTypeNum
       CheckPointGraph(IdMatrix(j, 2), IdMatrix(j, 1)) = (j-1)*size(Corresp, 1) + i; %number of the check-point inserted 
    end
end


%prepare matrix for D
Ns = sum(ExpandedGraphStructure(:));
[r, c] = find(ExpandedGraphStructure == 1);
TmpIneq = zeros(Ns, Ddim);
Pineq = zeros(Ns, N);

for i = 1:length(r)
   Pineq(i, r(i)) = 1;
   Pineq(i, c(i)) = -1;
   if(CheckPointGraph(r(i), c(i)) > 0)
       TmpIneq(i, CheckPointGraph(r(i), c(i))) = -1;
   end
end

% TmpIneq = zeros(N*N, Ddim);
% for i = 1:AttackTypeNum
%     for j = 1:CheckPointNum
%         IdMatrix = Corresp{j, 2};
%         Id = (IdMatrix(i, 1)-1)*N + IdMatrix(i, 2); %edge, corresponding to the j-th check-point (Corresp{j, 2}) and i-th attack type
%         D_id = CheckPointNum*(i-1) + j; %which component of d corresponds to the link between IdMatrix(i, :)
%         TmpIneq(Id, D_id) = -1;
%     end
% end

%prepare matrix for p
%Pineq1 = repmat(eye(N), N, 1);
%Pineq2 = reshape(Pineq1, N, N*N)';

%Pineq = -Pineq2 + Pineq1;

CutIneq = [TmpIneq, Pineq, zeros(Ns, xDim)];

Aineq = [-Aineq; CutIneq];
bineq = zeros(size(Aineq, 1), 1);

%% Prepare Aeq and CostRow
% p(START) = 0; p(STOP) = 1;
p1 = zeros(1, size(Aineq, 2));
p2 = p1;
p1(Ddim + StartID) = 1;
p2(Ddim + StopID) = 1;
Aeq = [p1; p2];
beq = [0; 1];
f = [zeros(1, Ddim + pdim), f];
ft = [zeros(1, Ddim + pdim), ft];
xCostRow = f;
xCostRowt = ft;
%% Additional problem expanding. Adding z

% update the size of Aeq
zDim = 1;
Aeq = [Aeq, zeros(size(Aeq, 1), zDim)]; % add z. It is not used here => all zeros
Aineq = [Aineq, zeros(size(Aineq, 1), zDim)];

f = [zeros(1, Ddim + pdim + xDim), 1];

%% Preparation of procedural controls
Kdim = length(ExpandedAssetID(:)); % dim of K - number of Expanded assets
%% Additional slackness variables - enable risk tolerating and procedural controls
nuDim = length(ExpandedAssetID(:)); %dimentionality of tolerating variable 

% we need to find the rows in Aeq, which are 'responsible' for the
% connection to STOP
LinearIDs = (ExpandedAssetID(:)-1)*N + StopID;

% add -k-p-nu <= -p_stop
EdgeIDs = find(ExpandedGraphStructure(:) == 1); %stupid edge innumeration...

RowIDs = zeros(length(LinearIDs), 1);

for i=1:length(LinearIDs)
    RowIDs(i) = find(EdgeIDs == LinearIDs(i));
end

tmp = zeros(size(Aineq, 1), nuDim + Kdim);

ToInsert = [-eye(nuDim), -eye(Kdim)];

tmp(RowIDs + Ddim, :) = ToInsert;

Aineq = [Aineq, tmp];


%add mu+k <= 1
ToInsert = [zeros(nuDim, Ddim + pdim + xDim+ zDim), eye(nuDim), eye(Kdim)];
Aineq = [Aineq; ToInsert];
bineq = [bineq; ones(nuDim, 1)];

%add mu+p <= 1
ToInsert = zeros(nuDim, size(Aineq, 2));
pIds = eye(pdim);
pIds = pIds(ExpandedAssetID(:), :);
tmpStart = Ddim + pdim + xDim+ zDim + 1;
tmpEnd = Ddim + pdim + xDim+ zDim + nuDim;
ToInsert(:, tmpStart:tmpEnd) = eye(nuDim);
Aineq = [Aineq; ToInsert];
bineq = [bineq; ones(nuDim, 1)];

%add k<=K*y
procNum = size(ProcControlMat, 3);
ProcLimMatrix = reshape(ProcControlMat, length(ExpandedAssetID(:)), procNum); 
ProcControlStructure = [eye(Kdim), -ProcLimMatrix]; % the part, representing inequality matrix
yDim = size(ProcLimMatrix, 2);

Aineq = [Aineq, zeros(size(Aineq, 1), yDim)];
Aineq = [Aineq; zeros(Kdim, Ddim + pdim + xDim+ zDim + nuDim), ProcControlStructure];
bineq = [bineq; zeros(Kdim, 1)];
CostRow = [xCostRow, zeros(1, zDim+nuDim+Kdim + yDim)];
CostRowt = [xCostRowt, zeros(1, zDim+nuDim+Kdim), ProcCost]; 

%create mu and z constraints: U*mu - z <=0
tmp = [zeros(nuDim, Ddim + pdim + xDim), -ones(nuDim, zDim), eye(nuDim), zeros(nuDim, Kdim+yDim)];
Aineq = [Aineq; tmp];
bineq = [bineq; zeros(nuDim, 1)];

%add cost limitation
Aineq = [Aineq; CostRowt];
bineq = [bineq; TempCostTh];


%% Change Aeq - meet the required deminsion
AllAddDim = nuDim+Kdim+yDim;
Aeq = [Aeq, zeros(size(Aeq, 1), AllAddDim)];

%% Add regularization - in order to achieve a better solution

lambda = min(abs(CostFun(CostFun>0)))/max(abs([TempCostFun, ProcCost]));
f = CostRowt*lambda + CostRow; %you should minimize that



end
